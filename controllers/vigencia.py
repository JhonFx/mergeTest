# -*- coding: utf-8 -*-

import re
import json
import datetime
import urllib2
import datetime
from webservice_queries import list_subjects, subject_details ,asignaturas_vigentes ,asignaturas_no_vigentes

# Settings variable
ws_url = settings.dace_ws_url

@auth.requires(auth.is_logged_in() and auth.has_membership(auth.id_group(role="DECANATO")))
def list_vigente_asignatura():
    """
        Controlador para la vista: Lista de asignaturas vigentes | Vista en la cual se listan todas las asignaturas vigentes
    """

    message = "Lista de asignaturas vigentes"

    # Se busca las asignaturas vigentes en la tabla ASIGNATURAS_VIGENTES (codigo y denominación) y con dos outer left join se consigue los ID de los programas vigentes o de los más recientes
    rows = db(db.ASIGNATURAS_VIGENTES.denominacion!="").select(db.ASIGNATURAS_VIGENTES.codigo, db.ASIGNATURAS_VIGENTES.denominacion)


    return dict(message=message, vigentes=rows)

@auth.requires(auth.is_logged_in() and auth.has_membership(auth.id_group(role="DECANATO")))
def search():
    '''
        Busqueda por el estado de vigencia por codigo
    '''

    if auth.has_membership(auth.id_group(role="INACTIVO")):
        redirect(URL(c='default', f='inactive'))

    message = 'Búsqueda de estado de vigencia del programa analitico de una asignatura'

    form_busqueda = SQLFORM.factory(Field('codigo', type="string"), labels={'codigo': 'Código'})

    if form_busqueda.process(formname='form_busqueda').accepted:
        response.flash = "Accepted!"
        redirect(URL(c='vigencia', f='search_results', vars=form_busqueda.vars))

    if form_busqueda.errors:
        response.flash = "Error en la búsqueda"
        pass

    return dict(message=message, form_busqueda=form_busqueda)

@auth.requires(auth.is_logged_in() and auth.has_membership(auth.id_group(role="DECANATO")))
def search_results():
    """
        Controlador para la vista:Vista que permite ver el estado de un código especifico
    """
    
    if(request.vars.codigo is not None):
        codigo = request.vars.codigo.upper()

    message = 'Estado de vigencia del ultimo programa de la asignatura :' + request.vars.codigo

    # Se determina si el codigo es valido y si la asignatura es vigente
    codigo_valido = False
    asignatura_vigente = False
    busqueda_vigencia_asignatura = db(db.ASIGNATURAS_VIGENTES.codigo == codigo).select(db.ASIGNATURAS_VIGENTES.codigo).first()

    if busqueda_vigencia_asignatura is not None:
        asignatura_vigente = True
        codigo_valido = True

    # Se determina si existe al menos un programa y si es vigente el programa
    existe_programa_transcrito = False
    programa_es_vigente = False

    busqueda_programa = db(db.PROGRAMA.codigo == codigo).select(db.ASIGNATURAS_VIGENTES.codigo).first()

    if (busqueda_programa is not None):
        existe_programa_transcrito = True
        if(asignatura_vigente):
            busqueda_vigencia_programa = db(db.PROGRAMAS_VIGENTES.codigo == codigo).select().first()
            print(busqueda_vigencia_programa)
            if busqueda_vigencia_programa is not None:
                programa_es_vigente = True

        

    # Si existe al menos un programa transcrito se busca el id del programa más reciente
    programs = None

    if existe_programa_transcrito:
        if(programa_es_vigente):
            programs = db((db.PROGRAMAS_VIGENTES.codigo == codigo) & (db.PROGRAMA.id == db.PROGRAMAS_VIGENTES.id_programa)).select(
                db.PROGRAMA.id,
                db.PROGRAMA.anio,
                db.PROGRAMA.periodo,
                db.PROGRAMA.anio_hasta,
                db.PROGRAMA.periodo_hasta,
                orderby=~db.PROGRAMA.anio | db.PROGRAMA.periodo,
                groupby=db.PROGRAMA.id
            )
        else:
            programs = db((db.PROGRAMAS_NO_VIGENTES.codigo == codigo) & (db.PROGRAMA.id == db.PROGRAMAS_NO_VIGENTES.id_programa)).select(
                db.PROGRAMA.id,
                db.PROGRAMA.anio,
                db.PROGRAMA.periodo,
                db.PROGRAMA.anio_hasta,
                db.PROGRAMA.periodo_hasta,
                db.PROGRAMAS_NO_VIGENTES.estado,                
                orderby=~db.PROGRAMA.anio | db.PROGRAMA.periodo,
                groupby=[db.PROGRAMA.id,db.PROGRAMAS_NO_VIGENTES.estado]
            )            

    return dict(
        message=message,
        codigo_valido=codigo_valido,
        asignatura_vigente=asignatura_vigente,
        existe_programa_transcrito=existe_programa_transcrito,
        programa_es_vigente=programa_es_vigente,
        programs=programs,
    )

@auth.requires(auth.is_logged_in() and auth.has_membership(auth.id_group(role="DECANATO")))
def actualizar_vigentes() :

    message = "Estado de vigencia de las asignaturas: actualizados satisfactoriamente"   
    
    #Actualizacion de vigencia de las asignaturas
    vigentes = asignaturas_vigentes(ws_url)

    db.ASIGNATURAS_VIGENTES.truncate('RESTART IDENTITY CASCADE')

    for asignatura in vigentes:
        db.ASIGNATURAS_VIGENTES.insert(codigo=asignatura["cod_asignatura"],denominacion=asignatura["nombre"])   

    no_vigentes = asignaturas_no_vigentes(ws_url)

    db.ASIGNATURAS_NO_VIGENTES.truncate('RESTART IDENTITY CASCADE')

    for asignatura in no_vigentes:
        db.ASIGNATURAS_NO_VIGENTES.insert(codigo=asignatura["cod_asignatura"],denominacion=asignatura["nombre"])   
    db.commit()	    
    #Actualización de vigencia de los programas    
    
    now = datetime.datetime.now()
    year= str(now.year)
    month= str(now.month)
  

    db.PROGRAMAS_VIGENTES.truncate('RESTART IDENTITY CASCADE')
    db.PROGRAMAS_NO_VIGENTES.truncate('RESTART IDENTITY CASCADE')
        
    #Sub querys que dependen del mes actual, ya que hay meses que estan en mas de un periodo
    
    # Periodos de trimestres regulares.
    SEP_DIC = 'SEP-DIC'
    ENE_MAR = 'ENE-MAR'
    ABR_JUL = 'ABR-JUL'

    # Periodos para programas de Pasantias
    ENE_MAY = 'ENE-MAY'
    ABR_SEP = 'ABR-SEP'
    JUL_DIC = 'JUL-DIC'
    OCT_FEB = 'OCT_FEB'
    
    #Subquerys una vez se determina que se encuentra en el mismo anio y se debe determinar la vigencia respecto al periodo (Que se valido hasta segun el periodo hasta)
    if(month=="1" or month=="2" or month=="3"):
        #Todos los periodos son validos si el mes actual es enero , febrero o marzo
        vig_query_periodo_hasta = True
    elif(month=="4" or month=="5"):
        vig_query_periodo_hasta = (db.PROGRAMA.periodo_hasta != ENE_MAR)
    elif(month=="6" or month=="7"):
        vig_query_periodo_hasta = (db.PROGRAMA.periodo_hasta != ENE_MAR) & (db.PROGRAMA.periodo_hasta != ENE_MAY)        
    elif(month=="8" or month=="9"):
        vig_query_periodo_hasta = (db.PROGRAMA.periodo_hasta != ENE_MAR) & (db.PROGRAMA.periodo_hasta != ENE_MAY) & (db.PROGRAMA.periodo_hasta != ABR_JUL)        
    elif(month=="10" or month=="11" or month=="12"):
        vig_query_periodo_hasta = (db.PROGRAMA.periodo_hasta != ENE_MAR) & (db.PROGRAMA.periodo_hasta != ENE_MAY) & (db.PROGRAMA.periodo_hasta != ABR_JUL) & (db.PROGRAMA.periodo_hasta != ABR_SEP)
            
            
    #Subquerys una vez se determina que se encuentra en el mismo anio y se debe determinar la vigencia respecto al periodo (Que sea valido segun el periodo desde)
    
    if(month=="1" or month=="2" or month=="3"):
        vig_query_periodo_desde = (db.PROGRAMA.periodo_hasta == ENE_MAR) | (db.PROGRAMA.periodo_hasta == ENE_MAY)
    elif(month=="4" or month=="5" or month=="6"):
        vig_query_periodo_desde = (db.PROGRAMA.periodo_hasta == ENE_MAR) | (db.PROGRAMA.periodo_hasta == ENE_MAY) | (db.PROGRAMA.periodo_hasta == ABR_JUL) | (db.PROGRAMA.periodo_hasta == ABR_SEP)
    elif(month=="7" or month=="8"):
        vig_query_periodo_desde = (db.PROGRAMA.periodo_hasta == ENE_MAR) | (db.PROGRAMA.periodo_hasta == ENE_MAY) | (db.PROGRAMA.periodo_hasta == ABR_JUL) | (db.PROGRAMA.periodo_hasta == ABR_SEP) | (db.PROGRAMA.periodo_hasta == JUL_DIC)
    elif(month=="9"):
        vig_query_periodo_desde = (db.PROGRAMA.periodo_hasta == ENE_MAR) | (db.PROGRAMA.periodo_hasta == ENE_MAY) | (db.PROGRAMA.periodo_hasta == ABR_JUL) | (db.PROGRAMA.periodo_hasta == ABR_SEP) | (db.PROGRAMA.periodo_hasta == JUL_DIC) | (db.PROGRAMA.periodo_hasta == SEP_DIC)
    elif(month=="10" or month=="11" or month=="12"):
        vig_query_periodo_desde = True
       
            
    #Programas respecto a la vigencia de la asignatura
    asig_vig = db.PROGRAMA.codigo==db.ASIGNATURAS_VIGENTES.codigo
    asig_no_vig = db.PROGRAMA.codigo==db.ASIGNATURAS_NO_VIGENTES.codigo
        
    #Vigencia desde
    vig_solo_anio_desde = db.PROGRAMA.anio<year 
        
    vig_anio_periodo_desde = (db.PROGRAMA.anio==year) & (vig_query_periodo_desde)
         
    #Vigencia hasta        
    vig_vacia_hasta = (db.PROGRAMA.anio_hasta==None)|(db.PROGRAMA.periodo_hasta==None)
    
    #Verificar que la actualidad esta por debajo del limite de vigencia
    vig_solo_anio_hasta = db.PROGRAMA.anio_hasta>year            
    vig_anio_periodo_hasta = (db.PROGRAMA.anio_hasta==year) & (vig_query_periodo_hasta)
         
    #Verificar que sobre pasa la actualidad al anio
    no_vig_solo_anio_hasta = db.PROGRAMA.anio_hasta<year   
    no_vig_anio_periodo_hasta = (db.PROGRAMA.anio_hasta==year) & (~(vig_query_periodo_hasta))
    
    #Estado 1:El programa tiene fecha de culminacion más antigua que la fecha actual
    
    query_1 =  db( (asig_vig) & ((no_vig_solo_anio_hasta) | (no_vig_anio_periodo_hasta ))).select(db.PROGRAMA.codigo,db.PROGRAMA.id,groupby=db.PROGRAMA.id)
     
    for programa in query_1:
        db.PROGRAMAS_NO_VIGENTES.insert(codigo=programa["codigo"],id_programa=programa["id"],estado=1)    

    #Estado 2:El programa no posee fecha de culminacion
 
    query_2 =  db((asig_vig) & (vig_vacia_hasta)).select(db.PROGRAMA.codigo,db.PROGRAMA.id)

    for programa in query_2:
        db.PROGRAMAS_NO_VIGENTES.insert(codigo=programa["codigo"],id_programa=programa["id"],estado=2)
           

    #Estado 3:El programa no es vigente ya que pertenece a una asignatura que no es vigente
    query_3 =  db(asig_no_vig).select(db.PROGRAMA.codigo,db.PROGRAMA.id,groupby=db.PROGRAMA.id)

    for programa in query_3:
        db.PROGRAMAS_NO_VIGENTES.insert(codigo=programa["codigo"],id_programa=programa["id"],estado=3)

    #Estado 4:El programa posee una fecha de entrada y salida de vigencia tal que es vigente actualmente 
 
    query_4 = db((asig_vig) & 
                 (~(vig_vacia_hasta)) & 
                 ((vig_solo_anio_desde) |(vig_anio_periodo_desde)) & 
                 ((vig_solo_anio_hasta) | (vig_anio_periodo_hasta))
                ).select(db.PROGRAMA.codigo,db.PROGRAMA.id,groupby=db.PROGRAMA.id)

    
    for programa in query_4:
        db.PROGRAMAS_VIGENTES.insert(codigo=programa["codigo"],id_programa=programa["id"],estado=4)
 
    db.commit()	
    
    return dict(message=message)      
    

