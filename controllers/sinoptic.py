# -*- coding: utf-8 -*-

from usbutils import get_ldap_data, random_key
from webservice_queries import  list_subjects, subject_details
from logs import *
import re
import urllib2
import json

ws_url = settings.dace_ws_url


"""
    Busca si existe una asignatura en Génesis (Simulado por SIGPAE-WS), dado su codigo
"""
def existe_en_genesis(cod):
    cod=cod.upper()
    subjects = list_subjects(ws_url)

    if subjects is None:
        return False
    
    for subject in subjects:
        if(subject[u'cod_asignatura']!=None and cod==subject[u'cod_asignatura'].upper()):
            return True
    return False

"""
    Busca si un programa está vigente apoyado en Génesis (Simulado por SIGPAE-WS), dado su codigo.
    El booleano Check determina si ya se revisó que existe el programa en genesis
    esto para evitar la doble revision si ya fue realizada antes
"""
@auth.requires(auth.is_logged_in() and auth.has_membership(auth.id_group(role="DECANATO")))
def info_programa_vigente(codigo,check=False):
    codigo=codigo.upper()
    info = {}
    info["codigo"]=codigo
    info["fecha_entrada_vigencia"] = None
    info["registrado"] = True
    info["vigente"] = False
    info["id"] = None
    info["denominacion"] = None
    
    if(not check and not existe_en_genesis(codigo)):
        info["registrado"] = False
        return False

    details=subject_details(codigo,ws_url)
    if details!=None:
        info["fecha_entrada_vigencia"] = details[u'vig_desde']

    ultimo_programa = db(db.PROGRAMA.codigo == codigo).select(db.PROGRAMA.id,db.PROGRAMA.denominacion,db.PROGRAMA.periodo,db.PROGRAMA.anio, orderby=~db.PROGRAMA.id, limitby=(0, 1)).first() 
    
    info["vigente"] = False    
    if(ultimo_programa!=None):
        info["id"] = ultimo_programa["id"]
        info["denominacion"] = ultimo_programa["denominacion"]
    
        periodo_sigpae = int(info["fecha_entrada_vigencia"].split("-")[0])
        anio_sigpae = int(info["fecha_entrada_vigencia"].split("-")[1])
    
        if(not(int(ultimo_programa["anio"])<anio_sigpae or (int(ultimo_programa["anio"])==anio_sigpae and int(ultimo_programa["periodo"])<periodo_sigpae ))):
            info["vigente"] = True
            info["fecha_entrada_vigencia"]= str(ultimo_programa["periodo"])+"-"+str(ultimo_programa["anio"])

    return info


'''
    Permite visualisar un programa sinoptico en una vista HTML con todos sus elementos.
'''
@auth.requires(auth.is_logged_in() and auth.has_membership(auth.id_group(role="DECANATO")))
def view():

    message = "Detalles del Programa Sinóptico"

    id =  request.vars['id']
    if not isinstance(id, str):
        id = id[0]

    #programa = db(db.PROGRAMA.id == id).select()
    programa = db(db.PROGRAMA.id == id).select(db.PROGRAMA.id,
                                               db.PROGRAMA.periodo,
                                               db.PROGRAMA.periodo_hasta,
                                               db.PROGRAMA.anio,
                                               db.PROGRAMA.anio_hasta,
											   db.PROGRAMA.codigo,
											   db.PROGRAMA.denominacion,
											   db.PROGRAMA.creditos,
											   db.PROGRAMA.horas_teoria,
											   db.PROGRAMA.horas_practica,
											   db.PROGRAMA.horas_laboratorio,
                                               db.PROGRAMA.sinopticos,
                                               db.PROGRAMA.objetivos_generales,
                                               db.PROGRAMA.objetivos_especificos,
                                               db.PROGRAMA.original_pdf)
	
    if programa:
        programa = programa.first()
    else:
        redirect(URL(c='default', f='not_authorized'))

    # obtenemos los campos adicionales, si existen
    campos_adicionales = db(db.CAMPOS_ADICIONALES_PROGRAMA.programa == programa).select()

    TIPOS_CITA = (
        ("APA", "APA"),
        ("IEEE", "IEEE"),
        ("Harvard", "Harvard")
    )

    # formulario para editar campos adicionales
    estilo_citas_form = SQLFORM.factory(
            Field('tipo_cita', type ='string',  requires = IS_IN_SET(TIPOS_CITA,
                zero='Seleccione', error_message = 'Seleccione un estilo de cita.')),

            labels = {
                'tipo_cita' : 'Estilo de cita de fuentes de información'},
            submit_button=T('Aceptar')
    )

    # procesamiento del formulario para la busqueda de libros
    if estilo_citas_form.process(formname = "estilo_citas_form").accepted:
        estilo  = ''

        if estilo_citas_form.vars.tipo_cita:
            estilo = estilo_citas_form.vars.tipo_cita

        redirect(
          URL(
            c='programs',
            f='generate',
            vars={
              'cod'    : programa.id,
              'estilo' : estilo
            }
          ),
          client_side = True
        )

    elif estilo_citas_form.errors:
        response.flash = 'Seleccione un estilo de referencia.'

    return dict(message=message, programa=programa,
                campos_adicionales=campos_adicionales,
                estilo_citas_form = estilo_citas_form)


@auth.requires(auth.is_logged_in() and auth.has_membership(auth.id_group(role="DECANATO")))
def search():
    """Forma principal para la búsqueda por código de un programa sinoptico en genesis (Simulado por SIGPAE-WS)."""

    if auth.has_membership(auth.id_group(role="INACTIVO")):
        redirect(URL(c='default', f='inactive'))

    message = 'Búsqueda de Programas Sinópticos'

    form_busqueda = SQLFORM.factory(Field('codigo', type="string"),
                                    labels={'codigo' : 'Código'})


    if form_busqueda.process(formname='form_busqueda').accepted:
        response.flash = "Accepted!"
        redirect(URL(c='sinoptic', f='search_list', vars=form_busqueda.vars))

    if form_busqueda.errors:
        response.flash = "Error en la búsqueda"
        pass
    
    return dict(message=message, form_busqueda = form_busqueda)


@auth.requires(auth.is_logged_in() and auth.has_membership(auth.id_group(role="DECANATO")))
def search_results():
    
    """
        Vista donde se muestran los resultados de la busqueda del codigo en Genesis (Simulado por SIGPAE-WS)
    """

    cod =  request.vars.codigo

    registrado_genesis = existe_en_genesis(cod)
    message =  "Estado de la asignatura  %s"%(cod)
    detalles_vigencia = None
    
    if(registrado_genesis):
        detalles_vigencia = info_programa_vigente(cod,True)

    return dict(message=message, registrado_genesis = registrado_genesis, detalles_vigencia = detalles_vigencia)


@auth.requires(auth.is_logged_in() and auth.has_membership(auth.id_group(role="DECANATO")))
def search_list():
    
    """
        Muestra una lista con los resultados obtenidos
    """
    cod =  request.vars.codigo
    registrado_genesis = existe_en_genesis(cod)
    message =  "Estado de la asignatura  %s"%(cod)
    detalles_vigencia = None
    
    if(registrado_genesis):
        detalles_vigencia = info_programa_vigente(cod,True)

    subjects = db(db.PROGRAMA.codigo.lower() == cod.lower()).select()

    if len(subjects) == 1: redirect(URL(c='sinoptic', f='view', vars=dict(id=subjects.first().id)))
        
    return dict(
        subjects=subjects,
        single=len(subjects) < 1,
        message=message,
        registrado_genesis=registrado_genesis,
        detalles_vigencia=detalles_vigencia
    )
