import urllib2
import json
import re

def list():
    """
        Inicio en la vista consulta de programas analiticos aprobados por el estudiante.
    """

    message = "Programas Analíticos de Estudio de Asignaturas Aprobadas"

    try:
        if ((auth.is_logged_in())and(auth.has_membership(auth.id_group(role="ESTUDIANTE")))and(not(request.vars.carnet))):
            carnetBusqueda = auth.user.username
            page = urllib2.urlopen(settings.dace_ws_url + '/estudiantes/asig-aprobadas/?carnet=%s'%(carnetBusqueda)).read()
        elif request.vars.carnet:
            carnetBusqueda = request.vars.carnet
            page = urllib2.urlopen(settings.dace_ws_url + '/estudiantes/asig-aprobadas/?carnet=%s'%(carnetBusqueda)).read()
        approved_subjects = json.loads(page)
            
    except urllib2.URLError as e:
        print(e.reason)
        response.flash = 'Error de conexión con el Web Service.'
        return dict(message=message, error=e.reason, approved_subjects=[])

    programs = []
    for subject in approved_subjects:
        # Por cada materia aprobada por el estudiante se agregan los programas que le corresponden
        # a una lista.
        if subject:
            codigo = subject['cod_asignatura']
            programs.append(
                db(db.PROGRAMA.codigo == codigo).select(db.PROGRAMA.codigo,
                                                        db.PROGRAMA.id,
                                                        db.PROGRAMA.periodo,
                                                        db.PROGRAMA.anio,
                                                        db.PROGRAMA.periodo_hasta,
                                                        db.PROGRAMA.anio_hasta)
                )

    approved_subjects2 = zip(approved_subjects, programs)

#Se hace una lista que se asociara a approved subjects que contendra el id del programa que le corresponde
    time_sensitive_programs = []
#Para acceder a la data obtenida en el zip anterior se usa una iteración de la siguiente manera:
    for subject in approved_subjects2:
        print "Buscando programa para materia: " + str(subject[0]['cod_asignatura'])
        program_for_student_found = False
        closest_guess = None
        time_for_guess = -1
        for prog in subject[1]:
            argsPage = []
            argsPage.append(carnetBusqueda)
            argsPage.append(prog.codigo)

#Se obtienen los tiempos en los que el estudiante aprobó la materia
            try:
                page = urllib2.urlopen(settings.dace_ws_url + '/estudiantes/tiempo-asig-aprobadas/?carnet=%s&codigo=%s'%(argsPage[0],argsPage[1])).read()
            except urllib2.URLError as e:
                response.flash = 'Problema con identificacion del estudiante'
                return dict(message=message, error=e.reason, approved_subjects=[])

#http://127.0.0.1:8000/SIGPAE_WS/default/webservices/estudiantes/tiempo-asig-aprobadas/?carnet=11-10088&codigo=MA2611
            time_of_aproved_subjects = json.loads(page)
#Se revisa que programa es el que estaba valido cuando se aprobó la materia
            for time_of_subject in time_of_aproved_subjects:
                anio_inic = (2000 + ((int(time_of_subject['siglo'])-1)*100) + int(time_of_subject['anio_inic']))
                print "prog anio:" + str(prog.anio)
                print "prog anio hasta:" + str(prog.anio_hasta)
                print "anio inic:" + str(anio_inic)
                print "mes inic:" + str(time_of_subject['mes_inic'])
                print "mes fin:" + str(time_of_subject['mes_fin'])
                if (prog.periodo == 'ENE-MAR'):
                    mes_vigencia_inic = 1
                elif (prog.periodo == 'ABR-JUL'):
                    mes_vigencia_inic = 4
                elif (prog.periodo == 'SEP-DIC'):
                    mes_vigencia_inic = 9
                elif (prog.periodo == 'ENE-MAY'):
                    mes_vigencia_inic = 1
                elif (prog.periodo == 'ABR-SEP'):
                    mes_vigencia_inic = 4
                elif (prog.periodo == 'JUL-DIC'):
                    mes_vigencia_inic = 6
                elif (prog.periodo == 'OCT-FEB'):
                    mes_vigencia_inic = 10


                if (prog.anio_hasta is None):
#Cuando el programa no tiene tiempo final
                    if (anio_inic >= prog.anio):
                        print "no hay tiempo final para el programa:" + prog.codigo
#por ahora se ignora y se trata como que esta bien el tiempo final
                        if not(program_for_student_found):
#Se añade el codigo del programa a una lista.
                            months_from_start = (int(anio_inic-prog.anio)*12) + (int(time_of_subject['mes_inic']) - mes_vigencia_inic)
                            if ((months_from_start >= 0)and((time_for_guess == -1)or(months_from_start <= time_for_guess))):
                                print "consegui un programa para"
                                print str(prog.id)
                                closest_guess = prog.id
                                time_for_guess = months_from_start
                            else:
#Si ya hay un programa que encaja con el tiempo establecido, no se hace append y se escribe un aviso
                                print "Existen dos programas que estan disponibles para el tiempo en el que se curso la materia"


                else:
#Cuando el programa tiene tiene tiempo final
                    if ((anio_inic > prog.anio) and (anio_inic < prog.anio_hasta)):
                        if not(program_for_student_found):
                            print "consegui el programa b"
                            print str(prog.id)
                            time_sensitive_programs.append(prog.id)
                            program_for_student_found = True
                        else:
                            print "Existen dos programas que estan disponibles para el tiempo en el que se curso la materia"
                    elif ((anio_inic == prog.anio) and (anio_inic < prog.anio_hasta)):
#Cuando la materia se vio el mismo anio en el que entro en vigencia el programa
                        if (int(time_of_subject['mes_inic']) >= mes_vigencia_inic):
                            print "mes_vigencia_inic:" + str(mes_vigencia_inic)
                            if not(program_for_student_found):
                                print "consegui el programa c"
                                print str(prog.id)
                                time_sensitive_programs.append(prog.id)
                                program_for_student_found = True
                            else:
                                print "Existen dos programas que estan disponibles para el tiempo en el que se curso la materia"
                    elif ((anio_inic > prog.anio) and (anio_inic == prog.anio_hasta)):
#Cuando la materia se vio el mismo anio en el que salio de vigencia el programa
                        if (prog.periodo_hasta == 'ENE-MAR'):
                            mes_vigencia_fin = 3
                        elif (prog.periodo_hasta == 'ABR-JUL'):
                            mes_vigencia_fin = 7
                        elif (prog.periodo_hasta == 'SEP-DIC'):
                            mes_vigencia_fin = 12
                        elif (prog.periodo_hasta == 'ENE-MAY'):
                            mes_vigencia_fin = 5
                        elif (prog.periodo_hasta == 'ABR-SEP'):
                            mes_vigencia_fin = 9
                        elif (prog.periodo_hasta == 'JUL-DIC'):
                            mes_vigencia_fin = 12
#Hay un detalle aqui con que el anio final para oct_feb no es igual al prog.anio_hasta
                        elif (prog.periodo_hasta == 'OCT-FEB'):
                            mes_vigencia_fin = 2
                        if (int(time_of_subject['mes_fin']) <= mes_vigencia_fin):
                            print "mes_vigencia_fin:" + str(mes_vigencia_fin)
                            if not(program_for_student_found):
                                print "consegui el programa d"
                                print str(prog.id)
                                time_sensitive_programs.append(prog.id)
                                program_for_student_found = True
                            else:
                                print "Existen dos programas que estan disponibles para el tiempo en el que se curso la materia"


        if not(program_for_student_found)and(closest_guess == None):
            print "no se consiguió programa para la materia "
            time_sensitive_programs.append(None)
        elif not(program_for_student_found)and(not(closest_guess == None)):
            print "no se consiguió programa para la materia "
            time_sensitive_programs.append(closest_guess)
        else:
            print str(time_sensitive_programs)

            
            
    form_correo = SQLFORM.factory(
                                     Field('codigo', type="string"),
                                     Field('periodo', type="string"),
                                     Field('observaciones', type="string"),
                                     labels={'codigo' : 'Codigo de la materia',
                                             'periodo' : 'Fecha en que se curso',
                                             'observaciones': 'Observaciones'},
                                     submit_button=T('Aceptar')
    )

    approved_subjects = zip(approved_subjects,programs,time_sensitive_programs)
    if form_correo.process(formname='form_correo').accepted:
        response.flash = "Enviando Correo (No implementado)"
        #Enviar Correo        
        redirect(
          URL(
            c='students',
            f='list',
            vars= {'carnet':carnetBusqueda}
          )
        )
    elif form_correo.errors:
        response.flash = 'No implementado.'

    return dict(message=message, approved_subjects=approved_subjects, form_correo=form_correo)
