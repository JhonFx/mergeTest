from   pdf_generator import generatePDF
import urllib2
import json
import re
import cStringIO
import os


@auth.requires(auth.is_logged_in() and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def list():
    """
        Inicio en la vista consulta de programas.
    """

    message = "Programas Analíticos de Estudio por Departamento"

    try:
        page = urllib2.urlopen(settings.dace_ws_url + '/departamentos/').read()
        departments =  json.loads(page)
    except urllib2.URLError as e:
        response.flash = 'Error de conexión con el Web Service.'
        return dict(message=message, error = e.reason, departments = {})

    return dict(message=message, departments = departments)

'''
    No requiere permiso especial, pues se trata de una vista que puede ser consultada publicamente.
'''
def view():
    """
        Permite visualisar un programa en una vista HTML con todos sus elementos.
    """

    message = "Detalles del Programa"

    id =  request.vars['id']
    if not isinstance(id, str):
        id = id[0]

    programa = db(db.PROGRAMA.id == id).select()
    if programa:
        programa = programa.first()
    else:
        redirect(URL(c='default', f='not_authorized'))

    # obtenemos los campos adicionales, si existen
    campos_adicionales = db(db.CAMPOS_ADICIONALES_PROGRAMA.programa == programa).select()

    TIPOS_CITA = (
        ("APA", "APA"),
        ("IEEE", "IEEE"),
        ("Harvard", "Harvard")
    )

    # formulario para editar campos adicionales
    estilo_citas_form = SQLFORM.factory(
            Field('tipo_cita', type ='string',  requires = IS_IN_SET(TIPOS_CITA,
                zero='Seleccione', error_message = 'Seleccione un estilo de cita.')),

            labels = {
                'tipo_cita' : 'Estilo de cita de fuentes de información'},
            submit_button=T('Aceptar')
    )

    # procesamiento del formulario para la busqueda de libros
    if estilo_citas_form.process(formname = "estilo_citas_form").accepted:
        estilo  = ''

        if estilo_citas_form.vars.tipo_cita:
            estilo = estilo_citas_form.vars.tipo_cita

        redirect(
          URL(
            c='programs',
            f='generate',
            vars={
              'cod'    : programa.id,
              'estilo' : estilo
            }
          ),
          client_side = True
        )

    elif estilo_citas_form.errors:
        response.flash = 'Seleccione un estilo de referencia.'

    return dict(message=message, programa=programa,
                campos_adicionales=campos_adicionales,
                estilo_citas_form = estilo_citas_form)


def replace_number(referencia,numeroReferencia):

    if ("[1]" in referencia):
        nuevaReferencia = referencia.replace("[1]","["+str(numeroReferencia)+"] ")
    else:
        nuevaReferencia = "["+str(numeroReferencia)+"] "+referencia

    return nuevaReferencia

def fuentes_informacion(program):

  referencias_programa = db(db.PROGRAMA_REFERENCIAS.programa == program).select()
  print("REFERENCIAS DEL PROGRAMA:")
  for e in referencias_programa:
      print(e)

  fuentes_informacion_harvard = ''
  fuentes_informacion_ieee    = ''
  fuentes_informacion_apa     = ''
  fuentes_informacion         = {}
  numeroReferencia           =  1

  for referencia  in referencias_programa:

    # Referencias segun el tipo
    if referencia.referencia_libros:
        fuentes_informacion_harvard += "- "+referencia.referencia_libros.cite
        fuentes_informacion_ieee += replace_number(referencia.referencia_libros.citeIEEE,numeroReferencia)
        fuentes_informacion_apa += "- "+ referencia.referencia_libros.citeAPA
    if referencia.referencia_articulos:
        fuentes_informacion_harvard += "- "+referencia.referencia_articulos.cite
        fuentes_informacion_ieee += replace_number(referencia.referencia_articulos.citeIEEE,numeroReferencia)
        fuentes_informacion_apa += "- "+ referencia.referencia_articulos.citeAPA
    if referencia.referencia_videos:
        fuentes_informacion_harvard += "- "+referencia.referencia_videos.cite
        fuentes_informacion_ieee += replace_number(referencia.referencia_videos.citeIEEE,numeroReferencia)
        fuentes_informacion_apa += "- "+ referencia.referencia_videos.citeAPA
    if referencia.referencia_web:
        fuentes_informacion_harvard += "- "+referencia.referencia_web.cite
        fuentes_informacion_ieee += replace_number(referencia.referencia_web.citeIEEE,numeroReferencia)
        fuentes_informacion_apa += "- "+ referencia.referencia_web.citeAPA
    if referencia.referencia_otros:
        fuentes_informacion_harvard += "- "+referencia.referencia_otros.cite
        fuentes_informacion_ieee += replace_number(referencia.referencia_otros.citeIEEE,numeroReferencia)
        fuentes_informacion_apa += "- "+ referencia.referencia_otros.citeAPA
    fuentes_informacion_harvard += '\n'
    fuentes_informacion_ieee += '\n'
    fuentes_informacion_apa += '\n'

    # Se agregan los comentarios, si existen
    if referencia.referencia_libros:
        if  (referencia.referencia_libros.comentarios):
            print(referencia.referencia_libros.comentarios)
            fuentes_informacion_harvard += "Comentarios: "+referencia.referencia_libros.comentarios
            fuentes_informacion_harvard += '\n'

            fuentes_informacion_ieee += "Comentarios: "+referencia.referencia_libros.comentarios
            fuentes_informacion_ieee += '\n'

            fuentes_informacion_apa += "Comentarios: "+referencia.referencia_libros.comentarios
            fuentes_informacion_apa += '\n'
    if referencia.referencia_articulos:
        if  (referencia.referencia_articulos.comentarios):
            print(referencia.referencia_articulos.comentarios)
            fuentes_informacion_harvard += "Comentarios: "+referencia.referencia_articulos.comentarios
            fuentes_informacion_harvard += '\n'

            fuentes_informacion_ieee += "Comentarios: "+referencia.referencia_articulos.comentarios
            fuentes_informacion_ieee += '\n'

            fuentes_informacion_apa += "Comentarios: "+referencia.referencia_articulos.comentarios
            fuentes_informacion_apa += '\n'
    if referencia.referencia_videos:
        if  (referencia.referencia_videos.comentarios):
            print(referencia.referencia_videos.comentarios)
            fuentes_informacion_harvard += "Comentarios: "+referencia.referencia_videos.comentarios
            fuentes_informacion_harvard += '\n'

            fuentes_informacion_ieee += "Comentarios: "+referencia.referencia_videos.comentarios
            fuentes_informacion_ieee += '\n'

            fuentes_informacion_apa += "Comentarios: "+referencia.referencia_videos.comentarios
            fuentes_informacion_apa += '\n'
    if referencia.referencia_web:
        if  (referencia.referencia_web.comentarios):
            print(referencia.referencia_web.comentarios)
            fuentes_informacion_harvard += "Comentarios: "+referencia.referencia_web.comentarios
            fuentes_informacion_harvard += '\n'

            fuentes_informacion_ieee += "Comentarios: "+referencia.referencia_web.comentarios
            fuentes_informacion_ieee += '\n'

            fuentes_informacion_apa += "Comentarios: "+referencia.referencia_web.comentarios
            fuentes_informacion_apa += '\n'
    if referencia.referencia_otros:
        if  (referencia.referencia_otros.comentarios):
            print(referencia.referencia_otros.comentarios)
            fuentes_informacion_harvard += "Comentarios: "+referencia.referencia_otros.comentarios
            fuentes_informacion_harvard += '\n'

            fuentes_informacion_ieee += "Comentarios: "+referencia.referencia_otros.comentarios
            fuentes_informacion_ieee += '\n'

            fuentes_informacion_apa += "Comentarios: "+referencia.referencia_otros.comentarios
            fuentes_informacion_apa += '\n'

    numeroReferencia += 1

  fuentes_informacion['APA']     = fuentes_informacion_apa
  fuentes_informacion['IEEE']    = fuentes_informacion_ieee
  fuentes_informacion['Harvard'] = fuentes_informacion_harvard

  return fuentes_informacion


def generate():
    """
        Genera un PDF con los contenidos del programa registrados en el sistema.
    """
    cod    = request.vars['cod']
    estilo = request.vars['estilo']
    if not cod:
        redirect(URL(c='default', f='not_authorized'))

    # Obtener el programa
    programa = db(db.PROGRAMA.id == cod).select().first()

    if not programa:
        redirect(URL(c='default', f='not_authorized'))

    # Se generan las referencias.
    referencias_programa = fuentes_informacion(programa)

    # Obtener extras
    extras = {}
    for e in db(db.CAMPOS_ADICIONALES_PROGRAMA.programa == programa).select():
        extras[e.nombre] = e.contenido

    #WebService para Obtener nombre departamento segun el codigo
    try:
        page = urllib2.urlopen(
            settings.dace_ws_url + '/departamentos').read()
        department_data = json.loads(page)
    except urllib2.URLError as e:
        response.flash = 'Error de conexión con el Web Service.'
        return dict(error=e.reason)
    cod_depto = programa.codigo[:2] # Dos primeros digitos codigo del departamento
    for depto in department_data:
        if depto.get("siglas_depto") == cod_depto:
            nombre_depto = depto.get("nombre")

    buffer = cStringIO.StringIO()
    generatePDF(buffer=buffer,
                request=request,
                cod=programa.codigo,
                division="", #Falta obtener el codigo de división
                depto=nombre_depto,
                nombre=programa.denominacion,
                anio_vig=programa.anio,
                periodo_vig=programa.periodo,
                h_teoria=programa.horas_teoria,
                h_practica=programa.horas_practica,
                h_laboratorio=programa.horas_laboratorio,
                creditos=programa.creditos,
                sinopticos=programa.sinopticos,
                fuentes=referencias_programa[estilo],
                estrategias_met=programa.estrategias_met,
                estrategias_eval=programa.estrategias_eval,
                obj_general=programa.objetivos_generales,
                obj_especificos=programa.objetivos_especificos,
                observaciones=programa.observaciones
                #extras=extras
    )
    pdf = buffer.getvalue()
    buffer.close()

    header = {'Content-Type':'application/pdf'}
    response.headers.update(header)
    return pdf

#@auth.requires(
#    auth.has_membership(auth.id_group(role="DACE-ADMINISTRADOR")) or
#    auth.has_membership(auth.id_group(role="DEPARTAMENTO"))
#)
def originalpdf():
    """
        Permite descargar le PDF original de programa. Habilitado solo para los programas que tienen un PDF historico.
        (Vienen de una Transcripcion.)
    """

    program_id = request.args(0)
    if not program_id:
        redirect(URL(c='default', f='not_authorized'))

    # Obtener el programa
    programa = db(db.PROGRAMA.id == program_id).select().first()

    if not programa:
        redirect(URL(c='default', f='not_authorized'))

    if programa.original_pdf:
        fullpath = os.path.join(request.folder,'static/transcriptions/originalpdf', programa.original_pdf)
        response.stream(os.path.join(request.folder, fullpath))

    redirect(URL(c='default', f='not_authorized'))


@auth.requires(auth.is_logged_in() and auth.has_permission('request_ap') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def add():

    file = request.vars["file"]
    file_type = request.vars["file_type"]
    extract_type = request.vars["extract_type"]

    message = "Nueva propuesta de Borrador de Programa"

    form = SQLFORM.factory(
      Field('codigo', type='string', length = 8,
            requires = IS_MATCH('([A-Z]{2,2}[0-9]{4,4})|([A-Z]{3,3}[0-9]{3,3})',
            error_message = 'Codigo de asignatura no válido.')),
      Field('periodo', type ='string', length = 9,
            requires =  IS_IN_SET(PERIODOS, zero='Seleccione',
            error_message = 'Seleccione un periodo.')),
      Field('anio', type = 'integer',  length = 4,
            requires = [IS_INT_IN_RANGE(1967, 1e100,
                            error_message='El año debe ser un numero positivo \
                                de la forma YYYY a partir de 1967.'),
                        IS_LENGTH(4,
                            error_message ='El año debe ser de la forma YYYY.')
                        ]
            ),
      labels = {
          'codigo': 'Código',
          'periodo': 'Período',
          'anio': 'Año',
      },
      submit_button=T('Guardar'),
    )

    # if form.process().accepted:


    # transcription = db((db.TRANSCRIPCION.codigo == form.vars.codigo)&(db.TRANSCRIPCION.periodo == form.vars.periodo)&(db.TRANSCRIPCION.anio == form.vars.anio)).select().first()

    # if transcription:

    #   response.flash = "Una transcripción con el mismo código y la misma vigencia ya existe."

    # else:

    #     # new_transcription = db.TRANSCRIPCION.insert(original_pdf = file,
    #     #                             transcriptor = auth.user.username,
    #     #                             codigo = form.vars.codigo,
    #     #                             periodo = form.vars.periodo,
    #     #                             anio = form.vars.anio)

    #     # id = new_transcription.id
    #     # #Registro en la bitacora de la transcripcion
    #     # #regiter_in_transcriptions_journal(db, auth, id, 'CREACIÓN', 'Transcripción creada.')
    #     # #redirect(URL('edit',
    #     #             vars = dict(id = id , file = file,
    #     #                         file_type = file_type,
    #     #                         extract_type = extract_type)))



    return dict(message = message, form = form)







@auth.requires(auth.is_logged_in() and auth.has_permission('manage_ap_request') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def teachers():
    """
      Vista para un usuario que tiene el rol de profesor asignado.
    """

    message = "Profesores asignados"

    group_id = auth.id_group(role="TRANSCRIPTOR")

    all_transcriptors_for_user = db(db.REGISTRO_TRANSCRIPTORES.supervisor == auth.user.username)._select(db.REGISTRO_TRANSCRIPTORES.transcriptor)

    transcriptors = db(db.auth_user.username.belongs(all_transcriptors_for_user)).select(db.auth_user.id,
                                                                                         db.auth_user.username,
                                                                                         db.auth_user.first_name,
                                                                                         db.auth_user.last_name,
                                                                                         db.auth_user.email)
    lista_transcriptores = []
    # Obtenemos los roles de cada usuario
    for transcriptor in transcriptors:
        lista_transcriptores.append({'id' : transcriptor.id,
                                     'username': transcriptor.username,
                                     'name' : transcriptor.first_name + ' ' + transcriptor.last_name,
                                     'email': transcriptor.email})

    # formulario para nuevos transcriptores
    # se buscan por correo institucional, y se comprueba por medio de una expresion regular
    form = SQLFORM.factory(Field('correo', type="string",
                                  requires = IS_MATCH(r'\b([a-zA-Z0-9-]+@usb\.ve)\b',
                                  error_message = 'Correo no corresponde a un Correo Institucional.')),
                           labels={'correo':'Correo'})

    ## Formulario para colocar el mensaje.
    formulario_contactar = SQLFORM.factory(
                                Field('asunto', type="string", requires=[IS_LENGTH(50)]),
                                Field('mensaje', type="text", requires=[IS_NOT_EMPTY(error_message='El mensaje no puede estar vacío')]),
                                Field('userid', type="userid"),
                                submit_button = 'Enviar')

    if form.process(formname="formulario_agregar_transcriptor").accepted:
        email = form.vars.correo
        usuario = db(db.auth_user.email == email).select().first()
        if usuario:

            # chequeo sobre si el usuario puede transcribir.
            # primero revisamos que el usuario pertenece a un grupo que puede ser transcriptor
            puede_transcribir = True
            roles      = db(db.auth_membership.user_id == usuario.id).select(db.auth_membership.group_id)

            no_permitidos = [int(auth.id_group(role="DECANATO")),
                             int(auth.id_group(role="COORDINACION")),
                             int(auth.id_group(role="DEPARTAMENTO"))]

            # Revisamos que el usuario no tenga rol de decanato, Dpto, o Coord. para transcribir.
            for i in roles:
                if i['group_id'] in no_permitidos:
                    puede_transcribir = False
                    session.flash = 'Usuarios con rol de DECANATO, COORDINACION o DEPARTAMENTO no pueden transcribir programas.'

            # luego, revisamos si ya se encuentra transcribiendo para algun supervisor
            existente = db(db.REGISTRO_TRANSCRIPTORES.transcriptor == usuario.username).select()
            if existente:
                puede_transcribir = False
                session.flash = 'El Usuario ya es Transcriptor de otro DECANATO, COORDINACION o DEPARTAMENTO.'

            # finalmente, puede transcribir y se agrega a la lista de usuarios transcriptores
            if puede_transcribir:
                auth.add_membership(group_id, usuario.id)

                new_id = db.REGISTRO_TRANSCRIPTORES.insert(transcriptor = usuario.username,
                                                           supervisor   = auth.user.username)

                # registro en el log
                regiter_in_log(db, auth, 'ROL', 'Asignado rol TRANSCRIPTOR para el Usuario %s.'%(usuario.username))

                session.flash = 'Usuario agregado como Transcriptor.'

        else:
            session.flash = 'Usuario no encontrado.'

        redirect(URL(c='transcriptions',f='transcriptors'))
    elif form.errors:
        response.flash = 'No se pudo agregar al usuario.'

    if formulario_contactar.process(formname="formulario_contactar").accepted:
        userid = request.vars.userid
        asunto = request.vars.asunto
        mensaje = request.vars.mensaje

        ## Obtenemos el usuario al que deseamos contactar.
        usuario = db(db.auth_user.id == userid).select().first()

        enviar_correo_contacto(mail, usuario, asunto, mensaje)

        regiter_in_log(db, auth, 'CONTACTO', 'Envio de mensaje de contacto al Usuario %s.'%(usuario.username))

        session.flash = 'Correo enviado satisfactoriamente'
        redirect(URL(c='transcriptions', f='transcriptors'))

    if formulario_contactar.errors:
        response.flash = "No se pudo enviar el correo al Usuario."

    return dict(message = message,
                transcriptores = lista_transcriptores,
                group_id = group_id,
                form=form,
                formulario_contactar = formulario_contactar)


@auth.requires(auth.is_logged_in() and auth.has_permission('manage_ap_request') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def following():

    message = "Seguimiento de borradores de programa"

    return dict(message = message)

@auth.requires(auth.is_logged_in() and auth.has_permission('manage_ap_request') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def list_pending():

    message = "Borradores por revisión"

    return dict(message = message)

@auth.requires(auth.is_logged_in() and auth.has_permission('manage_ap_request') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def list_approved():

    message = "Borradores aprobados"

    return dict(message = message)
