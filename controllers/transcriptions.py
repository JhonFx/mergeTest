# -*- coding: utf-8 -*-
from wand.image         import Image
from PIL                import Image as Pi
from webservice_queries import *
from notifications      import *
from logs               import *
from formularios 	import *
from unidecode import unidecode
import os
import sys
import io
import re
import pyocr
import pyocr.builders
import StringIO

# Manejo de acentos en los PDFs
import codecs

# Tiempo
import time
import datetime
from datetime import date

# pymarc
from pymarc import marcxml

# Subprocess
import subprocess

# Multiprocessing
from multiprocessing import Process, Value, Array, Pool, Queue, Event

# Para el buscador de video
import urllib
import urllib2
import json
# Para el buscador de articulos
from scholar import *

# Librerias para realizar las citas
from citeproc.py2compat import *
from citeproc import CitationStylesStyle, CitationStylesBibliography
from citeproc import formatter
from citeproc import Citation, CitationItem
from citeproc.source.bibtex import BibTeX
from pybtex.database import BibliographyData, Entry
from citations import *

reload(sys)
sys.setdefaultencoding('utf-8')

API_KEY_YOUTUBE = "AIzaSyAkGlXfDzaEL2n2rL4qhR4_rpU8PpxyCVM"

@auth.requires(auth.is_logged_in() and auth.has_permission('manage_transcriptors', 'auth_user') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def transcriptors():
    """
      Vista para un usuario que puede asignar a otros roles de transcriptor.
    """

    message = "Transcriptores"

    group_id = auth.id_group(role="TRANSCRIPTOR")

    all_transcriptors_for_user = db(db.AFILIACION.supervisor == auth.user.username)._select(db.AFILIACION.transcriptor)

    transcriptors = db(db.auth_user.username.belongs(all_transcriptors_for_user)).select(db.auth_user.id,
                                                                                         db.auth_user.username,
                                                                                         db.auth_user.first_name,
                                                                                         db.auth_user.last_name,
                                                                                         db.auth_user.email)
    lista_transcriptores = []
    # Obtenemos los roles de cada usuario
    for transcriptor in transcriptors:
        lista_transcriptores.append({'id' : transcriptor.id,
                                     'username': transcriptor.username,
                                     'name' : transcriptor.first_name + ' ' + transcriptor.last_name,
                                     'email': transcriptor.email})

    # formulario para nuevos transcriptores
    # se buscan por correo institucional, y se comprueba por medio de una expresion regular
    form = SQLFORM.factory(Field('correo', type="string",
                                  requires = IS_MATCH(r'\b([a-zA-Z0-9-]+@usb\.ve)\b',
                                  error_message = 'Correo no corresponde a un Correo Institucional.')),
                           labels={'correo':'Correo'})

    ## Formulario para colocar el mensaje.
    formulario_contactar = SQLFORM.factory(
                                Field('asunto', type="string", requires=[IS_LENGTH(50)]),
                                Field('mensaje', type="text", requires=[IS_NOT_EMPTY(error_message='El mensaje no puede estar vacío')]),
                                Field('userid', type="userid"),
                                submit_button = 'Enviar')

    if form.process(fomname="formulario_agregar_transcriptor").accepted:
        email = form.vars.correo
        usuario = db(db.auth_user.email == email).select().first()
        if usuario:

            # chequeo sobre si el usuario puede transcribir.
            # primero revisamos que el usuario pertenece a un grupo que puede ser transcriptor
            puede_transcribir = True
            roles      = db(db.auth_membership.user_id == usuario.id).select(db.auth_membership.group_id)

            no_permitidos = [int(auth.id_group(role="DECANATO")),
                             int(auth.id_group(role="COORDINACION")),
                             int(auth.id_group(role="DEPARTAMENTO"))]

            # Revisamos que el usuario no tenga rol de decanato, Dpto, o Coord. para transcribir.
            for i in roles:
                if i['group_id'] in no_permitidos:
                    puede_transcribir = False
                    session.flash = 'Usuarios con rol de DECANATO, COORDINACION o DEPARTAMENTO no pueden transcribir programas.'

            # luego, revisamos si ya se encuentra transcribiendo para algun supervisor

            #afiliacion_a_supervisor = db(db.AFILIACION.transcriptor == usuario.username).select()
            #if afiliacion_a_supervisor:
        #        puede_transcribir = False
        #        session.flash = 'El Usuario ya es Transcriptor de otro DECANATO, COORDINACION o DEPARTAMENTO.'

            # finalmente, puede transcribir y se agrega a la lista de usuarios transcriptores
            if puede_transcribir:
                auth.add_membership(group_id, usuario.id)


                new_id = db.AFILIACION.insert(transcriptor = usuario.username,
                                              supervisor   = auth.user.username)

                # registro en el log
                regiter_in_log(db, auth, 'ROL', 'Asignado rol TRANSCRIPTOR para el Usuario %s.'%(usuario.username))

                session.flash = 'Usuario agregado como Transcriptor.'

        else:
            session.flash = 'Usuario no encontrado.'

        redirect(URL(c='transcriptions',f='transcriptors'))
    elif form.errors:
        response.flash = 'No se pudo agregar al usuario.'

    if formulario_contactar.process(formname="formulario_contactar").accepted:
        userid = request.vars.userid
        asunto = request.vars.asunto
        mensaje = request.vars.mensaje

        ## Obtenemos el usuario al que deseamos contactar.
        usuario = db(db.auth_user.id == userid).select().first()

        enviar_correo_contacto(mail, usuario, asunto, mensaje)

        regiter_in_log(db, auth, 'CONTACTO', 'Envio de mensaje de contacto al Usuario %s.'%(usuario.username))

        session.flash = 'Correo enviado satisfactoriamente'
        redirect(URL(c='transcriptions', f='transcriptors'))

    if formulario_contactar.errors:
        response.flash = "No se pudo enviar el correo al Usuario."

    return dict(message = message,
                transcriptores = lista_transcriptores,
                group_id = group_id,
                form=form,
                formulario_contactar = formulario_contactar)

@auth.requires(auth.is_logged_in() and auth.has_permission('manage_transcriptors', 'auth_user') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def deletetranscriptor():
    """
      Funcion encargada de la eliminacion de transcriptores.
    """

    idusuario = request.args(0)
    idrole    = request.args(1)

    # eliminamos el usuario como transcriptor para el solicitante
    usuario  = db(db.auth_user.id == idusuario).select().first()
    afiliacion = db((db.AFILIACION.transcriptor == usuario.username ) &
                    (db.AFILIACION.supervisor == auth.user.username )).select().first()

    db((db.AFILIACION.transcriptor == usuario.username ) &
       (db.AFILIACION.supervisor == auth.user.username )).delete()

    # Reasignamos las transcriptiones pendientes al usuario que realizo la eliminacion
    registros = db(db.REGISTRO_TRANSCRIPTORES.afiliacion == afiliacion).select()

    print("########REGISTROS##########")
    print(registros)

    db(db.REGISTRO_TRANSCRIPTORES.afiliacion == afiliacion).delete()

    for registro in registros:
       db((db.TRANSCRIPCION.transcriptor == usuario.username)
         &(db.TRANSCRIPCION.id == registro.transcripcion)).delete()

      # Registro en la bitacora de transcripcion
      #regiter_in_transcriptions_journal(db, auth, transcripcion['id'], 'REASIGNACIÓN', 'Transcripción reasignada automáticamente por eliminación de Transcriptor.')

    hay_otros_supervisores = db((db.AFILIACION.supervisor != auth.user.username)
                                &(db.AFILIACION.transcriptor == usuario.username)).select().first()
    print("Otros supervisores:")
    print(hay_otros_supervisores)
    if not(hay_otros_supervisores):
    # quitamos el permiso de transcripcion para el usuario
        auth.del_membership(idrole, idusuario)

    # registro en el log
    regiter_in_log(db, auth, 'ROL', 'Eliminado rol TRANSCRIPTOR para el Usuario %s.'%(usuario.username))

    session.flash = 'Usuario eliminado como Transcriptor.'
    redirect(URL(c='transcriptions',f='transcriptors'))

@auth.requires(auth.is_logged_in() and auth.has_permission('manage_transcriptors', 'auth_user') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def following():
    """
        Vista para listar las transcripciones a evaluar por un supervisor en proceso.
    """
    message = "Seguimiento de Transcripciones"

    # obtenemos los transcriptores asociadas al supervisor

    all_transcriptors_for_user = db(db.AFILIACION.supervisor == auth.user.username).select(db.AFILIACION.transcriptor)
    all_afiliaciones_for_user = db(db.AFILIACION.supervisor == auth.user.username).select()
    all_registros_for_user = db(db.REGISTRO_TRANSCRIPTORES.afiliacion.belongs(all_afiliaciones_for_user)).select()
    print "all_transcriptors_for_user"
    print all_transcriptors_for_user
    print "all_afiliaciones_for_user"
    print all_afiliaciones_for_user
    print "all_registros_for_user"
    print all_registros_for_user

    #for user in all_transcriptors_for_user:
    transcriptors = []
    users = db(db.auth_user).select(db.auth_user.id,
                                            db.auth_user.username,
                                            db.auth_user.first_name,
                                            db.auth_user.last_name,
                                            db.auth_user.email)
    for user in users:
        for trans in all_transcriptors_for_user:
            if user.username == trans.transcriptor:
                transcriptors.append(user)


    print "transcriptors"
    print transcriptors

    #transcriptors.insert(0, auth.user)


    groups_list = ""
    separator = False
    if auth.has_membership(auth.id_group(role="DEPARTAMENTO")):
      groups_list += "DEP"
      separator = True

    if auth.has_membership(auth.id_group(role="COORDINACION")):
      if separator:
        groups_list += ", "
      groups_list += "COORD"
      separator = True

    if auth.has_membership(auth.id_group(role="DECANATO")):
      if separator:
        groups_list += ", "
      groups_list += "DEC"
      separator = True

    if auth.has_membership(auth.id_group(role="DACE-OPERADOR")):
      if separator:
        groups_list += ", "
      groups_list += "DACE-OP"
      separator = True

    if auth.has_membership(auth.id_group(role="DACE-ADMINISTRADOR")):
      if separator:
        groups_list += ", "
      groups_list += "DACE-ADM"
      separator = True

    if not separator:
      groups_list += auth.user.username

    lista_transcriptores = []
    # Obtenemos los roles de cada usuario
    for transcriptor in transcriptors:
      transcripciones = db((db.TRANSCRIPCION.transcriptor == transcriptor.username) & (db.TRANSCRIPCION.estado == 'pendiente')).select(
        db.TRANSCRIPCION.id, db.TRANSCRIPCION.codigo, db.TRANSCRIPCION.transcriptor, db.TRANSCRIPCION.fecha_modificacion
      )

      lista_transcriptores.append({'id' : transcriptor.id,
                                 'username': transcriptor.username if transcriptor != auth.user else groups_list,
                                 'name' : transcriptor.first_name + ' ' + transcriptor.last_name,
                                 'email': transcriptor.email,
                                 'transcripciones': transcripciones
      })
    print ("lista_transcriptores")
    print (lista_transcriptores)
    # formulario para nuevas transcripciones
    # se buscan por correo institucional, y se comprueba por medio de una expresion regular
    form = SQLFORM.factory(Field('transcriptor', type="string",
                                  requires = IS_IN_SET([x['username'] + " - " + x['name'] for x in lista_transcriptores],
                                  error_message = 'Seleccione un Transcriptor.',
                                  zero = "Seleccione...")),
                          Field('codigo', type='string', length = 8,
                                requires = IS_MATCH('([A-Z]{2,2}[0-9]{4,4})|([A-Z]{3,3}[0-9]{3,3})',
                                error_message = 'Codigo de asignatura no válido.')),
                         labels={
                            'correo': 'Correo',
                            'codigo': 'Código'
                          })

    # formulario para reasignar transcripcion a otro transcriptor
    formulario_reasignar = SQLFORM.factory(Field('transcriptor', type="string",
                                                  requires = IS_IN_SET([x['username'] + " - " + x['name'] for x in lista_transcriptores],
                                                  error_message = 'Seleccione un Transcriptor.',
                                                  zero = "Seleccione...")),
                                           Field('transcription_id', type='string'),
                                           Field('comentario', type='text', requires=IS_LENGTH(512, error_message='Comentario muy extenso, resuma a menos de 512 caracteres.')),
                                                  labels={'transcriptor':'Nuevo Transcriptor',
                                                          'comentario': 'Comentario'})

    # Formulario para la reasignacion de Transcripciones
    if formulario_reasignar.process(formname="formulario_reasignar").accepted:
        print("************REASIGNANDO************")
        transcription_id = formulario_reasignar.vars.transcription_id
        username = formulario_reasignar.vars.transcriptor.split()[0]
        if "DEP" in username or "DEC" in username or "COORD" in username or "DACE" in username:
          nuevo_transcriptor = auth.user
        else:
          nuevo_transcriptor = db(db.auth_user.username == username).select().first()

        if nuevo_transcriptor:
          comentario = formulario_reasignar.vars.comentario

          # encontramos el usuario transcriptor
          supervisor = auth.user

          if nuevo_transcriptor == auth.user or [x for x in transcriptors if x['id'] == nuevo_transcriptor.id]:
            # buscamos la transcripcion a reasignar y la actualizamos
            db.TRANSCRIPCION[transcription_id] = dict(transcriptor = nuevo_transcriptor.username)
            afiliacion_id = db((db.AFILIACION.transcriptor == nuevo_transcriptor.username)
                              & (db.AFILIACION.supervisor == auth.user.username)).select(db.AFILIACION.id).first()
            print("La afiliacion ")
            print(afiliacion_id)
            registro_id = db(db.REGISTRO_TRANSCRIPTORES.transcripcion == transcription_id).select(db.REGISTRO_TRANSCRIPTORES.id).first()
            print("El registro:")
            print(registro_id)
            db.REGISTRO_TRANSCRIPTORES[registro_id.id] = dict(afiliacion = afiliacion_id.id)

            #encontramos la transcripcion
            transcription = db(db.TRANSCRIPCION.id == transcription_id).select().first()

            # Registro en la bitacora de transcripcion
            if comentario:
                regiter_in_transcriptions_journal(db, auth, transcription_id, 'REASIGNACIÓN', 'Transcripción reasignada a Usuario %s con comentario: %s'%(transcriptor, comentario))
            else:
                regiter_in_transcriptions_journal(db, auth, transcription_id, 'REASIGNACIÓN', 'Transcripción reasignada a Usuario %s.'%(transcriptor))

            enviar_correo_reasignacion_transcripcion(
              mail, nuevo_transcriptor, (transcription.codigo + ' ' + '' if transcription.denominacion is None else transcription.denominacion
            ), comentario, supervisor)

            session.flash = "Reasignación exitosa."
            redirect(URL(c='transcriptions', f='following'))
          else:
            session.flash = 'El Usuario no esta registrado como Transcriptor o no esta bajo la Supervision de este ente en el Sistema.'
            redirect(URL(c='transcriptions', f='following'))
        else:
          session.flash = 'Usuario no encontrado.'
          redirect(URL(c='transcriptions', f='following'))

    elif formulario_reasignar.errors:
        response.flash = 'Error en la reasignación, intente nuevamente.'

    # Se maneja el formulario de Asignar una nueva transcripcion
    if form.process(formname="formulario_asignar_transcripcion", onvalidation=add_code_form_processing).accepted:
      username = form.vars.transcriptor.split()[0]
      if "DEP" in username or "DEC" in username or "COORD" in username or "DACE" in username:
        usuario = auth.user
      else:
        usuario = db(db.auth_user.username == username).select().first()

      if usuario:
        # luego, revisamos si ya se encuentra transcribiendo para algun supervisor
        print("USUARIO:")
        print(usuario)
        afiliacion_existente = db((db.AFILIACION.transcriptor == usuario.username)
                                & (db.AFILIACION.supervisor == auth.user.username)).select().first()
        if afiliacion_existente:
          # registro en el log
          new_transcription = db.TRANSCRIPCION.insert(
                            transcriptor = usuario.username,
                            codigo = form.vars.codigo,
                            periodo = form.vars.periodo,
                            anio = form.vars.anio
                          )

          id = new_transcription.id
          new_registro = db.REGISTRO_TRANSCRIPTORES.insert(
                            transcripcion = id,
                            afiliacion = afiliacion_existente
                         )
          #Registro en la bitacora de la transcripcion
          regiter_in_transcriptions_journal(db, auth, id, 'CREACIÓN', 'Transcripción creada.')
          session.flash = 'Usuario agregado como Transcriptor.'
          redirect(URL(c='transcriptions', f='following'))
        else:
          session.flash = 'El usuario no es un Transcriptor en el sistema.'
          redirect(URL(c='transcriptions', f='following'))
      else:
        session.flash = 'Usuario no encontrado.'
        redirect(URL(c='transcriptions', f='following'))
    elif form.errors:
      response.flash = 'No se pudo asignar la nueva Transcripcion al Usuario.'

    return dict(message=message, transcriptores=lista_transcriptores, registros=all_registros_for_user, form=form, formulario_reasignar=formulario_reasignar)

@auth.requires(
  auth.is_logged_in() and (auth.has_permission('create_transcription'))
  and not(auth.has_membership(auth.id_group(role="INACTIVO")))
)
def add():
  return initialize()


@auth.requires(
  auth.is_logged_in() and (auth.has_permission('work_on_transcription'))
  and not(auth.has_membership(auth.id_group(role="INACTIVO")))
)
def initialize():
    """
      Función para empezar una nueva transcripción.
      En la vista se selecciona un archivo .pdf o imagen de un .pdf de una transcripcion
      para poder iniciar el proceso.
    """
    print("**************INITIALIZE*****************")
    id =  request.vars['id']
    if ( ( type(id).__name__)=="list" ):
      id = id[0]
    print "El id que llega a initialize es:"
    print id

    mensaje = 'Nueva Transcripción'
    all_departaments = db(
      (db.auth_user.id==db.auth_membership.user_id)&((db.auth_membership.group_id==auth.id_group(role="DEPARTAMENTO"))|
      (db.auth_membership.group_id==auth.id_group(role="COORDINACION"))|(db.auth_membership.group_id==auth.id_group(role="DECANATO"))|
      (db.auth_membership.group_id==auth.id_group(role="DACE-OPERADOR"))|(db.auth_membership.group_id==auth.id_group(role="DACE-ADMINISTRADOR")))
    )._select(db.auth_user.id)
    departaments = [x.username for x in db(db.auth_user.id.belongs(all_departaments)).select()]

    group_id = auth.id_group(role="TRANSCRIPTOR")
    tiene_supervisor = db((db.REGISTRO_TRANSCRIPTORES.transcripcion == id)
                       & (db.AFILIACION.transcriptor == auth.user.username)
                       & (db.REGISTRO_TRANSCRIPTORES.afiliacion == db.AFILIACION.id)).select(db.REGISTRO_TRANSCRIPTORES.afiliacion).first()
    print("TIENE_SUPERVISOR:")
    print(tiene_supervisor)
    print("auth.user.username")
    print(auth.user.username)

    if (auth.has_membership(auth.id_group(role="PROFESOR")) and (tiene_supervisor is None)):
      form = SQLFORM.factory(
        Field('supervisor', type ='string', requires = IS_IN_SET(departaments, zero='Seleccione',
                            error_message = 'Seleccione un departamento.')),
        Field('file', 'upload', label = 'Archivo PDF',
                                uploadfolder=os.path.join(request.folder,'static/transcriptions/originalpdf/'),
                                requires = [IS_NOT_EMPTY(error_message='Seleccione un archivo.'),
                                            IS_UPLOAD_FILENAME(extension='pdf', error_message='Seleccione un archivo en formato PDF.')]),
        Field('file_type', 'select' , label = 'Tipo de Archivo',
                                      requires = IS_IN_SET(['Texto', 'Imagen'],
                                                 zero='Seleccione una opción...',
                                                 error_message='Seleccione un tipo de archivo.'),
                                      default='Texto'),
        Field('extract_type', 'select' , label = 'Extraer',
                                         requires = IS_IN_SET(['Solo Texto', 'Código y Denominación'],
                                                    zero='Seleccione una opción...',
                                                    error_message='Seleccione el tipo de extracción.'),
                                         default='Solo Texto'),
        formstyle = 'bootstrap3_stacked',
        col3 = {'file_type':'Seleccione "Texto" si el programa a transcribir proviene de un pdf de texto. Si es un programa escaneado, seleccione "Imagen".',
                'extract_type': 'Si desea detectar automáticamente el Código y el nombre de la asignatura seleccione "Código y Denominación", sino seleccione "Solo Texto".' }
      )
    else:
      # Se selecciona en el formulario el tipo de archivo a cargar.
      form = SQLFORM.factory(
        Field('file', 'upload', label = 'Archivo PDF',
                                uploadfolder=os.path.join(request.folder,'static/transcriptions/originalpdf/'),
                                requires = [IS_NOT_EMPTY(error_message='Seleccione un archivo.'),
                                            IS_UPLOAD_FILENAME(extension='pdf', error_message='Seleccione un archivo en formato PDF.')]),
        Field('file_type', 'select' , label = 'Tipo de Archivo',
                                      requires = IS_IN_SET(['Texto', 'Imagen'],
                                                 zero='Seleccione una opción...',
                                                 error_message='Seleccione un tipo de archivo.'),
                                      default='Texto'),
        Field('extract_type', 'select' , label = 'Extraer',
                                         requires = IS_IN_SET(['Solo Texto', 'Código y Denominación'],
                                                    zero='Seleccione una opción...',
                                                    error_message='Seleccione el tipo de extracción.'),
                                         default='Solo Texto'),
        formstyle = 'bootstrap3_stacked',
        col3 = {'file_type':'Seleccione "Texto" si el programa a transcribir proviene de un pdf de texto. Si es un programa escaneado, seleccione "Imagen".',
                'extract_type': 'Si desea detectar automáticamente el Código y el nombre de la asignatura seleccione "Código y Denominación", sino seleccione "Solo Texto".' }
      )

    if form.process().accepted:
      if form.vars.supervisor:

        afiliaciones = (db((db.AFILIACION.transcriptor == auth.user.username) & (db.AFILIACION.supervisor == form.vars.supervisor)).select())
        print "db.AFILIACION.transcriptor"
        print db.AFILIACION.transcriptor
        print "auth.user.username"
        print auth.user.username
        print("afiliaciones")
        print(afiliaciones)
        afiliacion = afiliaciones.first()
        registros = db((db.REGISTRO_TRANSCRIPTORES.afiliacion.belongs(afiliaciones)) & (db.REGISTRO_TRANSCRIPTORES.transcripcion == id)).select()
        print("LOS REGISTROS QUE CALCULE SON:")
        print(registros)

        if not(afiliacion):
          print("No había afiliacion")
          # Agregamos como transcriptor de ese departamento al profesor en caso de no existir dicha relacion en la DB
          auth.add_membership(group_id, auth.user.id)
          new_id = db.AFILIACION.insert(transcriptor = auth.user.username,
                                        supervisor = form.vars.supervisor)
          print("Hice la afiliacion "+str(new_id))
        else:
          new_id = afiliacion.id
          print("Ya habia una afiliacion, la "+str(new_id))

        # registro en el log
        regiter_in_log(db, auth, 'ROL', 'Asignado rol TRANSCRIPTOR para el Usuario %s.'%(auth.user.username))
        session.flash = 'Usuario agregado como Transcriptor.'

        new_registro = db.REGISTRO_TRANSCRIPTORES.insert(
                            transcripcion = id,
                            afiliacion = new_id
                            )
        redirect(URL('add_code_date',
                      vars = dict(file=form.vars.file,
                      file_type=form.vars.file_type,
                      extract_type=form.vars.extract_type,
                      id=id,
                      new_registro = new_registro)))
      else:
        redirect(URL('add_code_date',
                      vars = dict(file=form.vars.file,
                      file_type=form.vars.file_type,
                      extract_type=form.vars.extract_type,
                      id=id,
                      new_registro = None)))
    return dict(message = mensaje, form = form)


def add_code_form_processing(form):
  try:
    page = urllib2.urlopen(settings.dace_ws_url + '/asignaturas/%s/'%(form.vars.codigo)).read()
    subject = json.loads(page)
    if len(subject) <= 0:
      form.errors.codigo = 'El codigo del programa transcrito debe existir.'
  except urllib2.URLError as e:
    response.flash = 'Error de conexión con el Web Service.'
    form.errors.codigo = 'El codigo del programa transcrito debe existir.'


@auth.requires(
  auth.is_logged_in() and (auth.has_permission('create_transcription') or auth.has_permission('manage_transcription') or auth.has_permission('work_on_transcription'))
  and not(auth.has_membership(auth.id_group(role="INACTIVO")))
)
def add_code_date():

  file = request.vars["file"]
  file_type = request.vars["file_type"]
  extract_type = request.vars["extract_type"]

  id =  request.vars['id']
  if ( ( type(id).__name__)=="list" ):
    id = id[0]

  pdfurl = URL('static','transcriptions/originalpdf/' + file)

  message = "Código y entrada en vigencia de la transcripción"

  if not id or id == "None":
    form = SQLFORM.factory(
      Field('codigo', type='string', length = 8,
            requires = IS_MATCH('([A-Z]{2,2}[0-9]{4,4})|([A-Z]{3,3}[0-9]{3,3})',
            error_message = 'Codigo de asignatura no válido.')),
      Field('periodo', type ='string', length = 9,
            requires =  IS_IN_SET(PERIODOS, zero='Seleccione',
            error_message = 'Seleccione un periodo.')),
      Field('anio', type = 'integer',  length = 4,
            requires = [IS_INT_IN_RANGE(1967, 1e100,
                            error_message='El año debe ser un numero positivo \
                                de la forma YYYY a partir de 1967.'),
                        IS_LENGTH(4,
                            error_message ='El año debe ser de la forma YYYY.')
                        ]
            ),
      labels = {
          'codigo': 'Código',
          'periodo': 'Período',
          'anio': 'Año',
      },
      submit_button=T('Guardar'),
    )

    if form.process(onvalidation=add_code_form_processing).accepted:
      transcription = db((db.TRANSCRIPCION.codigo == form.vars.codigo)&(db.TRANSCRIPCION.periodo == form.vars.periodo)&(db.TRANSCRIPCION.anio == form.vars.anio)).select().first()

      if transcription:
        response.flash = "Una transcripción con el mismo código y la misma vigencia ya existe."
      else:
          new_transcription = db.TRANSCRIPCION.insert(original_pdf = file,
                                      transcriptor = auth.user.username,
                                      codigo = form.vars.codigo,
                                      periodo = form.vars.periodo,
                                      anio = form.vars.anio)

          id = new_transcription.id
          new_registro = request.vars['new_registro']
          print (new_registro)

          db.REGISTRO_TRANSCRIPTORES[new_registro] = dict(transcripcion = new_transcription)

          #Registro en la bitacora de la transcripcion
          regiter_in_transcriptions_journal(db, auth, id, 'CREACIÓN', 'Transcripción creada.')
          redirect(URL('edit',
                      vars = dict(id = id , file = file, previous_page = 'list',
                                  file_type = file_type,
                                  extract_type = extract_type)))
  else:
    form = SQLFORM.factory(
        Field('periodo', type ='string', length = 9,
              requires =  IS_IN_SET(PERIODOS, zero='Seleccione',
              error_message = 'Seleccione un periodo.')),
        Field('anio', type = 'integer',  length = 4,
              requires = [IS_INT_IN_RANGE(1967, 1e100,
                              error_message='El año debe ser un numero positivo \
                                  de la forma YYYY a partir de 1967.'),
                          IS_LENGTH(4,
                              error_message ='El año debe ser de la forma YYYY.')
                          ]
              ),
        labels = {
            'periodo': 'Período',
            'anio': 'Año',
        },
        submit_button=T('Guardar'),
      )
    if form.process().accepted:
      # Actualizamos la transcripcion con la informacion indicada
      db.TRANSCRIPCION[id] = dict(
          original_pdf = file,
          periodo = form.vars.periodo,
          anio = form.vars.anio,
          fecha_modificacion=datetime.date.today()
      )

      #Registro en la bitacora de la transcripcion
      regiter_in_transcriptions_journal(db, auth, id, 'MODIFICACIÓN', 'Transcripción modificada.')
      redirect(URL('edit',
                      vars = dict(id = id , file = file, previous_page = 'list',
                                  file_type = file_type,
                                  extract_type = extract_type)))
    elif form.errors:
      response.flash = 'Hay errores en el formulario.'

  return dict(message = message, form = form, pdfurl = pdfurl)




@auth.requires(
  auth.is_logged_in() and (auth.has_permission('create_transcription') or auth.has_permission('work_on_transcription') or auth.has_permission('manage_transcription'))
  and not(auth.has_membership(auth.id_group(role="INACTIVO")))
)
def delete_aditional_field():
    """
        Borra el campo adicional agregado.
    """

    transid  = request.args(0)
    id_campo = request.args(1)
    db(db.CAMPOS_ADICIONALES_TRANSCRIPCION.id == id_campo).delete()

    session.flash = "Campo Adicional eliminado exitosamente."
    redirect(URL(c='transcriptions',f='edit', vars=dict(id=transid)))

@auth.requires(
  auth.is_logged_in() and (auth.has_permission('create_transcription') or auth.has_permission('work_on_transcription') or auth.has_permission('manage_transcription'))
  and not(auth.has_membership(auth.id_group(role="INACTIVO")))
)
def delete_reference():
    """
        Borra referencia agregada.
    """

    transid  = request.args(0)
    id_reference = request.args(1)

    db(db.LIBROS_TRANSCRIPCIONES.id == id_reference).delete()

    session.flash = "Referencia eliminada exitosamente."
    redirect(URL(c='transcriptions',f='edit', vars=dict(id=transid)))


def verify_credits_and_hours(creditos,horas_teoria,horas_practica,horas_laboratorio,anio,periodo):
  # Se hace la revisión a aquellos planes de estudio que entraron
  # en vigencia luego de Julio de 2009
  total_creditos = 0

  creditos          = int(creditos)
  horas_teoria      = int(horas_teoria)
  horas_practica    = int(horas_practica)
  horas_laboratorio = int(horas_laboratorio)
  anio              = int(anio)


  if (anio > 2009 or (anio == 2009 and (periodo == 'SEP-DIC' or periodo == 'OCT_FEB') ) ):

    #Se suman las horas de teoria al total de créditos
    if (horas_teoria <= 5):
      total_creditos = total_creditos + horas_teoria

    #Se suman las horas de práctica al total de créditos
    if (horas_practica >= 3 and horas_practica <= 5):
      total_creditos = total_creditos + 1

    #Se suman las horas de laboratorio al total de créditos
    if (horas_laboratorio == 2):
      total_creditos = total_creditos + 1

    elif (horas_laboratorio == 3 or horas_laboratorio == 4):
      total_creditos = total_creditos + 2

    elif (horas_laboratorio >= 5 and horas_laboratorio <=7):
      total_creditos = total_creditos + 3

    elif (horas_laboratorio >= 8 and horas_laboratorio <= 11):
      total_creditos = total_creditos + 4

    elif (horas_laboratorio == 12):
      total_creditos = total_creditos + 5

    #Se verifica si el número de créditos corresponde al
    #número de créditos acumulados.
    if (creditos!=total_creditos):
      return False

  return True


@auth.requires(
  auth.is_logged_in() and ((auth.has_permission('create_transcription') or auth.has_permission('work_on_transcription')) or auth.has_permission('manage_transcription'))
  and not(auth.has_membership(auth.id_group(role="INACTIVO")))
)
def edit():
    """
      Función para editar la planilla de transcripción.
    """

    id =  request.vars['id']
    previous_page = request.vars['previous_page']
    if ( ( type(id).__name__)=="list" ):
      id = id[0]
    print("ID QUE LLEGA A EDIT ES: ")
    print(id)
    print("PREVIOUS PAGE QUE LLEGA A EDIT ES: ")
    print(previous_page)

    transcription = db(db.TRANSCRIPCION.id == id).select().first()

    print(transcription.estado)
    if  (auth.has_membership(auth.id_group(role="COORDINACION")) or
         auth.has_membership(auth.id_group(role="DEPARTAMENTO")) or
         auth.has_membership(auth.id_group(role="DECANATO"))     or
         auth.has_membership(auth.id_group(role="DACE-OPERADOR"))or
         auth.has_membership(auth.id_group(role="DACE-ADMINISTRADOR"))
         ) and (transcription.estado == 'pendiente') and (not(auth.has_membership(auth.id_group(role="PROFESOR")))):
      print "No puede editar esta Transcripción hasta que no sea remitida para evaluar su aprobación."
      redirect(URL(c='transcriptions',f='following'))

    pdfurl = transcription.original_pdf
    code   = transcription.codigo
    text   = transcription.texto

    # Tipos de fuentes web
    TIPOS_FUENTES_WEB = (
        ("fuente_web1", "Cursos Online, blog, portal web"),
        ("fuente_web2", "Items de: Cursos Online, blog o portal web"),
        ("otros", "Otros")
    )

    if not pdfurl:
      redirect(URL(c='transcriptions',f='initialize', vars=dict(id=id)))

    # Extrae el texto según el tipo de archivo cargado para transcribir
    if 'file_type' in request.vars:
        if request.vars['file_type'] == "Texto":

            text = extract_text(os.path.join(request.folder,'static/transcriptions/originalpdf',request.vars['file']))
        elif request.vars['file_type'] == "Imagen":
            text = extract_text_from_image(os.path.join(request.folder,'static/transcriptions/originalpdf',request.vars['file']))

        db.TRANSCRIPCION[id] = dict(texto = text)

    # Se extrae en código y departamento del texto para llenar automáticamente en la forma.
    if 'extract_type' in request.vars:
        if request.vars['extract_type'] == "Código y Denominación":
            code = match_codigo_asig(text)
            db.TRANSCRIPCION[id] = dict(codigo = code)

            # hacemos la busqueda de la asignatura, si es posible
            details = subject_details(code,settings.dace_ws_url)
            if details:
                db.TRANSCRIPCION[id] = dict(denominacion = details['nombre'])

    pdfurl = URL('static','transcriptions/originalpdf/' + pdfurl)

    # Campos por defecto en la forma de transcripción.
    transcription_form = SQLFORM(db.TRANSCRIPCION,
                   record = id,
                   fields = ['codigo', 'denominacion', 'fecha_elaboracion',
                             'periodo', 'horas_teoria', 'horas_practica',
                             'horas_laboratorio' , 'creditos', 'anio',
                             'periodo_hasta', 'anio_hasta',
                             'sinopticos','ftes_info_recomendadas',
                             'estrategias_met','estrategias_eval',
                             'observaciones','objetivos_generales','objetivos_especificos'],
                   submit_button=T('Guardar')
                   )

    # obtenemos los campos adicionales, si existen
    campos_adicionales = db(db.CAMPOS_ADICIONALES_TRANSCRIPCION.transcripcion == transcription).select()

    # Obtenemos las referencias de la transcripcion, si existen
    referencias_transcripcion = []
    libros_transcripcion = db(db.LIBROS_TRANSCRIPCIONES.transcripcion == transcription).select()
    for r in libros_transcripcion:
        referencias_transcripcion.append(r)
    videos_transcripcion = db(db.VIDEOS_TRANSCRIPCIONES.transcripcion == transcription).select()
    for r in videos_transcripcion:
        referencias_transcripcion.append(r)
    articulos_transcripcion = db(db.ARTICULOS_TRANSCRIPCIONES.transcripcion == transcription).select()
    for r in articulos_transcripcion:
        referencias_transcripcion.append(r)
    fuentes_web_transcripcion = db(db.REFERENCIAS_WEB_TRANSCRIPCIONES.transcripcion == transcription).select()
    for r in fuentes_web_transcripcion:
        referencias_transcripcion.append(r)
    fuentes_otros_transcripcion = db(db.REFERENCIAS_OTROS_TRANSCRIPCIONES.transcripcion == transcription).select()
    for r in fuentes_otros_transcripcion:
        referencias_transcripcion.append(r)


    #-----------------BUSCAR LIBROS---------------#
    # formulario para el buscador de libros
    new_field_form_books = form_buscador_libros()
    # procesamiento del formulario para la busqueda de libros
    procesar_form_buscador_libros(new_field_form_books,id,text,previous_page)

    #-----------------BUSCAR VIDEOS---------------#
    # formulario para el buscador de vídeos
    new_field_form_video = form_buscador_videos()
    # procesamiento del formulario para la busqueda de videos
    procesar_form_buscador_videos(new_field_form_video,id,text,previous_page)

    #-----------------BUSCAR ARTICULOS---------------#
    # formulario para el buscador de articulos
    new_field_form_journal = form_buscador_articulos()
    # procesamiento del formulario para la busqueda de articulo
    procesar_form_buscador_articulos(new_field_form_journal,id,text,previous_page)

    #-----------------AÑADIR FUENTE WEB---------------#
    # formulario para las fuentes web
    new_field_form_fuentes_web = form_anadir_fuentes_web()
    # Procesamiento para agregar referencias web
    procesar_form_anadir_fuentes_web(new_field_form_fuentes_web,id,previous_page)

    #-----------------AÑADIR OTRAS FUENTES---------------#
    # formulario para otras fuentes de información
    new_field_form_otras_fuentes = form_anadir_otras_fuentes()
    # Procesamiento para agregar otro tipo de referencias
    procesar_form_anadir_otras_fuentes(new_field_form_otras_fuentes,id,db,previous_page)

    #------------------EDITAR OTRAS FUENTES------------------#
    # formulario para editar otras fuentes
    reference_edit_otras_fuentes_form = form_editar_otras_fuentes()
    #procesamiento para editar otras fuentes
    procesar_form_editar_otras_fuentes(id,db,reference_edit_otras_fuentes_form,previous_page)

    #----------------COMENTAR REFERENCIAS----------------#
    # formulario para comentar las referencias
    new_field_comentario_referencia = form_anadir_comentario_a_referencia()
    # Procesamiento para agregar comentarios a las referencias
    procesar_form_anadir_comentario_a_referencia(new_field_comentario_referencia,id,previous_page)

    #---------------AÑADIR CAMPO ADICIONAL---------------#
    # formulario para el nuevo campo adicional
    new_field_form = form_anadir_campo_adicional(db)
    #procesamiento del formulario para una nuevo campo adicional
    procesar_form_anadir_campo_adicional(id,db,new_field_form,transcription,previous_page)

    #---------------EDITAR CAMPO ADICIONAL---------------#
    # formulario para editar campos adicionales
    edit_field_form = form_editar_campo_adicional()
    # procesamiento para editar campos adicionales
    procesar_form_editar_campo_adicional(id,db,edit_field_form,previous_page)

    #---------------EDITAR REFERENCIA VIDEO---------------#
    # formulario para editar videos
    reference_edit_field_form = form_editar_ref_video()
    # procesamiento para editar referencias (Videos)
    process_form_editar_ref_video(id,db,reference_edit_field_form,previous_page)

    #---------------EDITAR REFERENCIA LIBRO---------------#
    # formulario para editar libros
    reference_edit_book_form = form_editar_ref_libros()
    #procesamiento para editar referencias (Libros)
    procesar_form_editar_ref_libros(id,db,reference_edit_book_form,previous_page)

    #---------------EDITAR REFERENCIA ARTICULO MINIMO---------------#
    # formulario para editar artículo (en forma minima)
    reference_edit_article_form = form_editar_ref_articulo_minimo()
    # procesamiento para editar referencias (Articulos mínimos)
    procesar_form_editar_ref_articulo_minimo(id,db,reference_edit_article_form,previous_page)

    #---------------EDITAR FUENTE WEB (CONTENEDOR)---------------#
    # formulario para agregar fuentes web (MOOC, blogs, websites)
    reference_edit_fuente_web_1_form = form_editar_fuente_web_1()
    #procesamiento para editar referencias web tipo contenedor
    procesar_form_editar_fuente_web_1(id,db,reference_edit_fuente_web_1_form,previous_page)

    #------------------EDITAR FUENTE WEB (ITEM)------------------#
    # formulario para agregar fuentes web (items de: MOOC, blogs, websites)
    reference_edit_fuente_web_2_form = form_editar_fuente_web_2()
    # procesamiento para editar fuente web tipo item
    procesar_form_editar_fuente_web_2(id,db,reference_edit_fuente_web_2_form,previous_page)

    #------------------EDITAR FUENTE WEB (OTROS)------------------#
    # formulario para agregar fuentes web (otros)
    reference_edit_fuente_web_otros_form = form_editar_fuente_web_otros()
    # procesamiento para agregar fuentes web (otros)
    procesar_form_editar_fuente_web_otros(id,db,reference_edit_fuente_web_otros_form,previous_page)

    #------------------MODIFICAR ARTICULOS COMPLETOS------------------#
    # formulario para modificar articulos (version completa)
    full_article_form = form_editar_articulo_completo()
    # procesamiento para editar referencias (Articulos completos)
    procesar_form_editar_articulo_completo(id,db,full_article_form,previous_page)

    #------------------RECHAZAR FORMULARIO------------------#
    # formulario para modificar articulos (version completa)
    reject_transcription_form = form_rechazar_transcripcion()
    # procesamiento para rechazar una transcripcion
    if reject_transcription_form.process(formname = "reject_transcription_form").accepted:

        db.TRANSCRIPCION[id] = dict(estado = 'pendiente')
        current.session.flash = "Transcripción rechazada exitosamente."
        # comentarios
        comentario = reject_transcription_form.vars.comentario

        # obtenemos el usuario para enviar la notificaciones
        usuario = db(db.auth_user.username == transcription.transcriptor).select().first()
        supervisor = auth.user
        enviar_correo_rechazo_transcripcion(
          mail, usuario, (transcription.codigo + ' ' + '' if transcription.denominacion is None else transcription.denominacion
        ), comentario, supervisor)

        #Registro en la bitacora de la transcripcion
        if comentario:
            regiter_in_transcriptions_journal(db, auth, id, 'RECHAZO', 'Transcripción rechazada con comentario: %s.'%(comentario))
        else:
            regiter_in_transcriptions_journal(db, auth, id, 'RECHAZO', 'Transcripción rechazada.')

        redirect(URL(c='transcriptions',f='list_pending'))

    elif reject_transcription_form.errors:
        response.flash = 'No se pudo rechazar la Transcripción.'

    # Procesamiento del formulario de la transcripcion
    if transcription_form.accepts(request, session, hideerror=True, keepvalues = True, formname = "transcription_form"):

        creditos     = transcription_form.vars.creditos
        horas_teoria = transcription_form.vars.horas_teoria
        horas_practica = transcription_form.vars.horas_practica
        horas_laboratorio = transcription_form.vars.horas_laboratorio
        anio = transcription_form.vars.anio
        periodo = transcription_form.vars.periodo

        creditos_horas_correcto = verify_credits_and_hours(creditos,horas_teoria,
                                                          horas_practica,horas_laboratorio,anio,periodo)

        print("Creditos correctos",creditos_horas_correcto)

        if (creditos_horas_correcto):
          response.flash = 'Transcripción guardada satisfactoriamente.'
          #Registro en la bitacora de la transcripcion
          regiter_in_transcriptions_journal(db, auth, id, 'MODIFICACIÓN', 'Transcripción modificada.')

        else:
          response.flash = 'La carga acádemica no corresponde con el número de créditos. Ajuste el formulario.'

    elif transcription_form.errors:
        session.flash = 'Hay errores en el formulario'

    return dict(text=text,
                pdfurl=pdfurl,
                code=code,
                id = id,
                previous_page = previous_page,
                campos_adicionales = campos_adicionales,
                referencias_transcripcion = referencias_transcripcion,
                transcription_form = transcription_form,
                new_field_form = new_field_form,
                new_field_form_otras_fuentes = new_field_form_otras_fuentes,
                new_field_form_journal = new_field_form_journal,
                new_field_form_books = new_field_form_books,
                new_field_form_video = new_field_form_video,
                new_field_form_fuentes_web = new_field_form_fuentes_web,
                edit_field_form = edit_field_form,
                full_article_form = full_article_form,
                reference_edit_book_form = reference_edit_book_form,
                reference_edit_field_form = reference_edit_field_form,
                reference_edit_article_form = reference_edit_article_form,
                reference_edit_fuente_web_1_form = reference_edit_fuente_web_1_form,
                reference_edit_fuente_web_2_form = reference_edit_fuente_web_2_form,
                reference_edit_fuente_web_otros_form = reference_edit_fuente_web_otros_form,
                reference_edit_otras_fuentes_form    = reference_edit_otras_fuentes_form,
                new_field_comentario_referencia = new_field_comentario_referencia,
                reject_transcription_form = reject_transcription_form,
                transcription_estado = transcription.estado)

@auth.requires(
  auth.is_logged_in() and (auth.has_permission('create_transcription') or auth.has_permission('work_on_transcription') or auth.has_permission('manage_transcription'))
  and not(auth.has_membership(auth.id_group(role="INACTIVO")))
)
def listarLibros():

  """
    Función encargada de buscar los libros en la Biblioteca del Congreso.
  """

  def generate_list(records):
    lista_records = []

    # Obtenemos la lista de libros arrojadas por la Libreria del Congreso
    for i in records:

        titulo = ''
        autor = ''
        coautores = ''
        colectivo = ''
        isbn = ''
        year = ''
        publisher = ''
        edicion = ''

        if i:
          # Según la referencia: https://www.loc.gov/marc/bibliographic/bd008.html
          # el campo 008 contiene la información del tipo de record su idioma.

          # Según la referencia: https://www.loc.gov/marc/bibliographic/bd008b.html
          # en el `leader field` se dicen los caracteres que representan a un libro.

          # El idioma son los caracteres desde el 35 hasta el 37, ambos incluidos.
          idioma_slice = slice(35,38)
          # El tipo son los caracteres 6 y 7, ambos incluidos.
          tipo_slice = slice(6,8)
          # Idiomas aceptados: español, inglés, italiano, alemán y francés.
          idiomas_aceptados = {'spa','eng','ita','ger','fre'}

          # Se extrae el tipo en el leader.
        #   import pdb
        #   pdb.set_trace()
          tipo_record = i.leader.encode('utf-8')[tipo_slice]

          # Se obtiene el idioma en el 008, que es de control.
          # Pymarc posee el atributo `data` para acceder al valor de los campos
          # de control.
          idioma = i['008'].data.encode('utf-8')[idioma_slice]

          # Los coautores se listan con el 700
          # NOTA: es posible que este campo incluya otros tipos de personas que contribuyeron al
          # libro, como editores o ilustradores. El estandar no explica como diferenciarlos
          coautores = "; ".join(x.format_field() for x in i.get_fields('700'))

          # El tipo son los caracteres que van desde
          if (i.title()):

            titulo = i.title().encode('utf-8')
          # El campo 100 tiene el autor principal. Los campos 110 y 111 guardan autores colectivos
          if i['100']:
            autor = i['100'].format_field()

          if i['110']:
              colectivo = i['110'].format_field()
          elif i['111']:
              colectivo = i['111'].format_field()

          if (i.isbn()):

            isbn = i.isbn().encode('utf-8')

          if (i.pubyear()):

            year = i.pubyear().encode('utf-8')

          if (i.publisher()):

            publisher = i.publisher().encode('utf-8')


          lista_records.append({'titulo'    : titulo,
                                'autor'     : autor,
                                'coautores' : coautores,
                                'colectivo' : colectivo,
                                'isb'       : isbn,
                                'year'      : year,
                                'publisher' : publisher,
                                'idioma'    : idioma,
                                'tipo'      : tipo_record,
                                })

    # Filtrar los elementos de la lista por idioma.
    lista_records = filter((lambda record: record['idioma'] in idiomas_aceptados), lista_records)
    # Filtrar los elementos por el tipo.
    lista_records = filter((lambda record: record['tipo'][0] in {'a','t'} and record['tipo'][1] in {'a','c','d','m'} ), lista_records)
    return lista_records

  def generate_query_url(titulo, autores, edicion, anio, otros = None):
    """
    Genera un URL para hacer una búsqueda de una referencia en la Librería del
    Congreso.

    Según la documentación de CQL, un query se compone de varias entradas separadas
    por operadores booleanos, específicamente: and, or y prox. El query se termina
    cuando consigue el operador &.

    Para separar los elementos del URL se utiliza &.

    """
    version = "1.2"
    operation = "searchRetrieve"
    server = settings.loc_service_url
    maximumRecords = 10
    query_comenzada = False
    query = ''

    if len(titulo) > 0:
      query += 'dc.title="{}"'.format(titulo)
      query_comenzada = True

    if len(autores) > 0 and query_comenzada:
      for autor in autores.split(","):
        query += '&dc.author="{}"'.format(autor)
    elif len(autores) > 0 and not(query_comenzada):
      autores_separados = autores.split(",")
      num_autores_separados = len(autores_separados)
      query += 'dc.author="{}"'.format(autores_separados[0])
      for i in range(1,num_autores_separados):
        query += '&dc.author="{}"'.format(autores_separados[i])
      query_comenzada = True

    if len(anio) > 0 and query_comenzada:
      query += '&({})'.format(anio)
    elif len(anio) > 0 and not(query_comenzada):
      query += '({})'.format(anio)
      query_comenzada = True

    if len(edicion) > 0 and query_comenzada:
      if edicion == "1":
        pass
      elif edicion == "2":
        query += '&(2d ed)'
      elif edicion == "3":
        query += '&(3d ed)'
      else:
        query += '&({}th ed)'.format(edicion)

    if (len(edicion) > 0) and not(query_comenzada):
      if edicion == "1":
        pass
      elif edicion == "2":
        query += '(2d ed)'
      elif edicion == "3":
        query += '(3d ed)'
      else:
        query += '({}th ed)'.format(edicion)
      query_comenzada = True

    if otros is not None:
      query += '&({})'.format(otros)

    if query:
      return "{server}version={version}&operation={operation}&query={query}&maximumRecords={maximumRecords}".format(
        server=server,
        version=version,
        operation=operation,
        query=query,
        maximumRecords=maximumRecords)

    else:
      return query

  #----------------------------------------------------------------------------#

  def process_consultar_libreria(e,query_url_arg, records_arg):
    q = query_url_arg.get()
    print "Empece a cargar con"
    print q
    r = marcxml.parse_xml_to_array(q)
    respuesta = r

    print "Termine de cargar"
    records_arg.put(respuesta)
    e.set()

  def prueba_tiempo():
    i = 0
    while True:
      print i
      i = i + 1 

  #----------------------------------------------------------------------------#


  id           =  request.vars['id']
  vistaRetorno = request.vars['vistaRetorno']
  text         = request.vars['text']
  previous_page = request.vars['previous_page']
  print("previous_page en listarLibros:")
  print(previous_page)

  if ( ( type(id).__name__)=="list" ):
    id = id[0]

  message = "Seleccione los libros correspondientes."

  query_url = generate_query_url(
    titulo=request.vars['titulo'],
    autores=request.vars['autores'],
    edicion=request.vars['edicion'],
    anio=request.vars['anio'],
    otros=None
  )

  # formulario para agregar libros
  reference_form = SQLFORM.factory(
          Field('autor',     type='string'),
          Field('coautores', type="string"),
          Field('colectivo', type="string"),
          Field('isbn',      type='string'),
          Field('titulo',    type='string'),
          Field('fecha', type = 'integer',  length = 4,
                requires = [IS_INT_IN_RANGE(0, 1e100,
                                error_message='El año debe ser un numero positivo \
                                    de la forma YYYY a partir de 1967.'),
                            IS_LENGTH(4,
                                error_message ='El año debe ser de la forma YYYY.')
                            ]),
          Field('editorial', type='string'),
          Field('book_comment',type='text'),
          Field('edition_comment',type='string'),
          Field('open_access', type='boolean'),
          Field('book_url',type='string'),
          labels = {
              'autor'     : 'Autor principal \n (Apellido, nombre)',
              'coautores' : 'Coautores',
              'colectivo' : 'Autor institucional (Nombre o abreviación de la Institución)',
              'titulo'    : 'Título',
              'fecha'     : 'Año de publicación',
              'isbn'      : 'ISBN',
              'editorial' : 'Editorial',
              'book_comment': 'Comentario del libro',
              'edition_comment': 'Comentario de la edición',
              'open_access': 'Acceso abierto',
              'book_url': 'Dirección web del libro' },
          submit_button=T('Crear referencia')
          )


  # procesamiento para agregar referencias (Libros)
  if reference_form.process(formname = "reference_form").accepted:
      autor         = reference_form.vars.autor
      coautores         = reference_form.vars.coautores
      colectivo         = reference_form.vars.colectivo
      titulo        = reference_form.vars.titulo
      anio          = reference_form.vars.fecha
      publisher     = reference_form.vars.editorial
      isbn          = reference_form.vars.isbn
      book_comment  = reference_form.vars.book_comment
      edition_comment = reference_form.vars.edition_comment
      open_access = reference_form.vars.open_access
      book_url = reference_form.vars.book_url


      data = {'titulo':titulo,'publisher':publisher,
              'autor': autor ,'year':anio}

      # Se toman los datos de la referencia seleccionada y se
      # envian a la funcion encargada de escribir la cita de
      # dicha referencia.
      cita = cite(data,'book','book-minimal','harvard1')
      referenceCiteIEEE   = cite(data,'book','book-minimal','ieee')
      referenceCiteAPA    = cite(data,'book','book-minimal','apa')

      # Se agrega la referencia.
      transcription = db(db.TRANSCRIPCION.id == id).select().first()

      # Se almacena la referencia en la base de datos.
      db.LIBROS_TRANSCRIPCIONES.insert(
              tipo = 'book',
              transcripcion = transcription,
              titulo = titulo,
              autor = autor,
              coautores = coautores,
              colectivo = colectivo,
              pubyear = anio,
              publisher = publisher,
              isbn = isbn,
              comentarios = book_comment,
              edition_comment = edition_comment,
              book_url = book_url,
              cite = cita,
              citeIEEE = referenceCiteIEEE,
              citeAPA  = referenceCiteAPA
          )

      session.flash = 'Referencia agregada correctamente.'
      redirect(URL(c='transcriptions',f=vistaRetorno,vars={'id' : id, 'previous_page':previous_page}), client_side = True)
  elif reference_form.errors:
      response.flash = 'No se pudo actualizar la referencia.'


  if query_url:
    #Se genera un proceso aparte para buscar en la librería del congreso
    print "EL QUERY QUE SE PASA:"
    print query_url
    print "Se empieza a generar el proceso para buscar en la libreria del congreso"
    #records = marcxml.parse_xml_to_array(query_url)
    query_url_arg = Queue()
    query_url_arg.put(query_url)
    records_arg = Queue()
    e = Event()

    proces_consult_lib = Process(target=process_consultar_libreria, args=(e,query_url_arg, records_arg))
    proces_consult_lib.start()
    print "ahora espero a que termine el proceso de consultar la libreria"
    i = 0
    maxIterations = 4
    sleepTime = 5 

    #Se inicia un ciclo de espera con tantos ciclos como hayan maxIterations y con tiempo de
    #espera entre iteracioens igual a sleepTime
    while (i < maxIterations):
      time.sleep(sleepTime)
      print "Me despierto a revisar"

      # Se checkea que el proceso termino
      if e.is_set():
        print "El proceso de consulta con la librería culminó exitosamente"
        records = records_arg.get()
        lista_records = generate_list(records)
        return dict(records=lista_records,message=message,
                        idTranscripcion = id, form = reference_form,
                        vistaRetorno = vistaRetorno, text = text, previous_page = previous_page)

      # Si el proceso no se ha cerrado
      elif (not(e.is_set()) and (i == maxIterations - 1)):
        print "El proceso de consulta con la librería se cerró por timeout"
        session.flash = 'El proceso de consulta con la librería del congreso se cerró por timeout'
        # Terminar proceso
        proces_consult_lib.terminate()
        records = []
        redirect(URL(c='transcriptions',
                     f=vistaRetorno,vars={'id' : id,
                                          'previous_page': previous_page }),
                     client_side = True)
      i = i + 1

    print "Se termina de buscar en la libreria del congreso"
    i = 0
        
  else:
    redirect(URL(c='transcriptions',
            f=vistaRetorno,vars={'id' : id,
                                 'previous_page': previous_page }),
            client_side = True)


@auth.requires(auth.is_logged_in() and (auth.has_permission('create_transcription') or auth.has_permission('work_on_transcription')
or auth.has_permission('manage_transcription') ) and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def agregar_referencias():


  """
    Función encargada de almacenar el libro introducido de forma manual a la base
    de datos.
  """

  transid      =  request.vars['id']
  vistaRetorno =  request.vars['vistaRetorno']
  previous_page = request.vars['previous_page']
  print("previous_page en agregar_referencias:")
  print(previous_page)

  if ( ( type(transid).__name__)=="list" ):
    transid = transid[0]

  tituloLibros = request.vars['titulo']
  coautores = request.vars['coautores']
  colectivo = request.vars['colectivo']
  autorLibros = request.vars['autor']
  isbnLibros = request.vars['isbn']
  yearLibros = request.vars['year']
  publisherLibros = request.vars['publisher']

  # Se toman los datos de la referencia seleccionada y se
  # envian a la funcion encargada de escribir la cita de
  # dicha referencia.
  referenceCiteHarvard = cite(request.vars,'book','book-minimal','harvard1')
  referenceCiteIEEE   = cite(request.vars,'book','book-minimal','ieee')
  referenceCiteAPA    = cite(request.vars,'book','book-minimal','apa')

  transcription = db(db.TRANSCRIPCION.id == transid).select().first()

  # Se almacena la referencia en la base de datos.
  db.LIBROS_TRANSCRIPCIONES.insert(
          tipo = 'book',
          transcripcion = transcription,
          titulo = tituloLibros,
          autor = autorLibros,
          coautores = coautores,
          colectivo = colectivo,
          isbn = isbnLibros,
          pubyear = yearLibros,
          publisher = publisherLibros,
          cite = referenceCiteHarvard,
          citeIEEE = referenceCiteIEEE ,
          citeAPA = referenceCiteAPA
      )

  session.flash = 'Referencia agregada correctamente.'

  redirect(URL(c='transcriptions',f=vistaRetorno,
                vars={'id' : transid, 'previous_page' : previous_page}),
                client_side = True)


@auth.requires(auth.is_logged_in() and (auth.has_permission('create_transcription') or auth.has_permission('work_on_transcription')
or auth.has_permission('manage_transcription') ) and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def list_videos():

  """
    Función encargada de buscar los videos en la Youtube y agregar el video
    seleccionado a la base de datos.
  """

  video_list = []

  vistaRetorno = request.vars['vistaRetorno']
  id           =  request.vars['id']
  text = request.vars['text']
  if ( ( type(id).__name__)=="list" ):
    id = id[0]

  previous_page = request.vars['previous_page']
  print("previous_page en list_videos:")
  print(previous_page)

  # formulario para agregar videos
  reference_form = SQLFORM.factory(
          Field('autor' ,    type='string'),
          Field('coautores' ,    type='string'),
          Field('institucion' ,    type='string'),
          Field('titulo',    type='string'),
          Field('anio', type = 'integer',  length = 4,
                requires = [IS_INT_IN_RANGE(0, 1e100,
                                error_message='El año debe ser un numero positivo \
                                    de la forma YYYY a partir de 1967.'),
                            IS_LENGTH(4,
                                error_message ='El año debe ser de la forma YYYY.')
                            ]),
          Field('fecha' , type='date', requires = IS_NOT_EMPTY(error_message='Debe escoger una fecha de consulta')),
          Field('url'   , type='string'),
          Field('description', type='text'),
          Field('comentario_video', type='text'),
          labels = {
              'autor'     : 'Autor principal \n (Apellido, nombre)',
              'coautores' : 'Coautor(es)',
              'institucion' : 'Autor institucional',
              'titulo'    : 'Título',
              'anio'      : 'Año',
              'fecha'     : 'Fecha de consulta',
              'url'       : 'Enlace',
              'description' : 'Descripción',
              'comentario_video' : 'Comentarios'},
          submit_button=T('Crear referencia')
          )

  # procesamiento para agregar referencias (Video)
  if reference_form.process(formname = "reference_form").accepted:
      autor     = reference_form.vars.autor
      coautores     = reference_form.vars.coautores
      institucion     = reference_form.vars.institucion
      titulo    = reference_form.vars.titulo
      fecha     = reference_form.vars.fecha
      url       = reference_form.vars.url
      anio      = reference_form.vars.anio
      description      = reference_form.vars.description
      comentario_video      = reference_form.vars.comentario_video

      # Se cambia el formato de la fecha de "string" a "date".
      #fecha = date(*map(int, fecha.split('/')))

      #fecha_ingreso = "{dia} {mes} {anioConsulta}".format(dia=fecha.strftime("%d"),
      #                                            mes=fecha.strftime("%B"),
      #                                            anioConsulta=fecha.strftime("%Y"))

      autorHarvard = formar_cita_autores(autor,coautores,institucion,'harvard')
      autorIEEE = formar_cita_autores(autor,coautores,institucion,'ieee')
      autorAPA = formar_cita_autores(autor,coautores,institucion,'apa')

      data1 = {'autor':autorHarvard,'fecha':anio,'titulo':titulo,
              'url': url, 'fechaConsulta': fecha}
      data2 = {'autor':autorIEEE,'fecha':anio,'titulo':titulo,
              'url': url, 'fechaConsulta': fecha}
      data3 = {'autor':autorAPA,'fecha':anio,'titulo':titulo,
              'url': url, 'fechaConsulta': fecha}

      # Se arma de nuevo la cita de la referencia.
      cita = citeVideo(data1,"harvard")
      citaIEEE = citeVideo(data2,"ieee")
      citaAPA  = citeVideo(data3,"apa")

      transcription = db(db.TRANSCRIPCION.id == id).select().first()

      # Se almacena la referencia en la base de datos.
      db.VIDEOS_TRANSCRIPCIONES.insert(
              tipo = 'video',
              transcripcion = transcription,
              titulo  = titulo,
              autor   = autor,
              coautores = coautores,
              institucion = institucion,
              url     = url,
              pubyear = anio,
              fecha_ingreso = fecha,
              description = description,
              comentarios = comentario_video,
              cite = cita,
              citeIEEE = citaIEEE,
              citeAPA = citaAPA
          )

      session.flash = 'Referencia agregada correctamente.'
      redirect(URL(c='transcriptions',f=vistaRetorno,vars={'id' : id, 'previous_page' : previous_page}), client_side = True)

  elif reference_form.errors:
      response.flash = 'No se pudo actualizar la referencia.'

  title=request.vars['titulo'].replace(" ","%20")

  url_query = "https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=25&q={title}&type=video&key={api_key}".format(title=title,api_key=API_KEY_YOUTUBE)

  search = urllib2.urlopen(url_query).read()
  video = json.loads(search)

  fecha_ingreso = "{dia} {mes} {anioConsulta}".format(dia=datetime.date.today().strftime("%d"),
                                              mes=datetime.date.today().strftime("%B"),
                                              anioConsulta=datetime.date.today().strftime("%Y"))
  # Se almacenan los videos en una lista
  for elem in video.get('items', []):
    video_list.append({'titulo'         : elem['snippet']['title'].encode('utf-8'),
                        'autor'         : elem['snippet']['channelTitle'].encode('utf-8'),
                        'url'           : "https://www.youtube.com/watch?v="+elem['id']['videoId'].encode('utf-8'),
                        'description'   : elem['snippet']['description'].encode('utf-8'),
                        'fecha'         : elem['snippet']['publishedAt'].encode('utf-8'),
                        'img'           : elem['snippet']['thumbnails']['medium']['url'],
                        'fechaConsulta' : fecha_ingreso })


  return dict(idTranscripcion = id,video=video_list,
              form=reference_form, vistaRetorno = vistaRetorno, text=text, previous_page=previous_page)




@auth.requires(
  auth.is_logged_in() and (auth.has_permission('create_transcription') or auth.has_permission('manage_transcription') or auth.has_permission('work_on_transcription'))
  and not(auth.has_membership(auth.id_group(role="INACTIVO")))
)
def agregar_video():

  """
    Función encargada de almacenar un vídeo introducido de forma manual
    en la base de datos.
  """
  transid      =  request.vars['id']
  vistaRetorno = request.vars['vistaRetorno']
  previous_page = request.vars['previous_page']
  print("previous_page en agregar_video:")
  print(previous_page)

  if ( ( type(transid).__name__)=="list" ):
    transid = transid[0]


  tituloVideo   = request.vars['titulo']
  autorVideo    = request.vars['autor']
  fechaVideo    = request.vars['fecha'].split('-')[0]
  urlVideo      = request.vars['url']
  description   = request.vars['description']
  fechaConsulta = request.vars['fechaConsulta']

  citaVideo     = citeVideo(request.vars,"harvard")
  citaVideoIEEE = citeVideo(request.vars,"ieee")
  citaVideoAPA  = citeVideo(request.vars,"apa")

  transcription = db(db.TRANSCRIPCION.id == transid).select().first()

  # Se almacena la referencia en la base de datos.
  db.VIDEOS_TRANSCRIPCIONES.insert(
          tipo = 'video',
          transcripcion = transcription,
          titulo = tituloVideo,
          autor = autorVideo,
          pubyear = fechaVideo,
          url = urlVideo,
          description = description,
          fecha_ingreso = fechaConsulta,
          cite = citaVideo,
          citeIEEE = citaVideoIEEE,
          citeAPA = citaVideoAPA
      )

  session.flash = 'Referencia agregada correctamente.'

  redirect(URL(c='transcriptions',f=vistaRetorno,
                vars={'id' : transid, 'previous_page' : previous_page}),
                client_side = True)



@auth.requires(
  auth.is_logged_in() and (auth.has_permission('create_transcription') or auth.has_permission('manage_transcription') or auth.has_permission('work_on_transcription'))
  and not(auth.has_membership(auth.id_group(role="INACTIVO")))
)
def listar_jounals():

  """
    Controlador encargado de almacenar el artículos seleccionado  de forma
    automatica en la base de datos.
  """

  #----------------------------------------------------------------------------#
  def extract_author(autores):

    autoresFinales = autores['given']+" "+autores['family']

    return autoresFinales.encode('utf-8')

  #----------------------------------------------------------------------------#
  def find_journals(title,author):

    '''
        Función encargada de utilizar el API de CrossRef para buscar una lista
        de artículos
    '''
    lista_jounals = []
    journal = ""

    if (title and author):
      url = 'https://api.crossref.org/works?query.title={title}&query.author={author}'.format(title=title.replace(" ","+"),
                                                                                            author=author.replace(" ","+"))
    elif (title):
      url = 'https://api.crossref.org/works?query.title={title}'.format(title=title.replace(" ","+"))

    else:
      url = 'https://api.crossref.org/works?query.author={author}'.format(author=author.replace(" ","+"))



    # urllib2 no sabe manejar caracteres unicode
    data = urllib2.urlopen(unidecode(url)).read()
    articulos = json.loads(data)



    for i in range(0,len(articulos["message"]["items"])):

      items = articulos["message"]["items"]

      if (items[i]['type'] == "journal-article"):

        try:
          journal = items[i]['reference'][0]['journal-title']
        except:
          journal = ""

        try:
          page = items[i]['page']
        except:
          page = ""

        item = items[i]
        lista_jounals.append({'titulo' : item['title'][0].encode('utf-8') if 'title' in item else "",
                              'autor'  : extract_author(item['author'][0]) if 'author' in item else "",
                              'anio'   : item['issued']['date-parts'][0][0] if 'issued' in item and
                                        'date-parts' in item['issued'] else "",
                              'journal': journal,
                              'volume' : item['volume'] if 'volume' in item else "",
                              'page'   : page})

        journal = ""
        page = ""

    return lista_jounals


  #----------------------------------------------------------------------------#
  # Se extrae el id de las transcripciones.

  vistaRetorno = request.vars['vistaRetorno']

  id =  request.vars['id']
  transcription = db(db.TRANSCRIPCION.id == id).select().first()
  text = request.vars['text']
  if ( ( type(id).__name__)=="list" ):
    id = id[0]
  previous_page = request.vars['previous_page']
  print("previous_page en list_journals:")
  print(previous_page)

  # Se extrae el titulo y el autor del articulo
  titulo=request.vars['titulo']
  autores=request.vars['autores']
  lista_jounals = []

  #print("VARS DE REQUEST EN listar_jounals: ")
  #print(request.vars)

  print("LO QUE LE VOY A PASAR A FIND_JOURNALS")
  print("titulo")
  print(titulo)
  print(type(titulo))
  print("autores")
  print(autores)
  print(type(autores))

  result = find_journals(titulo,autores)

  # formulario para añadir articulos de forma manual
  full_article_form = SQLFORM.factory(
          Field('autor',     type='string', requires = IS_NOT_EMPTY(error_message='Debe introducir un autor.')),
          Field('titulo_art',    type='string', requires = IS_NOT_EMPTY(error_message='Debe introducir un título.')),
          Field('journal',    type='string', requires = IS_NOT_EMPTY(error_message='Debe introducir un journal.')),
          Field('fecha', type = 'integer',  length = 4,
                requires = [IS_INT_IN_RANGE(0, 1e100,
                                error_message='El año debe ser un numero positivo \
                                    de la forma YYYY.'),
                            IS_LENGTH(4,
                                error_message ='El año debe ser de la forma YYYY.')
                            ]),
          Field('volume', type='integer', requires = IS_NOT_EMPTY(error_message='Debe introducir un volumen.')),
          Field('pages' , type='string',  requires = IS_NOT_EMPTY(error_message='Debe introducir las páginas.')),
          Field('number', type='integer'),
          Field('mes'   , type='string'),
          labels = {
              'autor'     : 'Autor principal \n (Apellido, nombre)',
              'titulo_art'    : 'Título',
              'journal'   : 'Publicación Periódica',
              'fecha'     : 'Año',
              'volume'    : 'Volumen',
              'number'    : 'Número',
              'pages'     : 'Páginas',
              'mes'       : 'Mes de publicación' },
          submit_button=T('Crear referencia')
  )

# procesamiento del formulario para los articulos añadidos de forma manual.
  if full_article_form.process(formname = "full_article_form").accepted:

      autor   = full_article_form.vars.autor
      titulo_art  = full_article_form.vars.titulo_art
      year    = full_article_form.vars.fecha
      journal = full_article_form.vars.journal
      volume  = ''
      number  = ''
      pages   = ''
      month   = ''
      typeOfArticle = ''

      data = {'autor': autor, 'title': titulo_art, 'year': year, 'journal': journal}

      # Se crea la referencia para un full article
      typeOfArticle = "article-full"
      volume  = data['volume']  = full_article_form.vars.volume
      pages   = data['pages']   = full_article_form.vars.pages

      if (full_article_form.vars.number):
        number  = data['number']  = full_article_form.vars.number
      else:
        data['number']  = number

      if (full_article_form.vars.mes):
        month   = data['month']   = full_article_form.vars.mes
      else:
        data['month'] = month

      citeHarvard = cite(data,"article","article-full",'harvard1')
      citeIEEE    = cite(data,"article","article-full",'ieee')
      citeAPA     = cite(data,"article","article-full",'apa')

      # Se almacena la referencia en la base de datos.
      db.ARTICULOS_TRANSCRIPCIONES.insert(
              tipo = typeOfArticle,
              transcripcion = transcription,
              titulo   = titulo_art,
              autor    = autor,
              pubyear  = year,
              journal  = journal,
              volume   = volume,
              numero   = number,
              paginas  = pages,
              mes      = month,
              cite     = citeHarvard,
              citeIEEE = citeIEEE,
              citeAPA  = citeAPA
      )

      session.flash = 'Referencia agregada correctamente.'
      redirect(URL(c='transcriptions',f=vistaRetorno,vars={'id' : id, 'previous_page' : previous_page}), client_side = True)

  elif full_article_form.errors:
      response.flash = 'Introduzca todos los elementos en el formulario.'



  return dict(id = id,articulos=result,
              form = full_article_form, vistaRetorno = vistaRetorno, text = text, previous_page = previous_page)


@auth.requires(
  auth.is_logged_in() and (auth.has_permission('create_transcription') or auth.has_permission('manage_transcription') or auth.has_permission('work_on_transcription'))
  and not(auth.has_membership(auth.id_group(role="INACTIVO")))
)
def agregar_articulos():

  '''
    Controlador que se encarga de agregar a la base de datos el artículo
    seleccionado
  '''

  vistaRetorno = request.vars['vistaRetorno']

  id =  request.vars['id']
  if ( ( type(id).__name__)=="list" ):
    id = id[0]
  print "\n Aqui Escribo el request de agregar articulos"
  print(request.vars)
  previous_page = request.vars['previous_page']
  print("previous_page en agregar_articulo:")
  print(previous_page)

  titulo  = request.vars['title'].replace("\"",'')
  autor   = request.vars['autor']
  year    = request.vars['year']
  journal = request.vars['journal']
  volume  = request.vars['volume']
  page    = request.vars['page']

  month   = ""
  number  = ""
  data = {'autor' : autor, 'title': titulo, 'year': year, 'journal': journal,
          'volume': volume, 'pages': page, 'number': number, 'month': month}
  citeHarvard   = cite(data,"article","article-full",'harvard1')
  citeIEEE      = cite(data,'article',"article-full",'ieee')
  citeAPA       = cite(data,'article',"article-full",'apa')


  transcription = db(db.TRANSCRIPCION.id == id).select().first()

  # Se almacena la referencia en la base de datos.
  db.ARTICULOS_TRANSCRIPCIONES.insert(
          tipo = "article-full",
          transcripcion = transcription,
          titulo   = titulo,
          autor    = autor,
          pubyear  = year,
          journal  = journal,
          volume   = volume,
          numero   = number,
          paginas  = page,
          mes      = month,
          cite     = citeHarvard,
          citeIEEE = citeIEEE,
          citeAPA  = citeAPA
  )

  session.flash = 'Referencia agregada correctamente.'

  redirect(URL(c='transcriptions',f=vistaRetorno,
                vars={'id' : id, 'previous_page' : previous_page}),
                client_side = True)


#------------------------------------------------------------------------------#

def makeCite(autor,titulo,fecha_publiacion,enlace,fecha_consulta):

  data = {"autor":autor}

  autorHarvard   = cite(data,"misc","online",'harvard1')
  autorIEEE      = cite(data,'misc',"online",'ieee')
  autorAPA       = cite(data,'misc',"online",'apa')

  print("A.Hardvard", autorHarvard)
  print("A.IEEE",    autorIEEE)
  print("A.citeAPA", autorAPA)

  dataHarvard = {"autor":autorHarvard, "titulo": titulo, "fecha": fecha_publiacion,
          "url": enlace, "fechaConsulta": fecha_consulta}

  dataAPA = {"autor":autorAPA, "titulo": titulo, "fecha": fecha_publiacion,
          "url": enlace, "fechaConsulta": fecha_consulta}

  dataIEEE = {"autor":autorIEEE, "titulo": titulo, "fecha": fecha_publiacion,
          "url": enlace, "fechaConsulta": fecha_consulta}

  citeHarvard = citeFuentesWeb(dataHarvard,"harvard")
  citeAPA     = citeFuentesWeb(dataAPA,"apa")
  citeIEEE    = citeFuentesWeb(dataIEEE,"ieee")

  print("Hardvard", citeHarvard)
  print("IEEE",    citeIEEE)
  print("citeAPA", citeAPA)

  result = {"harvard": citeHarvard, "ieee": citeIEEE, "apa": citeAPA}

  return result

#------------------------------------------------------------------------------#

@auth.requires(
  auth.is_logged_in() and (auth.has_permission('manage_transcriptors', 'auth_user') or auth.has_permission('create_transcription') or auth.has_permission('work_on_transcription'))
  and not(auth.has_membership(auth.id_group(role="INACTIVO")))
)
def add_web_source():

  previous_page = request.vars['previous_page']
  id =  request.vars['id']
  if ( ( type(id).__name__)=="list" ):
    id = id[0]

  tipo_fuente_web = request.vars['tipo_fuente_web']
  vistaRetorno    = request.vars['vistaRetorno']

  print(tipo_fuente_web)

  # formulario para agregar fuentes web (MOOC, blogs, websites)
  fuente_web_1_form = SQLFORM.factory(
          Field('titulo',      type='string', requires = IS_NOT_EMPTY(error_message='Debe introducir un título.')),
          Field('autor',       type='string'),
          Field('coautores',   type='string'),
          Field('institucion', type='string'),
          Field('plataforma',  type='string'),
          Field('enlace',      type='string', requires = IS_NOT_EMPTY(error_message='Debe introducir un enlace.')),
          Field('fecha_publicacion', type = 'integer',  length = 4,
                requires = [IS_INT_IN_RANGE(0, 1e100,
                                error_message='El año debe ser un numero positivo \
                                    de la forma YYYY.'),
                            IS_LENGTH(4,
                                error_message ='El año debe ser de la forma YYYY.')
                            ]),
          Field('fecha_consulta',type='date', requires = IS_NOT_EMPTY(error_message='Debe escoger una fecha de consulta')),
          Field('comentarios', type='text'),
          labels = {
              'titulo'           : 'Título',
              'autor'            : 'Autor principal \n (Apellido, nombre)',
              'coautores'        : 'Coautor(es)',
              'institucion'      : 'Institución',
              'plataforma'       : 'Plataforma',
              'enlace'           : 'Enlace',
              'fecha_publicacion' : 'Año',
              'fecha_consulta'   : 'Fecha de consulta',
              'comentarios'      : 'Comentarios' },
          submit_button=T('Crear referencia')
   )

  # formulario para agregar fuentes web (items de: MOOC, blogs, websites)
  fuente_web_2_form = SQLFORM.factory(
          Field('titulo', type='string', requires = IS_NOT_EMPTY(error_message='Debe introducir un título.')),
          Field('autor',  type='string'),
          Field('coautores',   type='string'),
          Field('institucion', type='string'),
          Field('titulo_contenedor', type='string'),
          Field('plataforma',  type='string'),
          Field('enlace',      type='string', requires = IS_NOT_EMPTY(error_message='Debe introducir un enlace.')),
          Field('fecha_publicacion', type = 'integer',  length = 4,
                requires = [IS_INT_IN_RANGE(0, 1e100,
                                error_message='El año debe ser un numero positivo \
                                    de la forma YYYY.'),
                            IS_LENGTH(4,
                                error_message ='El año debe ser de la forma YYYY.')
                            ]),
          Field('fecha_consulta',type='date', requires = IS_NOT_EMPTY(error_message='Debe escoger una fecha de consulta')),
          Field('comentarios', type='text'),
          labels = {
              'titulo'            : 'Título',
              'autor'             : 'Autor principal \n (Apellido, nombre)',
              'coautores'         : 'Coautor(es)',
              'institucion'       : 'Institución',
              'titulo_contenedor' : 'Título del contenedor',
              'plataforma'        : 'Plataforma',
              'enlace'            : 'Enlace',
              'fecha_publiacion'  : 'Año',
              'fecha_consulta'    : 'Fecha de consulta',
              'comentarios'      : 'Comentarios' },
          submit_button=T('Crear referencia')
   )

  # formulario para agregar fuentes web (otros)
  fuente_web_otros_form = SQLFORM.factory(
          Field('titulo',      type='string', requires = IS_NOT_EMPTY(error_message='Debe introducir un título.')),
          Field('autor',       type='string'),
          Field('coautores', type="string"),
          Field('institucion', type="string"),
          Field('enlace',      type='string', requires = IS_NOT_EMPTY(error_message='Debe introducir un enlace.')),
          Field('fecha_publicacion', type = 'integer',  length = 4,
                requires = [IS_INT_IN_RANGE(0, 1e100,
                                error_message='El año debe ser un numero positivo \
                                    de la forma YYYY.'),
                            IS_LENGTH(4,
                                error_message ='El año debe ser de la forma YYYY.')
                            ]),
          Field('fecha_consulta',type='date', requires = IS_NOT_EMPTY(error_message='Debe escoger una fecha de consulta')),
          Field('comentarios', type='text'),
          labels = {
              'titulo'               : 'Título',
              'autor'                : 'Autor principal \n (Apellido, nombre)',
              'coautores'            : 'Coautor(es)',
              'institucion'            : 'Autor Institucional',
              'enlace'               : 'Enlace',
              'fecha_publicacion'     : 'Año',
              'fecha_consulta'       : 'Fecha de consulta',
              'comentarios'          : 'Comentarios'},
          submit_button=T('Crear referencia')
   )


  # procesamiento del formulario para agregar fuentes web (contenedor)
  if fuente_web_1_form.process(formname = "fuente_web_1_form", onvalidation=autores_form_check).accepted:
    titulo           = fuente_web_1_form.vars.titulo
    autor            = fuente_web_1_form.vars.autor
    coautores        = fuente_web_1_form.vars.coautores
    institucion      = fuente_web_1_form.vars.institucion
    plataforma       = fuente_web_1_form.vars.plataforma
    enlace           = fuente_web_1_form.vars.enlace
    fecha_publicacion = fuente_web_1_form.vars.fecha_publicacion
    fecha_consulta   = fuente_web_1_form.vars.fecha_consulta
    comentarios   = fuente_web_1_form.vars.comentarios

    data = {"autor":autor}

    autorHarvard = formar_cita_autores(autor,coautores,institucion,'harvard')
    autorIEEE = formar_cita_autores(autor,coautores,institucion,'ieee')
    autorAPA = formar_cita_autores(autor,coautores,institucion,'apa')

    print("A.Hardvard", autorHarvard)
    print("A.IEEE",    autorIEEE)
    print("A.citeAPA", autorAPA)

    dataHarvard = {"autor":autorHarvard, "titulo": titulo, "fecha": fecha_publicacion,
            "url": enlace, "fechaConsulta": fecha_consulta}

    dataAPA = {"autor":autorAPA, "titulo": titulo, "fecha": fecha_publicacion,
            "url": enlace, "fechaConsulta": fecha_consulta}

    dataIEEE = {"autor":autorIEEE, "titulo": titulo, "fecha": fecha_publicacion,
            "url": enlace, "fechaConsulta": fecha_consulta}

    citeHarvard = citeFuentesWeb(dataHarvard,"harvard")
    citeAPA     = citeFuentesWeb(dataAPA,"apa")
    citeIEEE    = citeFuentesWeb(dataIEEE,"ieee")

    print("Hardvard", citeHarvard)
    print("IEEE",    citeIEEE)
    print("citeAPA", citeAPA)

    transcription = db(db.TRANSCRIPCION.id == id).select().first()

    # Se almacena la referencia en la base de datos.
    db.REFERENCIAS_WEB_TRANSCRIPCIONES.insert(
            tipo = "fuente_web_1",
            transcripcion = transcription,
            titulo        = titulo,
            autor         = autor,
            coautores     = coautores,
            pubyear       = fecha_publicacion,
            fecha_ingreso = fecha_consulta,
            url           = enlace,
            institucion   = institucion,
            plataforma    = plataforma,
            cite          = citeHarvard,
            citeIEEE      = citeIEEE,
            citeAPA       = citeAPA,
            comentarios   = comentarios
    )

    session.flash = 'Referencia agregada correctamente.'

    redirect(URL(c='transcriptions',f=vistaRetorno,
                  vars={'id' : id, 'previous_page' : previous_page}),
                  client_side = True)

  elif fuente_web_1_form.errors:
      response.flash = 'Introduzca de manera correcta los elementos en el formulario.'


  # procesamiento del formulario para agregar fuentes web (items)
  if fuente_web_2_form.process(formname = "fuente_web_2_form", onvalidation=autores_form_check).accepted:
    titulo            = fuente_web_2_form.vars.titulo
    autor             = fuente_web_2_form.vars.autor
    coautores         = fuente_web_2_form.vars.coautores
    titulo_contenedor = fuente_web_2_form.vars.titulo_contenedor
    institucion       = fuente_web_2_form.vars.institucion
    plataforma        = fuente_web_2_form.vars.plataforma
    enlace            = fuente_web_2_form.vars.enlace
    fecha_publicacion  = fuente_web_2_form.vars.fecha_publicacion
    fecha_consulta    = fuente_web_2_form.vars.fecha_consulta
    comentarios   = fuente_web_2_form.vars.comentarios

    print("Fecha consulta", fecha_consulta)

    data = {"autor":autor}

    autorHarvard = formar_cita_autores(autor,coautores,institucion,'harvard')
    autorIEEE = formar_cita_autores(autor,coautores,institucion,'ieee')
    autorAPA = formar_cita_autores(autor,coautores,institucion,'apa')

    print("A.Hardvard", autorHarvard)
    print("A.IEEE",    autorIEEE)
    print("A.citeAPA", autorAPA)

    dataHarvard = {"autor":autorHarvard, "titulo": titulo, "fecha": fecha_publicacion,
            "url": enlace, "fechaConsulta": fecha_consulta}

    dataAPA = {"autor":autorAPA, "titulo": titulo, "fecha": fecha_publicacion,
            "url": enlace, "fechaConsulta": fecha_consulta}

    dataIEEE = {"autor":autorIEEE, "titulo": titulo, "fecha": fecha_publicacion,
            "url": enlace, "fechaConsulta": fecha_consulta}

    citeHarvard = citeFuentesWeb(dataHarvard,"harvard")
    citeAPA     = citeFuentesWeb(dataAPA,"apa")
    citeIEEE    = citeFuentesWeb(dataIEEE,"ieee")

    print("Hardvard", citeHarvard)
    print("IEEE",    citeIEEE)
    print("citeAPA", citeAPA)

    transcription = db(db.TRANSCRIPCION.id == id).select().first()

    # Se almacena la referencia en la base de datos.
    db.REFERENCIAS_WEB_TRANSCRIPCIONES.insert(
            tipo              = "fuente_web_2",
            transcripcion     = transcription,
            titulo            = titulo,
            titulo_contenedor = titulo_contenedor,
            autor             = autor,
            coautores         = coautores,
            pubyear           = fecha_publicacion,
            fecha_ingreso     = fecha_consulta,
            url               = enlace,
            institucion       = institucion,
            plataforma        = plataforma,
            cite              = citeHarvard,
            citeIEEE          = citeIEEE,
            citeAPA           = citeAPA,
            comentarios       = comentarios
    )

    session.flash = 'Referencia agregada correctamente.'

    redirect(URL(c='transcriptions',f=vistaRetorno,
                  vars={'id' : id, 'previous_page' : previous_page }),
                  client_side = True)


  elif fuente_web_2_form.errors:
      response.flash = 'Introduzca de manera correcta los elementos en el formulario.'



  # procesamiento del formulario para añadir otros tipos de fuente web
  if fuente_web_otros_form.process(formname = "fuente_web_otros_form").accepted:
    titulo                = fuente_web_otros_form.vars.titulo
    autor                 = fuente_web_otros_form.vars.autor
    coautores             = fuente_web_otros_form.vars.coautores
    institucion             = fuente_web_otros_form.vars.institucion
    enlace                = fuente_web_otros_form.vars.enlace
    fecha_publicacion      = fuente_web_otros_form.vars.fecha_publicacion
    fecha_consulta        = fuente_web_otros_form.vars.fecha_consulta
    comentarios           = fuente_web_otros_form.vars.comentarios


    data = {"autor":autor}

    autorHarvard = formar_cita_autores(autor,coautores,colectivo,'harvard')
    autorIEEE = formar_cita_autores(autor,coautores,colectivo,'ieee')
    autorAPA = formar_cita_autores(autor,coautores,colectivo,'apa')

    print("A.Hardvard", autorHarvard)
    print("A.IEEE",    autorIEEE)
    print("A.citeAPA", autorAPA)

    dataHarvard = {"autor":autorHarvard, "titulo": titulo, "fecha": fecha_publicacion,
            "url": enlace, "fechaConsulta": fecha_consulta}

    dataAPA = {"autor":autorAPA, "titulo": titulo, "fecha": fecha_publicacion,
            "url": enlace, "fechaConsulta": fecha_consulta}

    dataIEEE = {"autor":autorIEEE, "titulo": titulo, "fecha": fecha_publicacion,
            "url": enlace, "fechaConsulta": fecha_consulta}

    citeHarvard = citeFuentesWeb(dataHarvard,"harvard")
    citeAPA     = citeFuentesWeb(dataAPA,"apa")
    citeIEEE    = citeFuentesWeb(dataIEEE,"ieee")

    print("Hardvard", citeHarvard)
    print("IEEE",    citeIEEE)
    print("citeAPA", citeAPA)

    transcription = db(db.TRANSCRIPCION.id == id).select().first()

    # Se almacena la referencia en la base de datos.
    db.REFERENCIAS_WEB_TRANSCRIPCIONES.insert(
            tipo                 = "fuente_web_otros",
            transcripcion        = transcription,
            titulo               = titulo,
            autor                = autor,
            coautores            = coautores,
            institucion          = institucion,
            pubyear              = fecha_publicacion,
            fecha_ingreso        = fecha_consulta,
            url                  = enlace,
            cite                 = citeHarvard,
            citeIEEE             = citeIEEE,
            citeAPA              = citeAPA,
            comentarios          = comentarios
    )

    session.flash = 'Referencia agregada correctamente.'

    redirect(URL(c='transcriptions',f=vistaRetorno,
                  vars={'id' : id, 'previous_page' : previous_page}),
                  client_side = True)

  elif fuente_web_otros_form.errors:
      response.flash = 'Introduzca de manera correcta los elementos en el formulario.'


  if (tipo_fuente_web == "fuente_web1"):
    form = fuente_web_1_form

  elif (tipo_fuente_web == "fuente_web2"):
    form = fuente_web_2_form

  else:
    form = fuente_web_otros_form


  return dict(form=form)


#------------------------------------------------------------------------------#

@auth.requires(auth.is_logged_in()
               and (auth.has_permission('manage_transcriptors', 'auth_user') or auth.has_permission('create_transcription'))
               and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def view():
    """
      Vista para ver la transcripción realizada ya enviada a aprobación.
    """

    id =  request.vars['id']
    if ( ( type(id).__name__)=="list" ):
      id = id[0]

    transcription = db(db.TRANSCRIPCION.id == id).select()
    if transcription:
        transcription = transcription.first()
        pdfurl = transcription.original_pdf
        code   = transcription.codigo
        text   = transcription.texto
        if ((pdfurl is None) or (code is None) or (text is None)):
            session.flash = "El transcriptor no ha iniciado esta transcripción aún."
            redirect(URL(c='transcriptions', f='following'))
            print "Falta el código, el url del pdf o el text"

        pdfurl = URL('static','transcriptions/originalpdf/' + pdfurl)
        transcription_form = SQLFORM(db.TRANSCRIPCION,
                                     record   = id,
                                     writable = False,
                                     fields = ['codigo', 'denominacion', 'fecha_elaboracion',
                                               'periodo', 'horas_teoria', 'horas_practica',
                                               'horas_laboratorio' , 'creditos', 'anio',
                                               'periodo_hasta', 'anio_hasta',
                                               'sinopticos','ftes_info_recomendadas',
                                               'estrategias_met','estrategias_eval',
                                               'observaciones','objetivos_generales','objetivos_especificos'],
                                     submit_button=T('Guardar')
                                     )

        # obtenemos los campos adicionales, si existen
        campos_adicionales = db(db.CAMPOS_ADICIONALES_TRANSCRIPCION.transcripcion == transcription).select()
        # Obtenemos las referencias de la transcripcion, si existen
        referencias_transcripcion = []
        libros_transcripcion = db(db.LIBROS_TRANSCRIPCIONES.transcripcion == transcription).select()
        for r in libros_transcripcion:
            referencias_transcripcion.append(r)
        videos_transcripcion = db(db.VIDEOS_TRANSCRIPCIONES.transcripcion == transcription).select()
        for r in videos_transcripcion:
            referencias_transcripcion.append(r)
        articulos_transcripcion = db(db.ARTICULOS_TRANSCRIPCIONES.transcripcion == transcription).select()
        for r in articulos_transcripcion:
            referencias_transcripcion.append(r)
        fuentes_web_transcripcion = db(db.REFERENCIAS_WEB_TRANSCRIPCIONES.transcripcion == transcription).select()
        for r in fuentes_web_transcripcion:
            referencias_transcripcion.append(r)
        fuentes_otros_transcripcion = db(db.REFERENCIAS_OTROS_TRANSCRIPCIONES.transcripcion == transcription).select()
        for r in fuentes_otros_transcripcion:
            referencias_transcripcion.append(r)

        return dict(text=text,
                    pdfurl=pdfurl,
                    code=code,
                    id = id,
                    referencias_transcripcion = referencias_transcripcion,
                    transcription_form = transcription_form,
                    campos_adicionales = campos_adicionales)

    else:
        session.flash = "Ocurrió un problema haciendo el query en la base de datos"
        redirect(URL(c='transcriptions', f='view'))
        print "El select de la base de datos falló"

@auth.requires(auth.is_logged_in()
               and (auth.has_permission('manage_transcriptors', 'auth_user') or auth.has_permission('create_transcription') or auth.has_permission('work_on_transcription'))
               and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def journal():
    """
      Lista la bitacora correspondiente a una transcripcion.
    """

    transid = request.args(0)
    message = "Bitácora Transcripción #%s"%(transid)

    # Obtenemos los registros de la bitácora para la transcripcion
    registros = db(db.BITACORA_TRANSCRIPCION.transcripcion == transid).select(orderby=db.BITACORA_TRANSCRIPCION.id)

    return dict(message = message, transid = transid, registros = registros)

@auth.requires(auth.is_logged_in() and (auth.has_permission('create_transcription') or auth.has_permission('work_on_transcription')) and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def list():
    """
      Lista las transcripciones pendientes.
    """

    form = ''
    message = "Transcripciones"

    # Obtenemos las transcripciones
    transcripciones = db((db.TRANSCRIPCION.transcriptor == auth.user.username) & (db.TRANSCRIPCION.estado == 'pendiente')).select(db.TRANSCRIPCION.id,
                                                                                                                                  db.TRANSCRIPCION.codigo,
                                                                                                                                  db.TRANSCRIPCION.fecha_elaboracion,
                                                                                                                                  db.TRANSCRIPCION.fecha_modificacion)

    lista_transcripciones = []
    # Obtenemos los roles de cada usuario
    print("MOSTRANDO LOS IDS")
    for transcripcion in transcripciones:
        print(transcripcion.id)
        lista_transcripciones.append({'id' : transcripcion.id,
                                      'codigo': transcripcion.codigo,
                                      'fecha_elaboracion' : transcripcion.fecha_elaboracion,
                                      'fecha_modificacion': transcripcion.fecha_modificacion})

    return dict(message=message, transcripciones = lista_transcripciones, form=form)

@auth.requires(auth.is_logged_in() and auth.has_permission('work_on_transcription') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def list_sent():
    """
        Lista las transcripciones enviadas por el usuario que esperan aprobacion
        del supervisor.
    """
    message = "Transcripciones en Revisión"

    # Obtenemos las transcripciones
    transcripciones = db((db.TRANSCRIPCION.transcriptor == auth.user.username) &
                         (db.TRANSCRIPCION.estado == 'propuesta')).select(db.TRANSCRIPCION.id,
                                                                          db.TRANSCRIPCION.codigo,
                                                                          db.TRANSCRIPCION.fecha_elaboracion,
                                                                          db.TRANSCRIPCION.fecha_modificacion)

    lista_transcripciones = []
    # Obtenemos los roles de cada usuario
    for transcripcion in transcripciones:
        lista_transcripciones.append({'id' : transcripcion.id,
                                      'codigo': transcripcion.codigo,
                                      'fecha_elaboracion' : transcripcion.fecha_elaboracion,
                                      'fecha_modificacion': transcripcion.fecha_modificacion})

    return dict(message=message, transcripciones = lista_transcripciones)

@auth.requires(auth.is_logged_in() and auth.has_permission('manage_transcriptors', 'auth_user') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def list_pending():
    """
        Lista las transcripciones pendientes por aprobar desde el punto de vista
        del supervisor.
    """

    message = "Transcripciones por Revisión"


    print ("###########################LIST PENDING#############################")

    # obtenemos los transcriptores asociadas al supervisor
    all_transcriptors_for_user = db(db.AFILIACION.supervisor == auth.user.username).select(db.AFILIACION.transcriptor)
    all_afiliaciones_for_user = db(db.AFILIACION.supervisor == auth.user.username).select()
    all_registros_for_user = db(db.REGISTRO_TRANSCRIPTORES.afiliacion.belongs(all_afiliaciones_for_user)).select()
    print "all_transcriptors_for_user"
    print all_transcriptors_for_user
    print "all_afiliaciones_for_user"
    print all_afiliaciones_for_user
    print "all_registros_for_user"
    print all_registros_for_user

    #for user in all_transcriptors_for_user:
    transcriptors = []
    users = db(db.auth_user).select(db.auth_user.id,
                                            db.auth_user.username,
                                            db.auth_user.first_name,
                                            db.auth_user.last_name,
                                            db.auth_user.email)
    for user in users:
        for trans in all_transcriptors_for_user:
            if user.username == trans.transcriptor:
                transcriptors.append(user)


    print "transcriptors"
    print transcriptors

    lista_transcripciones = []
    for transcriptor in transcriptors:
    # Obtenemos las transcripciones
        transcripciones = db((db.AFILIACION.transcriptor == transcriptor.username)&
                          (db.AFILIACION.supervisor == auth.user.username)&
                          (db.REGISTRO_TRANSCRIPTORES.afiliacion == db.AFILIACION.id)&
                          (db.REGISTRO_TRANSCRIPTORES.transcripcion == db.TRANSCRIPCION.id)&
                          (db.TRANSCRIPCION.estado == 'propuesta')).select(db.TRANSCRIPCION.id,
                                                                          db.TRANSCRIPCION.codigo,
                                                                          db.TRANSCRIPCION.transcriptor,
                                                                          db.TRANSCRIPCION.fecha_elaboracion,
                                                                          db.TRANSCRIPCION.fecha_modificacion)
        print "transcripciones"
        print transcripciones

        for transcripcion in transcripciones:
            lista_transcripciones.append({'id' : transcripcion.id,
                                         'transcriptor' : transcripcion.transcriptor,
                                         'codigo': transcripcion.codigo,
                                         'fecha_elaboracion' : transcripcion.fecha_elaboracion,
                                         'fecha_modificacion': transcripcion.fecha_modificacion})

    print("lista_transcripciones:")
    print(lista_transcripciones)


    #for transcripcion in transcripciones:


    return dict(message=message, transcripciones = lista_transcripciones)

@auth.requires(auth.is_logged_in() and auth.has_permission('manage_transcriptors', 'auth_user') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def list_approved():
    """
        List las transcripciones aprbadas por el supervisor.
    """

    message = "Transcripciones Aprobadas"

    # obtenemos los transcriptores asociados
    all_transcriptors_for_user = db(db.AFILIACION.supervisor == auth.user.username)._select(db.AFILIACION.transcriptor)

    # Obtenemos las transcripciones
    transcripciones = db((db.TRANSCRIPCION.transcriptor == auth.user.username) &
                         (db.TRANSCRIPCION.estado == 'aprobada')).select(db.TRANSCRIPCION.id,
                                                                         db.TRANSCRIPCION.codigo,
                                                                         db.TRANSCRIPCION.transcriptor,
                                                                         db.TRANSCRIPCION.fecha_elaboracion,
                                                                         db.TRANSCRIPCION.fecha_modificacion,
                                                                         db.TRANSCRIPCION.periodo,
                                                                         db.TRANSCRIPCION.anio,
                                                                         )
    lista_transcripciones = []
    # Obtenemos los roles de cada usuario
    for transcripcion in transcripciones:
        lista_transcripciones.append({'id' : transcripcion.id,
                                      'codigo': transcripcion.codigo,
                                      'vigencia_desde': transcripcion.periodo + " " + str(transcripcion.anio),
                                      'fecha_modificacion': transcripcion.fecha_modificacion})

    return dict(message=message, transcripciones = lista_transcripciones)

def match_codigo_asig(text):
    """
      Del texto extraido se busca y se asocia el codigo de la asignatura
      a traves de una expresion regular.
    """
    expresion = '([A-Z]{2,3} *(-|\s|[^a-z|^A-Z|^0-9]|) *[0-9]{3,4})'
    patron = re.compile(expresion)
    matcher = patron.search(text)
    if matcher is not None:
        code = re.sub(' *(-|\s|[^a-z|^A-Z|^0-9]|) *', '', matcher.group(0))
        return code
    else:
        return None

def extract_text(path):
    """
      Se extraen el texto del programa en pdf para comparar al momento de
      realizar la transcripción.
    """
    os.system("pdftotext -layout " + path + " extraccion.txt")
    file = codecs.open("extraccion.txt", encoding="utf-8")
    text = file.read()
    file.close()
    os.system("rm extraccion.txt")
    return text

def extract_text_from_image(path):
    """
      Función para extraer el texto de la imagen para comparar
      al momento de hacer la transcripción.
    """
    tool = pyocr.get_available_tools()[0]
    lang = tool.get_available_languages()[2]

    req_image = []
    final_text = []

    image_pdf  = Image(filename=path, resolution=200)
    image_jpeg = image_pdf.convert('jpeg')

    for img in image_jpeg.sequence:
        img_page = Image(image=img)
        req_image.append(img_page.make_blob('jpeg'))

    for img in req_image:
        txt = tool.image_to_string(Pi.open(io.BytesIO(img)),
                                   lang=lang,
                                   builder=pyocr.builders.TextBuilder()
                                   )
        final_text.append(txt)

    trancription = ''
    for i in final_text:
        trancription += i

    return trancription

def check_required_fields(trasid):
    """
      Se chequea que los campos obligatorios estén llenos en el formulario.
    """

    transcripcion = db(db.TRANSCRIPCION.id == trasid).select().first()

    campos_obligatorios = [ 'codigo', 'denominacion', 'fecha_elaboracion', 'periodo', 'anio' ]

    check = True

    for campo in campos_obligatorios:
        if not(transcripcion[campo]):
            check = False

    return check

@auth.requires(auth.is_logged_in() and auth.has_permission('manage_transcriptors', 'auth_user') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def convert_to_program(transid):
    """
        Dada una Transcripcion aprobada, trasnscribe los datos a la tabla PROGRAMA, para que
        pueda ser consultado publicamente desde el Sistema SIGPAE.
    """

    transcripcion = db(db.TRANSCRIPCION.id == transid).select().first()

    new_id = db.PROGRAMA.insert(
        original_pdf = transcripcion.original_pdf,
        codigo = transcripcion.codigo,
        denominacion = transcripcion.denominacion,
        fecha_elaboracion = transcripcion.fecha_elaboracion,
        periodo = transcripcion.periodo,
        anio = transcripcion.anio,
        periodo_hasta = transcripcion.periodo_hasta,
        anio_hasta = transcripcion.anio_hasta,
        horas_teoria = transcripcion.horas_teoria,
        horas_practica = transcripcion.horas_practica,
        horas_laboratorio = transcripcion.horas_laboratorio,
        creditos = transcripcion.creditos,
        sinopticos = transcripcion.sinopticos,
        estrategias_met = transcripcion.estrategias_met,
        estrategias_eval = transcripcion.estrategias_eval,
        observaciones = transcripcion.observaciones,
        objetivos_generales = transcripcion.objetivos_generales,
        objetivos_especificos = transcripcion.objetivos_especificos,
        fecha_modificacion = transcripcion.fecha_modificacion,
        estado = 'aprobado'
    )

    # agregamos los campos adicionales asociados a la transcripcion, al nuevo programa.

    # obtenemos los campos adicionales, si existen
    campos_adicionales = db(db.CAMPOS_ADICIONALES_TRANSCRIPCION.transcripcion == transcripcion).select()

    for campo in campos_adicionales:
        db.CAMPOS_ADICIONALES_PROGRAMA.insert(
            programa  = new_id,
            nombre    = campo.nombre,
            contenido = campo.contenido
        )

    # Obtenemos las fuentes de informacion, si existen
    fuentes_libros = db(db.LIBROS_TRANSCRIPCIONES.transcripcion == transcripcion).select()
    fuentes_articulos = db(db.ARTICULOS_TRANSCRIPCIONES.transcripcion == transcripcion).select()
    fuentes_videos = db(db.VIDEOS_TRANSCRIPCIONES.transcripcion == transcripcion).select()
    fuentes_web = db(db.REFERENCIAS_WEB_TRANSCRIPCIONES.transcripcion == transcripcion).select()
    fuentes_otros = db(db.REFERENCIAS_OTROS_TRANSCRIPCIONES.transcripcion == transcripcion).select()

    for referencia in fuentes_libros:
        db.PROGRAMA_REFERENCIAS.insert(
          programa = new_id,
          referencia_libros = referencia
        )
    for referencia in fuentes_articulos:
        db.PROGRAMA_REFERENCIAS.insert(
          programa = new_id,
          referencia_articulos = referencia
        )
    for referencia in fuentes_videos:
        db.PROGRAMA_REFERENCIAS.insert(
          programa = new_id,
          referencia_videos = referencia
        )
    for referencia in fuentes_web:
        db.PROGRAMA_REFERENCIAS.insert(
          programa = new_id,
          referencia_web = referencia
        )
    for referencia in fuentes_otros:
        db.PROGRAMA_REFERENCIAS.insert(
          programa = new_id,
          referencia_otros = referencia
        )

    # Creamos una nueva entrada den la bitacora del programa
    regiter_in_programs_journal(db, auth, new_id, 'CREACIÓN', 'Creación del Programa a partir de una Transcripción Aprobada.')

@auth.requires(auth.is_logged_in() and (auth.has_permission('manage_transcription') or auth.has_permission('work_on_transcription')) and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def delete_transcription():
    """
      Permite a un supervisor eliminar una transcripción
    """

    transid = request.args(0)
    transcripcion = db(db.TRANSCRIPCION.id == transid).select().first()
    os.remove(os.path.join(request.folder,'static/transcriptions/originalpdf',transcripcion.original_pdf))
    db(db.LIBROS_TRANSCRIPCIONES.id == transid).delete()
    db(db.ARTICULOS_TRANSCRIPCIONES.id == transid).delete()
    db(db.VIDEOS_TRANSCRIPCIONES.id == transid).delete()
    db(db.REFERENCIAS_WEB_TRANSCRIPCIONES.id == transid).delete()
    db(db.REFERENCIAS_OTROS_TRANSCRIPCIONES.id == transid).delete()
    db(db.CAMPOS_ADICIONALES_TRANSCRIPCION.id == transid).delete()
    db(db.TRANSCRIPCION.id == transid).delete()
    db(db.REGISTRO_TRANSCRIPTORES.transcripcion == transid).delete()

    session.flash = "Transcripción eliminada exitosamente."
    redirect(URL(c='transcriptions',f='list'))

@auth.requires(auth.is_logged_in() and (auth.has_permission('create_transcription') or auth.has_permission('work_on_transcription')) and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def send_transcription():
    """
      Permite al usuario enviar una transcripcion al supervisor.
    """

    transid = request.args(0)

    if check_required_fields(transid):
        db.TRANSCRIPCION[transid] = dict(estado = 'propuesta')
        session.flash = "Transcripción enviada exitosamente a revisión."

        # Registro en la bitacora de transcripcion
        regiter_in_transcriptions_journal(db, auth, transid, 'ENVIO', 'Transcripción enviada a revisión.')

    else:
        session.flash = "Faltan campos obligatorios para esta Transcripción. Complételos antes de enviarla a revisión."

    redirect(URL(c='transcriptions',f='list'))

@auth.requires(auth.is_logged_in() and auth.has_permission('manage_transcriptors', 'auth_user') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def approve_transcription():
    """
      Permite al supervisor aprobar una transcripción.
    """
    print("ENTRE A APPROVE_TRANSCRIPTION")
    transid = request.args(0)
    db.TRANSCRIPCION[transid] = dict(estado = 'aprobada', transcriptor = auth.user.username)

    # Registro en la bitacora de transcripcion
    regiter_in_transcriptions_journal(db, auth, transid, 'APROBACIÓN', 'Transcripción aprobada.')

    convert_to_program(transid)

    session.flash = "Transcripción aprobada exitosamente."
    redirect(URL(c='transcriptions',f='list_pending'))

@auth.requires(auth.is_logged_in() and auth.has_permission('manage_transcriptors', 'auth_user') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def delete_transcription_as_supervizer():
    """
    Permite al supervisor rechazar trasncripciones pendientes.
    """

    transid = request.args(0)
    transcripcion = db(db.TRANSCRIPCION.id == transid).select().first()
    if transcripcion.original_pdf:
      os.remove(os.path.join(request.folder,'static/transcriptions/originalpdf',transcripcion.original_pdf))
    db(db.TRANSCRIPCION.id == transid).delete()

    session.flash = "Transcripción eliminada exitosamente."
    redirect(URL(c='transcriptions',f='following'))

@auth.requires(auth.is_logged_in()
               and (auth.has_permission('manage_transcriptors', 'auth_user') or auth.has_permission('create_transcription'))
               and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def download_journal():

    journal_id = request.args[0]
    if not(journal_id):
        redirect(URL(c='default',f='not_authorized'))

    #Excecute query
    rows = db(db.BITACORA_TRANSCRIPCION.transcripcion == journal_id).select(db.BITACORA_TRANSCRIPCION.transcripcion,
                                                                            db.BITACORA_TRANSCRIPCION.fecha,
                                                                            db.BITACORA_TRANSCRIPCION.usuario,
                                                                            db.BITACORA_TRANSCRIPCION.rol_usuario,
                                                                            db.BITACORA_TRANSCRIPCION.accion,
                                                                            db.BITACORA_TRANSCRIPCION.descripcion)
    #convert query to csv
    tempfile = StringIO.StringIO()
    rows.export_to_csv_file(tempfile)
    response.headers['Content-Type'] = 'text/csv'
    attachment = 'attachment; filename="BITACORA_TRANSCRIPCION_%s.csv"'%(journal_id)
    response.headers['Content-Disposition'] = attachment
    return tempfile.getvalue()
