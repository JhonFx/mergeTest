from logs               import *
import urllib2
import json
import re

@auth.requires(auth.is_logged_in() and auth.has_permission('request_ap') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def add():

    '''
        Descripción: Controlador para crear una nueva
        solicitud de programa analítico.
    '''

    # Mensaje principal del sitio
    message = "Nueva solicitud de Programa Analítico"

    # Cuando el solicitante es un departamento, no es necesario el drop-down
    if (auth.has_membership(auth.id_group(role="DEPARTAMENTO"))):

        form = SQLFORM.factory(
            Field('nombre_tentativo', type='string', notnull=True, required=True,
                requires = [IS_NOT_EMPTY(error_message='Indique un nombre tentativo.')]),
            Field('descripcion', type='text', notnull=True, required=True,
                requires = [IS_NOT_EMPTY(error_message='Indique una descripción.')]),

            labels = {
                  'nombre_tentativo': 'Nombre tentativo',
                  'descripcion': 'Descripción',
            },

            submit_button=T('Guardar solicitud'),
        )

    else:
        # Inicializando las listas que se usarán para el drop-down de
        # la vista y para almacenar los correos de los departamentos (vista de profesor o coordinación)
        departamentos = []
        correosDepartamentos = {}

        try:
            page = urllib2.urlopen(settings.dace_ws_url + '/departamentos/').read()
            departments =  json.loads(page, "utf-8")

            for department in departments:

                if (department['email']):
                    correosDepartamentos["(" + department['siglas_depto'] + ")"] = department['email']
                    departamentos.append(('%s - (%s)'%(department['nombre'], department['siglas_depto'])).encode('utf-8','ignore'))

        except urllib2.URLError as e:
            return dict(message=T('Sistema de Gestión de Programas Analíticos de Estudio'))

        form = SQLFORM.factory(
            Field('nombre_tentativo', type='string', notnull=True, required=True,
                requires = [IS_NOT_EMPTY(error_message='Indique un nombre tentativo.')]),
            Field('descripcion', type='text', notnull=True, required=True,
                requires = [IS_NOT_EMPTY(error_message='Indique una descripción.')]),
            Field('departamento', type="string",
                requires = IS_EMPTY_OR(IS_IN_SET(departamentos,
                error_message = 'Seleccione un departamento.',
                zero = "Seleccione..."))),

            labels = {
                  'nombre_tentativo': 'Nombre tentativo',
                  'descripcion': 'Descripción',
                  'departamento': 'Departamento asociado',
            },

            submit_button=T('Guardar solicitud'),

        )

    if form.process().accepted:

        correo_departamento = ""

        if (auth.id_group(role="DEPARTAMENTO")):
            correo_departamento = auth.user.username
        else:
            departamentoSeleccionado = form.vars.departamento
            deptoMatch = re.search("\((\w+)\)",departamentoSeleccionado)
            siglasDeptoSeleccionado = deptoMatch.group(0)
            correo_departamento = correosDepartamentos[siglasDeptoSeleccionado]

       	new_request = db.SOLICITUD_PROGRAMA.insert(
    							nombre_tentativo=form.vars.nombre_tentativo,
    							descripcion=form.vars.descripcion,
                                departamento=correo_departamento,
                                solicitante=auth.user.username,
    	)

        id = new_request.id
        register_in_requests_journal(db, auth, id, 'CREACIÓN', 'Solicitud creada.')
        response.flash = 'Solicitud agregada correctamente.'

        if (auth.id_group(role="DEPARTAMENTO")):
            redirect(URL(c='requests',f='list_sent'))
        else:
            redirect(URL(c='requests',f='list'))



    elif form.errors:
        response.flash = 'Verifique errores en los campos del formulario.'

    	# Registro en la bitácora de la solicitud


    return dict(message=message, form=form)


@auth.requires(auth.is_logged_in() and auth.has_permission('request_ap') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def list():

    '''
        Descripción:
    '''
    message = "Solicitudes en curso"
    # Obtenemos las solicitudes
    solicitudes = db(
        db.SOLICITUD_PROGRAMA.solicitante == auth.user.username).select(
            db.SOLICITUD_PROGRAMA.id,
            db.SOLICITUD_PROGRAMA.nombre_tentativo,
            db.SOLICITUD_PROGRAMA.descripcion,
            db.SOLICITUD_PROGRAMA.estado,
            db.SOLICITUD_PROGRAMA.solicitante,
            db.SOLICITUD_PROGRAMA.fecha_creacion,
            db.SOLICITUD_PROGRAMA.departamento,
            db.SOLICITUD_PROGRAMA.nombre_profesor,
            db.SOLICITUD_PROGRAMA.profesor_asignado,
            db.SOLICITUD_PROGRAMA.observacion_rechazo,
            db.SOLICITUD_PROGRAMA.observacion_profesor,)

    lista_solicitudes = []


    for solicitud in solicitudes:
        lista_solicitudes.append({'id' : solicitud.id,
                                  'nombre_tentativo': solicitud.nombre_tentativo,
                                  'descripcion': solicitud.descripcion,
                                  'departamento': solicitud.departamento,
                                  'estado' : solicitud.estado,
                                  'solicitante' : solicitud.solicitante,
                                  'nombre_profesor': solicitud.nombre_profesor,
                                  'fecha_creacion': solicitud.fecha_creacion,
                                  'profesor_asignado': solicitud.profesor_asignado,
                                  'observacion_rechazo': solicitud.observacion_rechazo,
                                  'observacion_profesor': solicitud.observacion_profesor,
                                })

    formulario_ver_solicitud = SQLFORM.factory(
                                        Field('solicitud_id', type='string'),
                                        Field('nombre_tentativo', label="Nombre tentativo", type='string', notnull=True, required=True),
                                        Field('fecha_creacion', label="Fecha creación", type="string", default = datetime.datetime.today().strftime("%d/%m/%Y %H:%M:%S")),
                                        Field('descripcion', label="Descripción", type='text', required=True),
                                        Field('departamento', label="Departamento", type='string', required=True),
                                        Field('estado', label="Estado", type="string", default='Pendiente'),
                                        Field('solicitante', label="Solicitante", type="string"),
                                        Field('nombre_profesor', label="Nombre profesor", type="string", default="No asignado"),
                                        Field('profesor_asignado', label="Profesor asignado", type="string", default="Ninguno"),
                                        Field('observacion_profesor', label="Observación profesor", type="text", default=""),
                                        Field('observacion_rechazo', label="Observación rechazo", type="text", default=""),
                                        submit_button=T('Modificar solicitud'))

    if formulario_ver_solicitud.process(formname="formulario_ver_solicitud").accepted:

        solicitud_id = formulario_ver_solicitud.vars.solicitud_id
        solicitud_nombre = formulario_ver_solicitud.vars.nombre_tentativo
        solicitud_fecha_creacion = formulario_ver_solicitud.vars.fecha_creacion
        solicitud_descripcion = formulario_ver_solicitud.vars.descripcion
        solicitud_departamento = formulario_ver_solicitud.vars.departamento
        solicitud_estado = "Pendiente"
        solicitud_solicitante = formulario_ver_solicitud.vars.solicitante
        solicitud_nombre_profesor = formulario_ver_solicitud.vars.nombre_profesor
        solicitud_profesor_asignado = formulario_ver_solicitud.vars.profesor_asignado
        solicitud_observacion_profesor = formulario_ver_solicitud.vars.observacion_profesor
        solicitud_observacion_rechazo = formulario_ver_solicitud.vars.observacion_rechazo

        new_data = dict(
                    id=solicitud_id,
                    nombre_tentativo = solicitud_nombre,
                    fecha_creacion = solicitud_fecha_creacion,
                    descripcion = solicitud_descripcion,
                    departamento = solicitud_departamento,
                    estado = solicitud_estado,
                    solicitante = solicitud_solicitante,
                    nombre_profesor = solicitud_nombre_profesor,
                    profesor_asignado = solicitud_profesor_asignado,
                    observacion_profesor = solicitud_observacion_profesor,
                    observacion_rechazo = solicitud_observacion_rechazo,
                )

        db.SOLICITUD_PROGRAMA[solicitud_id] = new_data

        # PENDIENTE REGISTRO EN LA BITÁCORA
        response.flash = "Modificación exitosa."
        redirect(URL(c='requests', f='list'))

    elif formulario_ver_solicitud.errors:
        response.flash = "No se pudieron guardar los cambios."

    return dict(message=message, solicitudes=lista_solicitudes,formulario_ver_solicitud=formulario_ver_solicitud)

@auth.requires(auth.is_logged_in() and auth.has_permission('manage_ap_request') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def list_sent():
    '''
        Descripción: Vista para que un Departamento pueda revisar las solicitudes que le
        han sido enviadas para su aprobación o rechazo
    '''

    message = "Solicitudes en revisión"

    # Obtenemos las solicitudes
    solicitudes = db(
        db.SOLICITUD_PROGRAMA.departamento == auth.user.username).select(
            db.SOLICITUD_PROGRAMA.id,
            db.SOLICITUD_PROGRAMA.nombre_tentativo,
            db.SOLICITUD_PROGRAMA.descripcion,
            db.SOLICITUD_PROGRAMA.estado,
            db.SOLICITUD_PROGRAMA.departamento,
            db.SOLICITUD_PROGRAMA.solicitante,
            db.SOLICITUD_PROGRAMA.fecha_creacion,
            db.SOLICITUD_PROGRAMA.nombre_profesor,
            db.SOLICITUD_PROGRAMA.profesor_asignado,
            db.SOLICITUD_PROGRAMA.observacion_rechazo,
            db.SOLICITUD_PROGRAMA.observacion_profesor,)

    lista_solicitudes = []
    # Obtenemos los roles de cada usuario
    for solicitud in solicitudes:
        lista_solicitudes.append({'id' : solicitud.id,
                                  'nombre_tentativo': solicitud.nombre_tentativo,
                                  'descripcion': solicitud.descripcion,
                                  'departamento': solicitud.departamento,
                                  'estado' : solicitud.estado,
                                  'solicitante' : solicitud.solicitante,
                                  'nombre_profesor': solicitud.nombre_profesor,
                                  'fecha_creacion': solicitud.fecha_creacion,
                                  'profesor_asignado': solicitud.profesor_asignado,
                                  'observacion_rechazo': solicitud.observacion_rechazo,
                                  'observacion_profesor': solicitud.observacion_profesor,
                                })

    formulario_ver_solicitud = SQLFORM.factory(
                                        Field('solicitud_id', type='string'),
                                        Field('nombre_tentativo', label="Nombre tentativo", type='string', notnull=True, required=True),
                                        Field('fecha_creacion', label="Fecha creación", type="string", default = datetime.datetime.today().strftime("%d/%m/%Y %H:%M:%S")),
                                        Field('descripcion', label="Descripción", type='text', required=True),
                                        Field('departamento', label="Departamento", type='string', required=True),
                                        Field('estado', label="Estado", type="string", default='Pendiente'),
                                        Field('solicitante', label="Solicitante", type="string"),
                                        Field('nombre_profesor', label="Nombre profesor", type="string", default="No asignado"),
                                        Field('profesor_asignado', label="Profesor asignado", type="string", default="Ninguno"),
                                        Field('observacion_profesor', label="Observación profesor", type="text", default=""),
                                        Field('observacion_rechazo', label="Observación rechazo", type="text", default=""),
                                        submit_button=T('Modificar solicitud'))

    if formulario_ver_solicitud.process(formname="formulario_ver_solicitud").accepted:

        solicitud_id = formulario_ver_solicitud.vars.solicitud_id
        solicitud_nombre = formulario_ver_solicitud.vars.nombre_tentativo
        solicitud_fecha_creacion = formulario_ver_solicitud.vars.fecha_creacion
        solicitud_descripcion = formulario_ver_solicitud.vars.descripcion
        solicitud_departamento = formulario_ver_solicitud.vars.departamento
        solicitud_estado = formulario_ver_solicitud.vars.estado
        solicitud_solicitante = formulario_ver_solicitud.vars.solicitante
        solicitud_nombre_profesor = formulario_ver_solicitud.vars.nombre_profesor
        solicitud_profesor_asignado = formulario_ver_solicitud.vars.profesor_asignado
        solicitud_observacion_profesor = formulario_ver_solicitud.vars.observacion_profesor
        solicitud_observacion_rechazo = formulario_ver_solicitud.vars.observacion_rechazo

        new_data = dict(
                    id=solicitud_id,
                    nombre_tentativo = solicitud_nombre,
                    fecha_creacion = solicitud_fecha_creacion,
                    descripcion = solicitud_descripcion,
                    departamento = solicitud_departamento,
                    estado = solicitud_estado,
                    solicitante = solicitud_solicitante,
                    nombre_profesor = solicitud_nombre_profesor,
                    profesor_asignado = solicitud_profesor_asignado,
                    observacion_profesor = solicitud_observacion_profesor,
                    observacion_rechazo = solicitud_observacion_rechazo,
                )

        db.SOLICITUD_PROGRAMA[solicitud_id] = new_data

        # PENDIENTE REGISTRO EN LA BITÁCORA
        response.flash = "Modificación exitosa."
        redirect(URL(c='requests', f='list_sent'))



    elif formulario_ver_solicitud.errors:
        response.flash = "No se pudieron guardar los cambios."

    # Formulario para asignar un profesor a una solicitud de programa analítico
    formulario_asignar_profesor = SQLFORM.factory(
                                           Field('nombre_profesor', type="string", notnull=True, required=True,
                                                requires = [IS_NOT_EMPTY(error_message='Indique un nombre y apellido para el profesor.')]),
                                           Field('profesor', type="string", notnull=True, required=True,
                                                requires = [IS_NOT_EMPTY(error_message='Indique el usuario del profesor.')]),
                                           Field('solicitudprof_id', type='string'),
                                           Field('comentario', type='text', requires=IS_LENGTH(512, error_message='Comentario muy extenso, resuma a menos de 512 caracteres.')),
                                                  labels={'nombre_profesor': 'Nombre y apellido del profesor',
                                                          'profesor':'Usuario del profesor',
                                                          'comentario': 'Comentario'},
                                                          submit_button=T('Asignar profesor'),)


    if formulario_asignar_profesor.process(formname="formulario_asignar_profesor").accepted:

        # Se obtienen las variables que el Departamento agregó en el form
        # para la asignación del profesor a la solicitud de programa analítico
        solicitud_id = formulario_asignar_profesor.vars.solicitudprof_id
        nombre_profesor = formulario_asignar_profesor.vars.nombre_profesor
        usuario_profesor = formulario_asignar_profesor.vars.profesor
        observacion_profesor = formulario_asignar_profesor.vars.comentario

        db.SOLICITUD_PROGRAMA[solicitud_id] = dict(estado="En espera de aprobación profesor.",
                                                    nombre_profesor=nombre_profesor,
                                                    profesor_asignado=usuario_profesor,
                                                    observacion_profesor=observacion_profesor,
                                                )

        # PENDIENTE REGISTRO EN LA BITÁCORA
        response.flash = "Asignación de profesor exitosa."
        redirect(URL(c='requests', f='list_sent'))

    elif formulario_asignar_profesor.errors:
        response.flash = 'Error en la asignación del profesor a la solicitud. Intente de nuevo.'

    # Formulario para agregar una observacion para el rechazo
    formulario_rechazo_solicitud = SQLFORM.factory(
                                           Field('solicitud_id_rech', type="string",),
                                           Field('comentario', type='text', requires=IS_LENGTH(512, error_message='Comentario muy extenso, resuma a menos de 512 caracteres.')),
                                                  labels={'solicitud_id_rech':'ID de la solicitud',
                                                          'comentario': 'Comentario'},
                                                  submit_button=T('Rechazar solicitud'),)


    if formulario_rechazo_solicitud.process(formname="formulario_rechazo_solicitud").accepted:

        solicitud_id = formulario_rechazo_solicitud.vars.solicitud_id_rech
        observacion_rechazo = formulario_rechazo_solicitud.vars.comentario
        db.SOLICITUD_PROGRAMA[solicitud_id] = dict(estado="Rechazada", observacion_rechazo=observacion_rechazo)

        # PENDIENTE REGISTRO EN LA BITÁCORA
        response.flash = "Rechazo exitoso."
        redirect(URL(c='requests', f='list_sent'))


    elif formulario_rechazo_solicitud.errors:
        response.flash = "Rechazo fallido."

    return dict(message=message, solicitudes=lista_solicitudes, formulario_asignar_profesor=formulario_asignar_profesor, formulario_rechazo_solicitud=formulario_rechazo_solicitud, formulario_ver_solicitud=formulario_ver_solicitud)


@auth.requires(auth.is_logged_in() and auth.has_permission('fill_ap') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
def list_assigned():
    message = "Solicitudes de Programas Analíticos asignadas por el Departamento"
    solicitudes = db(
        db.SOLICITUD_PROGRAMA.profesor_asignado == auth.user.username).select(
            db.SOLICITUD_PROGRAMA.id,
            db.SOLICITUD_PROGRAMA.nombre_tentativo,
            db.SOLICITUD_PROGRAMA.descripcion,
            db.SOLICITUD_PROGRAMA.estado,
            db.SOLICITUD_PROGRAMA.solicitante,
            db.SOLICITUD_PROGRAMA.departamento,
            db.SOLICITUD_PROGRAMA.fecha_creacion,
            db.SOLICITUD_PROGRAMA.nombre_profesor,
            db.SOLICITUD_PROGRAMA.profesor_asignado,
            db.SOLICITUD_PROGRAMA.observacion_rechazo,
            db.SOLICITUD_PROGRAMA.observacion_profesor,)

    lista_solicitudes = []


    for solicitud in solicitudes:
        lista_solicitudes.append({'id' : solicitud.id,
                                  'nombre_tentativo': solicitud.nombre_tentativo,
                                  'descripcion': solicitud.descripcion,
                                  'departamento': solicitud.departamento,
                                  'estado' : solicitud.estado,
                                  'solicitante' : solicitud.solicitante,
                                  'nombre_profesor': solicitud.nombre_profesor,
                                  'fecha_creacion': solicitud.fecha_creacion,
                                  'profesor_asignado': solicitud.profesor_asignado,
                                  'observacion_rechazo': solicitud.observacion_rechazo,
                                  'observacion_profesor': solicitud.observacion_profesor,
                                })

    formulario_ver_solicitud = SQLFORM.factory(
                                        Field('solicitud_id', type='string'),
                                        Field('nombre_tentativo', label="Nombre tentativo", type='string', notnull=True, required=True),
                                        Field('fecha_creacion', label="Fecha creación", type="string", default = datetime.datetime.today().strftime("%d/%m/%Y %H:%M:%S")),
                                        Field('descripcion', label="Descripción", type='text', required=True),
                                        Field('departamento', label="Departamento", type='string', required=True),
                                        Field('estado', label="Estado", type="string", default='Pendiente'),
                                        Field('solicitante', label="Solicitante", type="string"),
                                        Field('nombre_profesor', label="Nombre profesor", type="string", default="No asignado"),
                                        Field('profesor_asignado', label="Profesor asignado", type="string", default="Ninguno"),
                                        Field('observacion_profesor', label="Observación profesor", type="text", default=""),
                                        Field('observacion_rechazo', label="Observación rechazo", type="text", default=""),
                                        buttons=[])

    # Formulario para aceptar la asignación y que se cree el borrador de programa
    formulario_aceptar_solicitud = SQLFORM.factory(
                                           Field('solicitud_id_accept', type="string",),
                                                  submit_button=T('Aceptar solicitud'),)

    if formulario_aceptar_solicitud.process(formname="formulario_aceptar_solicitud").accepted:
        solicitud_id = formulario_aceptar_solicitud.vars.solicitud_id_accept
        db.SOLICITUD_PROGRAMA[solicitud_id] = dict(estado="Aceptada por profesor")
        solicitud = db(db.SOLICITUD_PROGRAMA.id == solicitud_id).select().first()

        # Se crea el programa en estado de borrador
        new_request = db.PROGRAMA.insert(
                                codigo = "XXX111",
                                original_pdf = "",
                                denominacion = solicitud.nombre_tentativo,
                                estado = "borrador",
                                fecha_elaboracion = datetime.datetime.today().strftime("%d/%m/%Y"),
                                profesor_asignado = solicitud.profesor_asignado,
                                departamento = solicitud.departamento,
        )

        # PENDIENTE REGISTRO EN LA BITÁCORA
        response.flash = "Aprobación exitosa. Se ha creado el borrador"
        redirect(URL(c='requests', f='list_assigned'))

    elif formulario_aceptar_solicitud.errors:
        response.flash = "Aprobación fallido."

    # Formulario para agregar una observacion para el rechazo
    formulario_rechazo_solicitud = SQLFORM.factory(
                                           Field('solicitud_id_rech', type="string",),
                                                  submit_button=T('Rechazar solicitud'),)


    if formulario_rechazo_solicitud.process(formname="formulario_rechazo_solicitud").accepted:
        solicitud_id = formulario_rechazo_solicitud.vars.solicitud_id_rech
        db.SOLICITUD_PROGRAMA[solicitud_id] = dict(estado="Pendiente")

        # PENDIENTE REGISTRO EN LA BITÁCORA
        response.flash = "Rechazo exitoso."
        redirect(URL(c='requests', f='list_assigned'))

    elif formulario_rechazo_solicitud.errors:
        response.flash = "Rechazo fallido."

    return dict(message=message, solicitudes=lista_solicitudes,formulario_ver_solicitud=formulario_ver_solicitud, formulario_aceptar_solicitud=formulario_aceptar_solicitud, formulario_rechazo_solicitud=formulario_rechazo_solicitud)


# PENDIENTE
# @auth.requires(auth.is_logged_in() and auth.has_permission('manage_ap_request', 'auth_user') and not(auth.has_membership(auth.id_group(role="INACTIVO"))))
# def reject_request_as_supervisor():
#     """
#     Permite al supervisor rechazar trsncripcionespendientes,
#     """
#     solic_id = request.args(0)


#     db.SOLICITUD_PROGRAMA[solic_id] = dict(estado = "Rechazada")

#     session.flash = "Transcripción rechazada exitosamente."
#     redirect(URL(c='requests',f='list_sent'))


# TO-DO (Funcion Journal)
