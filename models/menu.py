from web2pytest import web2pytest

response.title = settings.title
response.subtitle = settings.subtitle
response.meta.author = '%(author)s <%(author_email)s>' % settings
response.meta.keywords = settings.keywords
response.meta.description = settings.description

DEVELOPMENT_MENU = True

# Menu de autenticacion
if auth.is_logged_in():
    texto_principal = "Bienvenido, " + auth.user.first_name
else:
    texto_principal = "Bienvenido"

'''
    Opciones de menú desplegable para acciones de un usuario general
'''
opciones = [
    ((SPAN(_class='fa fa-user'), '  Ver Perfil'), False, URL('users', 'profile')),
    ((SPAN(_class='fa fa-question-circle'), '  Ayuda'), False, URL('help', 'index')),
    ((SPAN(_class='fa fa-sign-out'), '  Cerrar Sesión'), False, URL('default', 'logout'))
]

'''
    Opciones de menú desplegable para acciones de un usuario inactivo
'''
opciones_inactivo = [
    ((SPAN(_class='fa fa-sign-out'), '  Cerrar Sesión'), False, URL('default', 'logout'))
]

'''
    Opciones de menú desplegable para
'''
manejo_administrador = [
    ((SPAN(_class='fa fa-archive'), '  Registro de Eventos'), False, URL('administration', 'log')),
]

'''
    Opciones de menú desplegable para acciones de un usuario inactivo
'''
manejo_usuarios = [
    ((SPAN(_class='fa fa-user'), '  Gestionar Usuarios'), False, URL('users', 'filter')),
]

'''
    Menú desplegable para la edición de transcripciones
'''
editar_transcripciones = [
    ((SPAN(_class='fa fa-files-o'), '  Transcripciones en Curso'), False, URL('transcriptions', 'list')),
    ((SPAN(_class='fa fa-clock-o '), '  Transcripciones en Revisión'), False, URL('transcriptions', 'list_sent')),
]

'''
    Menú desplegable para la creación y edición de transcripciones
'''
crear_transcripciones = [
    ((SPAN(_class='fa fa-file-text-o'), ' Nueva Transcripción'), False, URL('transcriptions', 'add'))
]

'''
    Opciones de menú para las solicitudes de programa analítico
'''
crear_solicitud = [
    ((SPAN(_class='fa fa-file-text-o'), ' Nueva Solicitud'), False, URL('requests', 'add')),
]

ver_solicitudes = [
    ((SPAN(_class='fa fa-files-o'), '  Solicitudes en Curso'), False, URL('requests', 'list')),
]

aceptar_solicitud = [
    ((SPAN(_class='fa fa-graduation-cap'), '  Solicitudes asignadas'), False, URL('requests', 'list_assigned')),
]

aprobar_solicitud = [
    ((SPAN(_class='fa fa-clock-o '), '  Solicitudes en Revisión'), False, URL('requests', 'list_sent')),
]

'''
    Menú desplegable para la gestión de borradores de transcripciones
'''
manejar_transcripciones = [
    ((SPAN(_class='fa fa-user'), '  Transcriptores'), False, URL('transcriptions', 'transcriptors')),
    ((SPAN(_class='fa fa-eye'), '  Seguimiento de Transcripciones'), False, URL('transcriptions', 'following')),
    ((SPAN(_class='fa fa-clock-o'), '  Transcripciones por Revisión'), False, URL('transcriptions', 'list_pending')),
    ((SPAN(_class='fa fa-check'), '  Transcripciones Aprobadas'), False, URL('transcriptions', 'list_approved')),
]

'''
    Menú desplegable para la gestión de borradores de solicitudes
'''
manejar_solicitudes = [
    ((SPAN(_class='fa fa-file-text-o'), '  Proponer Borrador'), False, URL('programs', 'add')),
    ((SPAN(_class='fa fa-graduation-cap'), '  Profesores'), False, URL('programs', 'teachers')),
    ((SPAN(_class='fa fa-eye'), '  Seguimiento de Borradores'), False, URL('programs', 'following')),
    ((SPAN(_class='fa fa-clock-o'), '  Borradores por Revisión'), False, URL('programs', 'list_pending')),
    ((SPAN(_class='fa fa-check'), '  Borradores Aprobados'), False, URL('programs', 'list_approved')),
]

'''
    Menú desplegable para la gestión de borradores de programas analíticos
'''
escribir_borrador = [
    ((SPAN(_class='fa fa-eye'), '  Seguimiento de Borradores'), False, URL('drafts', 'list')),
]

manejar_borradores = [
    ((SPAN(_class='fa fa-clock-o'), '  Borradores por Revisión'), False, URL('drafts', 'list_pending')),
    ((SPAN(_class='fa fa-check'), '  Borradores Aprobados'), False, URL('drafts', 'list_approved')),
]

'''
    Menú desplegable para la consulta de programas sinopticos
'''
opciones_sinoptico = [
    ((SPAN(_name='specific-programs', _class='fa fa-file-text-o'), '  Especificos'), False, URL('sinoptic', 'search')),
    ((SPAN(_name='current-programs', _class='fa fa-file'), '  Vigentes'), False, URL('sinoptic', 'search')),
    ((SPAN(_name='all-programs', _class='fa fa-files-o '), '  Todos'), False, URL('sinoptic', 'search'))
]

'''
    Menú desplegable para la consulta de 
'''

opciones_biblioteca = [
    ((SPAN(_name='specific-books', _class='fa fa-file-text-o'), ' En programas específicos'), False, URL('books', 'search')),
    ((SPAN(_name='current-books', _class='fa fa-file'), ' En programas vigentes'), False, URL('books', 'search')),
    ((SPAN(_name='all-books', _class='fa fa-files-o'), ' En todos los programas'), False, URL('books', 'search',args='all'))
]

'''
    Menú desplegable para la consulta de programas analíticos
'''
consultar_programas = [
    ((SPAN(_class='fa fa-file-powerpoint-o'), '  Listar Programas'), False, URL('programs', 'list')),
]

queries = [
    ((SPAN(_class='fa fa-chevron-down'), '  Departamentos'), False, URL('queries', 'departments')),
    ((SPAN(_class='fa fa-chevron-down'), '  Asignaturas'), False, URL('queries', 'subjects')),
    ((SPAN(_class='fa fa-chevron-down'), '  Carreras'), False, URL('queries', 'careers')),
    ((SPAN(_class='fa fa-chevron-down'), '  Estudiantes'), False, URL('queries', 'students')),
]

opciones_estudiante = [
    ((SPAN(_class='fa fa-check'), '  Asignaturas Aprobadas'), False, URL('students', 'list')),
]

menu_autenticado = [
    (texto_principal, False, '#', opciones)
]

menu_opciones_rol = []

'''
Agregando el menu de acuerdo a las permisologias de los usuarios
'''

'''
Permisos de administración de DACE
'''
if auth.has_permission('manage_users', 'auth_user'):
    menu_opciones_rol.append(('Administración', False, '#', manejo_administrador))
    menu_opciones_rol.append(('Usuarios', False, '#', manejo_usuarios))
    menu_opciones_rol.append(('Consultas', False, '#', queries))

'''
Agregar consulta de programas para todos los usuarios
'''
if not(auth.has_membership(auth.id_group(role="INACTIVO"))):
    menu_opciones_rol.append(('Programas', False, '#', consultar_programas))

'''
Agrega consulta de sinopticos para los usuario con rol DECANATO
'''
if auth.has_membership(auth.id_group(role="DECANATO")):
    menu_opciones_rol.append(('Sinoptico', False, '#', opciones_sinoptico))    

'''
Agrega consulta de libros para los usuario con rol BIBLIOTECA
'''
if auth.has_membership(auth.id_group(role="BIBLIOTECA")):
    menu_opciones_rol.append(('Libros', False, '#', UL(LI(A(SPAN(_name='specific-books', _class='fa fa-file-text-o')," En Programas Especificos",_href = URL('books','search'))),
    LI(A(SPAN(_name='specific-books', _class='fa fa-file')," En Programas Vigentes"), _class="disabled",_href = URL('books','search')),
    LI(A(SPAN(_name='specific-books', _class='fa fa-files-o')," En todos los programas"),_class="disabled", _href = URL('books','search', args='all')))))

'''
Se construye el dropdown para trabajar con transcripciones
'''
if ((auth.has_permission('create_transcription')) or
    (auth.has_permission('work_on_transcription')) or
    (auth.has_permission('manage_transcription'))):

    opciones_transcrip = []

    if auth.has_permission('create_transcription'):
        ''' Se agrega la opcion de crear una nueva transcripcion, solo si es un Profesor '''
        opciones_transcrip = opciones_transcrip + crear_transcripciones

    if auth.has_permission('work_on_transcription'):
        ''' Se agrega la capacidad de ver la lista de transcripciones correspondientes '''
        opciones_transcrip = opciones_transcrip + editar_transcripciones

    if auth.has_permission('manage_transcription'):
        ''' Se agrega la capacidad de ver la lista de transcripciones correspondientes '''
        opciones_transcrip = opciones_transcrip + manejar_transcripciones

    menu_opciones_rol.append(('Transcripciones', False, '#', opciones_transcrip))


if auth.has_permission('request_ap'):

    opciones = crear_solicitud

    if auth.has_permission('consult_ap_request'):
        opciones = opciones + ver_solicitudes

    if auth.has_permission('manage_ap_request'):
        opciones = opciones + aprobar_solicitud

    if auth.has_permission('manage_assignment'):
        opciones = opciones + aceptar_solicitud

    menu_opciones_rol.append(('Solicitudes', False, '#', opciones))

if auth.has_permission('manage_ap_request'):
    menu_opciones_rol.append(('Borradores de Programas', False, '#', manejar_borradores))

if auth.has_permission('fill_ap'):
    menu_opciones_rol.append(('Borradores de Programas', False, '#', escribir_borrador))

if auth.has_permission('consult_ap'):
    menu_opciones_rol.append(('Estudiantes', False, '#', opciones_estudiante))

if web2pytest.is_running_under_test(request, request.application):
    response.menu = [
        (T('Iniciar Sesión'), False, settings.hostname_url + "SIGPAE/default/_test_login/", []),
    ]
else:
    response.menu = [
        (T('Iniciar Sesión'), False, settings.cas_login_url + settings.returnurl, []),
    ]

# response.menu = [
#     (T('Iniciar Sesión'), False , settings.cas_login_url + settings.returnurl, []),
# ]

if auth.has_membership(auth.id_group(role="INACTIVO")):

    menu_autenticado = [
        (texto_principal, False, '#', opciones_inactivo)
    ]

    menu_opciones_rol = []
