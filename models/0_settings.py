'''
0_settings.py

This file describes the globals settings for SIGPAE, such as title, db uri,
host names, and more. It is prefixed with '0_'
in order to Web2Py execute it before any other file in the models folder.

This file defines a Web2Py, system-visible variable called 'settings' which
will hold all these needed values. The values are read from the 'appconfig.json'.

The values from appconfig.json are read from appconfig.ini, and are converted to
JSON using: https://www.site24x7.com/tools/ini-to-json.html

'''

from gluon.storage import Storage
from gluon.contrib.appconfig import AppConfig

myconf = AppConfig(reload=True)
settings = Storage()

# App
settings.title = myconf.get('app.title')
settings.subtitle = myconf.get('app.subtitle')
settings.author   = myconf.get('app.author')

# Host
settings.hostname_url = myconf.get('host.hostname_url')
settings.login_method = myconf.get('host.login_method')
settings.returnurl = myconf.get('host.return_url')
settings.cas_login_url = myconf.get('host.cas_login_url')
settings.cas_verify_url = myconf.get('host.cas_verify_url')
settings.cas_logout_url = myconf.get('host.cas_logout_url')

# Dace Web Services
settings.dace_ws_url = myconf.get('dace_ws.url')

# Library of Congress
settings.loc_service_url = myconf.get('loc.service_url')

# DB management
settings.migrate = bool(myconf.get('db.migrate'))
settings.database_uri = myconf.get('db.uri')
settings.test_database_uri = myconf.get('db.test_uri')

# Email
settings.email_server = str(myconf.get('smtp.server'))
settings.email_sender = str(myconf.get('smtp.sender'))
settings.email_login  = str(myconf.get('smtp.login'))
