# SIGPAE 2.0

## Resumen

Este repositorio contiene el código fuente necesario para SIGPAE. En este README se encuentra la información necesaria para la instalación en ambiente de desarrollo y producción.

Para **SIGPAE WS** leer el README correspondiente al repositorio. Se recomienda primero seguir las instrucciones de SIGPAE.

## Estructura del repositorio

La rama ```master``` contiene el código fuente para el ambiente de Desarrollo (local). La rama ```deploy``` es la versión más actualizada en el ambiente de producción. Existen diferencias entre las configuraciones de ambas ramas. Por favor tener esto en cuenta al momento de hacer merge con la rama ```deploy```

## Dirección del Servidor

El servidor se encuentra disponible en la siguiente dirección https://159.90.61.100.

El usuario y la clave de acceso para la conexión SSH con el servidor serán entregados en un documento aparte.

## Instalación

### Web2Py

Primero, se debe descargar Web2Py [desde el sitio web oficial](http://www.web2py.com/init/default/download) y descomprimir el archivo .zip en el directorio de su preferencia.

Una vez hecho esto, dirigirse a ```web2py/applications/``` y clonar este repositorio. Tambien se debe clonar dentro de este directorio **SIGPAE WS**.

### Librerías Requeridas

Luego de la instalación de Web2Py, ejecute los siguientes comandos para instalar las librerías necesarias del sistema.

- Necesarias para la conexión con el CAS

```bash
$ sudo apt-get update
$ sudo apt-get install libsasl2-dev python-dev libldap2-dev libssl-dev ldap-utils
```

- Necesarias para la lectura de PDF (texto e imágenes)

```bash
$ sudo apt-get install xpdf
$ sudo apt-get install libmagickwand-dev
$ sudo apt-get install tesseract-ocr libtesseract-dev libleptonica-dev
$ sudo apt-get install tesseract-ocr-eng tesseract-ocr-spa
```

- Necesarias para el funcionamiento de citeproc
```bash
$ sudo apt-get install libxslt1.1 libxslt1-dev libxml2-dev libxml2
```

- Necesarias para el funcionamiento de algunos módulos de SIGPAE

```bash
$ cd web2py/applications/SIGPAE/
$ sudo pip install -r requirements.txt
```

### Base de Datos

Realizar la instalación de PostgreSQL de manera habitual.
```
$ sudo apt-get install postgresql-9.6
```
Luego, crear la base de datos.

Ingresar a PostgreSQL

```bash
$ sudo -su postgres
$ psql
```
Crear el usuario y la base de datos si no existen en la consola de postgres.

```bash
$ create user sigpae with password '123123';
$ create database newsigpae with owner sigpae;
$ create database newsigpae_test with owner sigpae;
```  

**SIGPAE** y **SIGPAE WS** usan el mismo usuario y password. En la version de desarrollo (rama ```master```) y la versión de producción (rama ```deploy```) los passwords difieren.

### Puertos

Además de los puertos usuales para una applicación web, el servidor donde se ejecuta
__SIGPAE debe poder iniciar conexiones con el puerto 210 de hosts remotos__ usando el
protocólo TCP para agregar referencias bibliográficas a transcripciones de
programas.

### Archivo de configuración
Se le ha debido proveer un archivo de configuración en formato `JSON`, llamado
`appconfig.json`. Este archivo contiene la configuración básica del sistema y
que es leído en distintos módulos.

Para instalar este archivo de configuración, debe crear la carpeta `private`
desde la raíz de SIGPAE.

```bash
mkdir private
```

Luego, mover el archivo `appconfig.json` a esta carpeta.

_NOTA_: Si hay algún cambio en este archivo, debe comunicárselo a TODOS los desarrolladores de SIGPAE y cambiarlo en la versión que se encuentra alojada en el servidor.

## Ejecución

### Al ejecutar la aplicación la primera vez.

Verifique que postgres se encuentre en ejecución.

El sistema SIGPAE requiere que exista al menos un usuario administrador para poder funcionar. Sin embargo, la primera vez que se ejecuta la aplicación, este usuario no existe.

El primer usuario en iniciar sesión tendrá asignado el rol de administrador de forma automática. Para que esto suceda, hay que seguir los siguientes pasos.

1. Abrir el archivo db.py (en SIGPAE/models) y comentar la línea. Esto permitirá iniciar sesión la primera vez.

    ``` python
    auth.add_membership(auth.id_group(role="DACE-ADMINISTRADOR"), 1)
    ```

2. Desde el directorio raíz de Web2Py, ejecutar en su terminal:

    ```bash
    $ python web2py.py
    ```  

3. Asignar la contraseña  de administrador **123123** en la interfaz de Web2Py.

4. Abrir la interfaz de la aplicación en http://localhost:8000/SIGPAE

5. Iniciar sesión con sus credenciales de la USB.

6. Descomentar la línea mencionada en el paso 1. Web2Py se encargará de hacer la recarga del servidor de forma automática.

7. Refrescar la vista actual. En este momento debería tener asignado el rol de administrador del sistema y podrá ejecutar la aplicación sin problemas.

### Ejecución habitual

Para ejecutar la aplicación en otro momento, bastará con ejecutar el paso 2.

### En Servidor

Subir los cambios al repositorio de git y ejecutar en el servidor via SSH.
```bash
$ cd /home/www-data/web2py/applications/SIGPAE_WS
$ sudo git pull
```  
Los cambios generalmente estarán disponibles de inmediato. Algunos cambios pudiesen requerir reiniciar el servicio de Apache del servidor de manera habitual.

```bash
$ sudo /etc/init.d/apache2 restart
```  

 Uno de los cambios mas comunes que requiere de esto son los realizados a los scripts en el directorio ```modules```.

### Ejecucion de Pruebas

Irse a la carpeta raiz de web2py y ejecutar el siguiente comando

```bash
$ py.test -xvs applications/SIGPAE/tests/
```  

## Desarrolladores

### Abril - Julio 2017
- [Leonardo Martínez](https://github.com/leotms)
- [Jonnathan Chiu Ng](https://github.com/Stahet)
- [Nicolás Mañán](https://github.com/nmanan)    


### Septiembre - Diciembre 2017
- [Alejandra Cordero](https://github.com/alejandra21)
- [Fernando Pérez](https://github.com/fernandobperezm)
- [Pablo Maldonado](https://github.com/prmm95)

## Última Actualización
- 30 de septiembre de 2017.
