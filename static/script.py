from __future__ import (absolute_import, division, print_function,
                        unicode_literals)


from pymarc import Record, Field
from pymarc import MARCReader
from pymarc import marcxml
from marc2bib import convert



from citeproc.py2compat import *

# Import the citeproc-py classes we'll use below.
from citeproc import CitationStylesStyle, CitationStylesBibliography
from citeproc import formatter
from citeproc import Citation, CitationItem

from citeproc.source.bibtex import BibTeX

import sys



# Se verifican los argumentos introducidos.
if (len(sys.argv) != 3):
	exit(0)
else:
	indice = int(sys.argv[1])
	url_query = sys.argv[2]

records = []

def warn(citation_item):
    print("WARNING: Reference with key '{}' not found in the bibliography."
          .format(citation_item.key))

while (len(records) == 0):
	records = marcxml.parse_xml_to_array(url_query)

citeKey = str(records[indice].title())+str(indice)

with open("./test.bib", "w") as text_file:
    text_file.write(convert(records[indice],bibkey=citeKey))

bib_source = BibTeX('test.bib')

bib_style = CitationStylesStyle('harvard1', validate=False)

bibliography = CitationStylesBibliography(bib_style, bib_source,
                                          formatter.plain)
citation1 = Citation([CitationItem(citeKey)])

bibliography.register(citation1)

for item in bibliography.bibliography():
    print(str(item))