# README para las pruebas
Este archivo pretende ser una guía para explicar el proceso de instalación de suites de pruebas en Web2Py, específicamente en SIGPAE.

Todos los pasos se encuentran en [Repo1](https://github.com/viniciusban/web2py.test) y [Repo2](https://github.com/jredrejo/bancal/tree/master/web2py/applications/bancal/tests) (que parece haberse basado en repo1)

## Resumen de lo importante

* tests/conftest.py -> configure test environment.
* modules/web2pytest/web2pytest.py -> test infrastructure.
* models/db.py -> ask web2pytest about tests and create db accordingly.

## Aspectos a considerar cuando se haga una prueba
Todas las llamadas a BD de datos a través del `fixture web2py` (ver test/conftest.py)
deben terminar con un commit a la BD para evitar locks cuando se intenten
truncar las tablas en las pruebas siguientes.

Un fixture de ejemplo:
```python
@pytest.fixture(params=["ESTUDIANTE","PROFESOR","EGRESADO","ADMINISTRATIVO","ORGANIZACION","EMPLEADO"])
def add_update_profile_auth_user_permission(request, web2py, create_user):
    web2py.db.auth_group.insert(role = request.param, description = "")
    web2py.auth.add_permission(web2py.auth.id_group(role=request.param), 'update_profile', 'auth_user')
    web2py.auth.add_membership(web2py.auth.id_group(role=request.param), create_user['id'])

    ...

    web2py.db.commit()

    ...

    return ...
```

Una prueba de ejemplo:
```python
def test_get_roles_empty(web2py):

    ...

    assert(web2py.db(web2py.db.auth_group).count() == 0)

    ...

    # Finish the test
    web2py.db.commit()
```

## Correr las pruebas

```bash
(env) web2py: $ python web2py.py -a 123123 --nogui &
(env) web2py: $ py.test -x -v -s applications/SIGPAE/tests
```

## Consideraciones especiales al hacer pruebas de módulos de web2py
Al hacer pruebas de módulos de web2py debemos importar la función que se está probando
dentro de la función de prueba para que la dependencia pueda ser resuelta.
Por ejemplo, para el módulo webservice_queries:

```python
def test_get_json(web2py, mocker):
    from webservice_queries import get_json
    ...
    get_json(url) # Se importa correctamente la función get_json
```
