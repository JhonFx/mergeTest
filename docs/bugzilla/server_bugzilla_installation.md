# Instrucciones para instalar Bugzilla local.
Estas son las instrucciones para instalar Bugzilla usando un servidor Arpache en la máquina local, bajo la rama `master` siguiendo las instrucciones en `https://bugzilla.readthedocs.io/en/5.0/installing/linux.html`

## Instalación de dependencias.
ejecutar
```bash
sudo apt-get install git nano

sudo apt-get install apache2 libappconfig-perl libdate-calc-perl libtemplate-perl libmime-perl build-essential libdatetime-timezone-perl libdatetime-perl libemail-sender-perl libemail-mime-perl libemail-mime-modifier-perl libdbi-perl libcgi-pm-perl libmath-random-isaac-perl libmath-random-isaac-xs-perl libapache2-mod-perl2 libapache2-mod-perl2-dev libchart-perl libxml-perl libxml-twig-perl perlmagick libgd-graph-perl libtemplate-plugin-gd-perl libsoap-lite-perl libhtml-scrubber-perl libjson-rpc-perl libtheschwartz-perl libtest-taint-perl libauthen-radius-perl libfile-slurp-perl libencode-detect-perl libmodule-build-perl libnet-ldap-perl libfile-which-perl libauthen-sasl-perl libtemplate-perl-doc libfile-mimeinfo-perl libhtml-formattext-withlinks-perl libgd-dev lynx-cur graphviz python-sphinx rst2pdf postgresql-server-dev-9.4

```

## Verificación de Perl.
La versión de Perl del sistema debe ser mayor a la 5.14.0
```bash
perl -v
```

## Descargar Bugzilla.
Bugzilla debe colocarse dentro de la carpeta `/var/www/html`  de `Apache` junto con la carpeta de `Web2Py`. Creando la carpeta en `home`
```bash
cd /var/www/html/
git clone --branch release-5.0-stable https://github.com/bugzilla/bugzilla
```

## Instalar requerimientos de Bugzilla (apartes)
Para asegurarnos que tenemos todos los requerimientos para Bugzilla. Ellos recomiendan hacer todos los pasos en las próximas partes como su.
```bash
su
./checksetup.pl --check-modules
./install-module.pl Email::Reply
./install-module.pl Daemon::Generic
./install-module.pl DBD:Pg
./install-module.pl PatchReader
./install-module.pl Cache::Memcached
./install-module.pl File::Copy::Recursive
```
Al ejecutar estos comandos se debe chequear que el módulo de PostgresQL esté instalado y que todos los módulos estén instalados.

## Configuración de Apache con Bugzilla.
Ref: `https://bugzilla.readthedocs.io/en/5.0/installing/apache.html#apache`
Evitar que Apache guarde las contraseñas en texto plano como parte de sus logs.
- Abrir el archivo de configuración de Apache: `nano /etc/apache2/apache2.conf`
- En la línea donde se encuentre el logging format `vhost_combined`, más específicamente, `LogFormat "%v:%p %h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" vhost_combined`: Reemplazar `%r` por `%m %U`
- `sudo apachectl start`

### Apache con mod_cgi
- Abrir el archivo de configuración de Apache: `nano /etc/apache2/sites-available/default.conf`
- Copiar lo siguiente dentro del virtualhost :80 así como se muestra:
```
<VirtualHost *:80>
	... (configuración antigua, no cambiar)

	###########################################################################
	###########################################################################
	########################  BUGZILLA CONFIGURATION  #########################
	###########################################################################
	###########################################################################
	Alias /bugs/ /var/www/html/bugzilla/
	<Directory /var/www/html/bugzilla>
		AddHandler cgi-script .cgi
		Options +ExecCGI +FollowSymLinks
		DirectoryIndex index.cgi index.html
		AllowOverride All
	</Directory>
</VirtualHost>
```

- Luego ejecutar: `a2enmod cgi`
- Reiniciar el servidor de apache: `service apache2 restart`

## Configuración de PostgreSQL y Bugzilla.
Ref: `https://bugzilla.readthedocs.io/en/5.0/installing/postgresql.html#postgresql`

Se necesita verificar que PostgreSQL está en una versión mayor o igual que la 8.03.0000. Seguidamente se crea el usuario `bugs`, donde para la versión de producción, la contraseña es: `SIGPAEd3v3l0p`.
```bash
psql -V
su - postgres
createuser -U postgres -dRSP bugs
```

Luego se edita la configuración de Postgres para poder conectar Bugzilla en `127.0.0.1` hacia todas las bases de datos, usando el usuario `bugs`. Para esto, le pedimos a Postgres que nos diga dónde está el archivo `pg_hba.conf`.
```bash
su - postgres
psql
SHOW hba_file;
```

Luego, editamos ese archivo que está en `/etc/postgresql/9.4/main/pg_hba.conf` y añadimos el host (con Tabs).
```bash
nano /etc/postgresql/9.4/main/pg_hba.conf
host	all    bugs   127.0.0.1    255.255.255.255  md5
sudo service postgresql stop
sudo service postgresql start
```

## Configuración Local.
```bash
./checksetup.pl
```

Esto arrojará ciertas variables dentro de un archivo de Perl que deben configurarse, en este caso, `$db_driver`, `$db_pass` y `$webservergroup`.

```bash
nano localconfig
```
```perl
$db_driver = 'Pg'
$db_pass = 'SIGPAEd3v3l0p'
$webservergroup = 'www-data'
```

Luego de realizar estos cambios, volver a ejectutar: `./checksetup.pl`

En este punto, se pidió un administrador, correo, y contraseña. Éstos fueron `12-11152`, `Fernando Benjamin Perez Maurera` y `SIGPAEd3v3l0p`.

Lo siguiente es hacer un proxy para llevar la aplicación a otro puerto.
```bash

```

# Web2Py + Apache
```bash
apt-get update
apt-get -y upgrade
apt-get -y install openssh-server
apt-get -y install python
apt-get -y install python-dev
apt-get -y install apache2
apt-get -y install libapache2-mod-wsgi
apt-get -y install libapache2-mod-proxy-html
```

```bash
sudo ln -s /etc/apache2/mods-available/proxy_http.load            /etc/apache2/mods-enabled/proxy_http.load
sudo a2enmod ssl
sudo a2enmod proxy
sudo a2enmod proxy_http
sudo a2enmod wsgi
```

Crear conf de web2py y apache.
```
nano /etc/apache2/sites-available/web2py
```

## Configuración en Gitlab.
