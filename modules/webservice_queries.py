# -*- coding: utf-8 -*-
import urllib2
import json

def get_json(url, data=None, **kwargs):
    """
    Request GET parseado como json. Devuelve None en caso de error en la URL

    @param url  URL al que se hará el request
    @param data Parámetros GET que se pasarán
    @return dict | None
    """
    try:
        page = urllib2.urlopen(url, data=data).read()
        json_response = json.loads(page)
    except urllib2.URLError as e:
        return None
    return json_response


def subject_details(cod, ws_url):
    subject = get_json('{ws_url}/asignaturas/{cod}/'.format(ws_url=ws_url, cod=cod))

    if subject and len(subject) > 0:
        subject = subject[0]

    return subject

def list_department_subjects(ws_url, department_code):
    return get_json('{ws_url}/asignaturas?siglas_depto={cod}'.format(ws_url=ws_url, cod=department_code))

def list_career_subjects(ws_url, career_code):
    return get_json('{ws_url}/asignaturas?cod_carrera={cod}'.format(ws_url=ws_url, cod=career_code))

def list_departments(ws_url):
    return get_json('{ws_url}/departamentos'.format(ws_url=ws_url))

def list_subjects(ws_url):
    return get_json('{ws_url}/asignaturas'.format(ws_url=ws_url))

def list_careers(ws_url):
    return get_json('{ws_url}/carreras'.format(ws_url=ws_url))

def asignaturas_vigentes(ws_url):
    return get_json('{ws_url}/asignaturas_vigentes'.format(ws_url=ws_url))

def asignaturas_no_vigentes(ws_url):
    return get_json('{ws_url}/asignaturas_no_vigentes'.format(ws_url=ws_url))

