# -*- coding: utf-8 -*-
from gluon import current
from gluon.dal import DAL, Field
from gluon.validators import *
from gluon.http import *
from gluon.html import *
import os
import sys
import io
import re
import pyocr
import pyocr.builders
import StringIO
from gluon.sqlhtml import SQLFORM
from gluon.languages import *
# Librerias para realizar las citas
#from citeproc.py2compat import *
#from citeproc import CitationStylesStyle, CitationStylesBibliography
#from citeproc import formatter
#from citeproc import Citation, CitationItem
#from citeproc.source.bibtex import BibTeX
#from pybtex.database import BibliographyData, Entry
from citations import *

# Para el buscador de video
import urllib
import urllib2
import json
# Para el buscador de articulos
from scholar import *


#request = current.request
T = current.T


#----------------------------BUSCAR LIBROS----------------------------------#
def form_buscador_libros():
    """Genera un formulario para la busqueda de libros"""
    new_field_form_books = SQLFORM.factory(
            Field('titulo', type="string", required=True),
            Field('autores', type="string", required=True),
            Field('anio', type = 'integer'),
            Field('edicion', type="integer", required=True),
            labels = {
                'titulo' : 'Título',
                'autores': 'Autor principal \n (Apellido, nombre)',
                'anio'   : 'Año',
                'edicion': 'Edición',
            },
            submit_button=T('Buscar libro')
            )
    return new_field_form_books

def procesar_form_buscador_libros(new_field_form_books,id,text,previous_page):
    if new_field_form_books.process(formname = "new_field_form_books").accepted:
        titulo  = ''
        autores = ''
        anio    = ''
        edicion = ''

        if new_field_form_books.vars.titulo:
            # Se parsea el XML del la busqueda solicitada
            titulo = new_field_form_books.vars.titulo
        if new_field_form_books.vars.autores:
            autores = new_field_form_books.vars.autores
        if new_field_form_books.vars.anio:
            anio = new_field_form_books.vars.anio
        if new_field_form_books.vars.edicion:
            edicion = new_field_form_books.vars.edicion

        redirect(
          URL(
            c='transcriptions',
            f='listarLibros',
            vars={
              'id'          : id,
              'titulo'      : titulo,
              'autores'     : autores,
              'anio'        : anio,
              'edicion'     : edicion,
              'vistaRetorno': "edit",
              'text'        : text,
              'previous_page': previous_page
            }
          ),
          client_side = True
        )

#-------------------------BUSCAR VIDEO-----------------------------
def form_buscador_videos():
    """formulario para el buscador de vídeos"""
    new_field_form_video = SQLFORM.factory(
            Field('titulo', type="string", required=True),
            labels = {
                'titulo' : 'Título del vídeo'
            },
            submit_button=T('Buscar vídeo')
            )
    return new_field_form_video

def procesar_form_buscador_videos(new_field_form_video,id,text,previous_page):
    if new_field_form_video.process(formname = "new_field_form_video").accepted:
        titulo  = ''

        if new_field_form_video.vars.titulo:
          titulo = new_field_form_video.vars.titulo

        redirect(
          URL(
            c='transcriptions',
            f='list_videos',
            vars={
              'id'           : id,
              'titulo'       : titulo,
              'vistaRetorno' : "edit",
              'text'         : text,
              'previous_page': previous_page
            }
          ),
          client_side = True
        )

#-------------------------BUSCAR ARTICULO-----------------------------
def form_buscador_articulos():
    """formulario para el buscador de articulos"""
    new_field_form_journal = SQLFORM.factory(
            Field('titulo', type="string", required=True),
            Field('autores', type="string", required=True),
            labels = {
                'titulo' : 'Título',
                'autores': 'Autor(es)'
            },
            submit_button=T('Buscar articulo')
            )
    return new_field_form_journal

def procesar_form_buscador_articulos(new_field_form_journal,id,text,previous_page):
   if new_field_form_journal.process(formname = "new_field_form_journal").accepted:

        titulo  = ''
        autores = ''

        if new_field_form_journal.vars.titulo:
            titulo = new_field_form_journal.vars.titulo
        if new_field_form_journal.vars.autores:
            autores = new_field_form_journal.vars.autores

        # Si el usuario no introduce ni titulo ni autor no se ejecutara la
        # busqueda.
        if (not(new_field_form_journal.vars.titulo) and not(new_field_form_journal.vars.autores)):

          current.session.flash = 'Introduzca un título o un autor para buscar el artículo.'
          redirect(URL(c='transcriptions',f='edit',
                      vars={'id' : id}), client_side = True)

        print("PROCESAR FORM BUSQUEDA ARTICULO")
        print(titulo)
        print(autores)

        print("Tipo de titulo:")
        print(type(titulo))
        # Se ejecuta la busqueda de articulo.
        redirect(
          URL(
            c='transcriptions',
            f='listar_jounals',
            vars={
              'id'           : id,
              'titulo'       : titulo,
              'autores'      : autores,
              'vistaRetorno' : "edit",
              'text'         : text,
              'previous_page': previous_page
            }
          ),
          client_side = True
	)

#-------------------------AÑADIR FUENTE WEB (SELECCIONAR TIPO)-----------------------------
def form_anadir_fuentes_web():
    """formulario para las fuentes web"""
    TIPOS_FUENTES_WEB = (
        ("fuente_web1", "Cursos Online, blog, portal web"),
        ("fuente_web2", "Items de: Cursos Online, blog o portal web"),
        ("otros", "Otros")
    )

    new_field_form_fuentes_web = SQLFORM.factory(
            Field('tipo_fuente_web', type ='string',  requires = IS_IN_SET(TIPOS_FUENTES_WEB,
                zero='Seleccione', error_message = 'Seleccione un tipo de fuente web.')),

            labels = {
                'tipo_fuente_web' : 'Tipos de fuentes web'},
            submit_button=T('Aceptar')
    )
    return new_field_form_fuentes_web

def procesar_form_anadir_fuentes_web(new_field_form_fuentes_web,id,previous_page):
    """Procesamiento para agregar referencias web"""
    if new_field_form_fuentes_web.process(formname = "new_field_form_fuentes_web").accepted:

      if new_field_form_fuentes_web.vars.tipo_fuente_web:
        tipo_fuente_web = new_field_form_fuentes_web.vars.tipo_fuente_web

        redirect(
          URL(
            c='transcriptions',
            f='add_web_source',
            vars={
              'id'              : id,
              'tipo_fuente_web' : tipo_fuente_web,
              'vistaRetorno'    : "edit",
              'previous_page'    : previous_page
            }
          ),
          client_side = True
        )

#-------------------------AÑADIR OTRAS FUENTES-----------------------------
def form_anadir_otras_fuentes():
    """formulario para otras fuentes de información"""
    new_field_form_otras_fuentes = SQLFORM.factory(
            Field('titulo', type="string", requires=IS_NOT_EMPTY(error_message='Por favor, introduzca un titulo')),
            Field('autor', type="string",required=True),
            Field('coautores', type="string"),
            Field('colectivo', type="string"),
            Field('tipo_material', type='string'),
            Field('elemento_referencial',  type='string'),
            Field('pubyear',  type='string'),
            Field('comentario_otra_fuente', type='text'),
            labels = {
                'titulo' : '* Título',
                'autor': 'Autor principal \n (Apellido, nombre)',
                'coautores': 'Coautor(es)',
                'colectivo': 'Autor institucional',
                'tipo_material': 'Tipo de Material',
                'elemento_referencial': 'Elemento referencial',
                'pubyear': 'Año de publicación',
                'comentario_otra_fuente': 'Comentario'
            },
            submit_button=T('Crear Referencia')
    )
    return new_field_form_otras_fuentes

def autores_form_check(form):
    if not(form.vars.autor or form.vars.coautores or form.vars.colectivo or form.vars.institucion):
        form.errors.autor = 'Debe haber al menos un autor, o un coautor, o un autor institucional'

def procesar_form_anadir_otras_fuentes(new_field_form_otras_fuentes,id,db, previous_page):
    if new_field_form_otras_fuentes.process(formname = "new_field_form_otras_fuentes", onvalidation=autores_form_check).accepted:
        titulo = new_field_form_otras_fuentes.vars.titulo
        autor  = new_field_form_otras_fuentes.vars.autor
        coautores  = new_field_form_otras_fuentes.vars.coautores
        colectivo  = new_field_form_otras_fuentes.vars.colectivo
        pubyear = new_field_form_otras_fuentes.vars.pubyear
        comentario = new_field_form_otras_fuentes.vars.comentario_otra_fuente
        tipo_material = new_field_form_otras_fuentes.vars.tipo_material
        elemento_referencial = new_field_form_otras_fuentes.vars.elemento_referencial

        autorHarvard = formar_cita_autores(autor,coautores,colectivo,'harvard')
        autorIEEE = formar_cita_autores(autor,coautores,colectivo,'ieee')
        autorAPA = formar_cita_autores(autor,coautores,colectivo,'apa')

        dataHarvard = {"autor":autorHarvard, "titulo": titulo, "fecha": pubyear, "tipo_material": tipo_material}

        dataAPA = {"autor":autorAPA, "titulo": titulo, "fecha": pubyear, "tipo_material": tipo_material}

        dataIEEE = {"autor":autorIEEE, "titulo": titulo, "fecha": pubyear, "tipo_material": tipo_material}

        citeHarvard = citeFuenteOtro(dataHarvard,"harvard")
        citeAPA     = citeFuenteOtro(dataAPA,"apa")
        citeIEEE    = citeFuenteOtro(dataIEEE,"ieee")

        print("Hardvard",citeHarvard)
        print("APA",citeAPA)
        print("IEEE",citeIEEE)

        # Se agrega la referencia.
        transcription = db(db.TRANSCRIPCION.id == id).select().first()

        # Se almacena la referencia en la base de datos.
        db.REFERENCIAS_OTROS_TRANSCRIPCIONES.insert(
                tipo = 'otro',
                transcripcion = transcription,
                titulo = titulo,
                autor = autor,
                coautores = coautores,
                colectivo = colectivo,
                pubyear = pubyear ,
                elemento_referencial = elemento_referencial,
                tipo_material = tipo_material,
                cite = citeHarvard,
                citeIEEE = citeAPA,
                citeAPA = citeIEEE,
                comentarios = comentario
        )

        current.session.flash = 'Referencia agregada correctamente.'
        redirect(URL(c='transcriptions',f='edit',vars={'id' : id, 'previous_page': previous_page}), client_side = True)


#-------------------------EDITAR OTRAS FUENTES-----------------------------
def form_editar_otras_fuentes():
    """formulario para editar otras fuentes"""
    reference_edit_otras_fuentes_form = SQLFORM.factory(
            Field('id_campo_otras_fuentes',   type='string'),
            Field('titulo', type="string", requires=IS_NOT_EMPTY(error_message='Por favor, introduzca un titulo')),
            Field('autor', type="string",required=True),
            Field('coautores', type="string"),
            Field('colectivo', type="string"),
            Field('tipo_material', type='string'),
            Field('elemento_referencial',  type='string'),
            Field('pubyear',  type='string'),
            Field('comentario_otra_fuente', type='text'),
            labels = {
                'titulo' : '* Título',
                'autor': 'Autor principal \n (Apellido, nombre)',
                'coautores': 'Coautor(es)',
                'colectivo': 'Autor institucional',
                'tipo_material': 'Tipo de Material',
                'elemento_referencial': 'Elemento referencial',
                'pubyear': 'Año de publicación',
                'comentario_otra_fuente': 'Comentario'
            },
            submit_button=T('Guardar')
     )
    return reference_edit_otras_fuentes_form

def procesar_form_editar_otras_fuentes(id,db,reference_edit_otras_fuentes_form,previous_page):
    # procesamiento para editar otras fuentes
    if reference_edit_otras_fuentes_form.process(formname = "reference_edit_otras_fuentes_form", onvalidation=autores_form_check).accepted:
        id_campo      = reference_edit_otras_fuentes_form.vars.id_campo_otras_fuentes
        autor         = reference_edit_otras_fuentes_form.vars.autor
        coautores  = reference_edit_otras_fuentes_form.vars.coautores
        colectivo  = reference_edit_otras_fuentes_form.vars.colectivo
        titulo        = reference_edit_otras_fuentes_form.vars.titulo
        pubyear = reference_edit_otras_fuentes_form.vars.pubyear
        comentario     = reference_edit_otras_fuentes_form.vars.comentario_otra_fuente
        tipo_material = reference_edit_otras_fuentes_form.vars.tipo_material
        elemento_referencial = reference_edit_otras_fuentes_form.vars.elemento_referencial

        data = {"autor":autor}

        autorHarvard = formar_cita_autores(autor,coautores,colectivo,'harvard')
        autorIEEE = formar_cita_autores(autor,coautores,colectivo,'ieee')
        autorAPA = formar_cita_autores(autor,coautores,colectivo,'apa')

        dataHarvard = {"autor":autorHarvard, "titulo": titulo, "fecha": pubyear, "tipo_material": tipo_material}

        dataAPA = {"autor":autorAPA, "titulo": titulo, "fecha": pubyear, "tipo_material": tipo_material}

        dataIEEE = {"autor":autorIEEE, "titulo": titulo, "fecha": pubyear, "tipo_material": tipo_material}

        citeHarvard = citeFuenteOtro(dataHarvard,"harvard")
        citeAPA     = citeFuenteOtro(dataAPA,"apa")
        citeIEEE    = citeFuenteOtro(dataIEEE,"ieee")

        print("Genere una nueva cita:")
        print(citeHarvard)

        # Se modifica la cita.
        db.REFERENCIAS_OTROS_TRANSCRIPCIONES[id_campo] = dict(cite      = citeHarvard)
        db.REFERENCIAS_OTROS_TRANSCRIPCIONES[id_campo] = dict(citeIEEE  = citeIEEE)
        db.REFERENCIAS_OTROS_TRANSCRIPCIONES[id_campo] = dict(citeAPA   = citeAPA)
        db.REFERENCIAS_OTROS_TRANSCRIPCIONES[id_campo] = dict(autor     = autor)
        db.REFERENCIAS_OTROS_TRANSCRIPCIONES[id_campo] = dict(coautores     = coautores)
        db.REFERENCIAS_OTROS_TRANSCRIPCIONES[id_campo] = dict(colectivo     = colectivo)
        db.REFERENCIAS_OTROS_TRANSCRIPCIONES[id_campo] = dict(titulo    = titulo)
        db.REFERENCIAS_OTROS_TRANSCRIPCIONES[id_campo] = dict(pubyear = pubyear)
        db.REFERENCIAS_OTROS_TRANSCRIPCIONES[id_campo] = dict(tipo_material = tipo_material)
        db.REFERENCIAS_OTROS_TRANSCRIPCIONES[id_campo] = dict(elemento_referencial = elemento_referencial)
        db.REFERENCIAS_OTROS_TRANSCRIPCIONES[id_campo] = dict(comentarios = comentario)

        print("En la base de datos guarde: ")
        rows = db(db.REFERENCIAS_OTROS_TRANSCRIPCIONES.id != None).select()
        for row in rows:
            print row

        current.session.flash = 'Referencia actualizada.'
        redirect(URL(c='transcriptions',f='edit',vars={'id' : id,'previous_page':previous_page}), client_side = True)

    elif reference_edit_otras_fuentes_form.errors:
        for e in reference_edit_otras_fuentes_form.errors:
            print e
        current.response.flash = 'No se pudo actualizar la referencia.'

#-------------------------AÑADIR COMENTARIO A REFERENCIA-----------------------------
def form_anadir_comentario_a_referencia():
    """formulario para comentar las referencias"""
    new_field_comentario_referencia = SQLFORM.factory(
            Field('id_campo', type='string'),
            Field('comentario', type="text"),
            labels = {
                'comentario' : 'Comentario'
            },
            submit_button=T('Agregar Comentario')
            )
    return new_field_comentario_referencia

def procesar_form_anadir_comentario_a_referencia(new_field_comentario_referencia,id,previous_page):
    """Procesamiento para agregar comentarios a las referencias"""
    if new_field_comentario_referencia.process(formname = "new_field_comentario_referencia").accepted:
      id_campo = new_field_comentario_referencia.vars.id_campo
      comentarios = new_field_comentario_referencia.vars.comentario

      db.LIBROS_TRANSCRIPCIONES[id_campo] = dict(comentarios = comentarios)
      current.session.flash = 'Comentario guardado.'
      redirect(URL(c='transcriptions',f='edit',vars={'id' : id, 'previous_page' : previous_page}), client_side = True)

    elif new_field_comentario_referencia.errors:
        response.flash = 'No se pudo agregar un nuevo comentario en esta referencia.'


#-------------------------AÑADIR CAMPO ADICIONAL-----------------------------
def form_anadir_campo_adicional(db):
    """formulario para el nuevo campo adicional"""
    new_field_form = SQLFORM.factory(
            Field('nombre', type="string",
                   requires = IS_EMPTY_OR(
                                IS_IN_DB(db, db.NOMBRES_CAMPOS_ADICIONALES_TRANSCRIPCION.nombre, zero='Seleccione...'))),
            Field('otro_nombre', type='string'),
            labels = {
                'nombre' : 'Campos Adicionales',
                'otro_nombre' : 'Otro Campo'
            },
            submit_button=T('Agregar Campo')
            )
    return new_field_form

def check_valid_aditional_field_name(name,db):

    """
      Función para chequear que el campo adicional no contenga los mismos
      nombres que los campos obligatorios.

    """
    campos_definidos= [
          "Codigo", "Código",
          "Denominacion", "Denominación",
          "Fecha Elaboracion", "Fecha Elaboración",
          "Periodo", "Año",
          "Periodo De Vigencia",
          "Horas De Teoria", "Horas De Teoría",
          "Horas De Practica", "Horas De Práctica",
          "Horas De Laboratorio",
          "Creditos", "Créditos",
          "Objetivos Generales",
          "Objetivos Específicos", "Objetivos Especificos",
          "Contenidos Sinópticos", "Contenidos Sinopticos",
          "Requisitos",
          "Estrategias Metodológicas", "Estrategias Metodologicas",
          "Estrategias de Evaluación", "Estrategias de Evaluacion",
          "Fuentes de Información Recomendadas", "Fuentes de Informacion Recomendadas",
          "Observaciones",
          "Campos Adicionales"]

    # revisa si no existe otro campo adicional definido con el mismo nombre
    nombre = db(db.NOMBRES_CAMPOS_ADICIONALES_TRANSCRIPCION.nombre == name).select()

    if nombre:
        return False

    return not(name in campos_definidos)

def procesar_form_anadir_campo_adicional(id,db,new_field_form,transcription,previous_page):
    """procesamiento del formulario para una nuevo campo adicional"""
    if new_field_form.process(formname = "new_field_form").accepted:
        nombre_campo = ''

        if new_field_form.vars.nombre:
            nombre_campo = new_field_form.vars.nombre.capitalize()

            campo_existe = campos_adicionales = db((db.CAMPOS_ADICIONALES_TRANSCRIPCION.transcripcion == transcription) &
                                                   (db.CAMPOS_ADICIONALES_TRANSCRIPCION.nombre == nombre_campo)).select()
            if campo_existe:
                current.session.flash = 'Campo %s ya fue agregado previamente.'%(nombre_campo)
            else:
                db.CAMPOS_ADICIONALES_TRANSCRIPCION.insert(
                        transcripcion = transcription,
                        nombre = nombre_campo
                    )
                current.session.flash = 'Nuevo campo %s agregado.'%(nombre_campo)
        elif new_field_form.vars.otro_nombre:
            nombre_campo = new_field_form.vars.otro_nombre.capitalize()

            # se verifica si es un campo adicional no antes definido
            if check_valid_aditional_field_name(nombre_campo,db):
                # en caso afirmativo, lo registramos y lo asociamos a la transcripcion
                field_id = db.NOMBRES_CAMPOS_ADICIONALES_TRANSCRIPCION.insert(nombre=nombre_campo)
                db.CAMPOS_ADICIONALES_TRANSCRIPCION.insert(
                        transcripcion = transcription,
                        nombre = field_id['nombre']
                    )
                current.session.flash = 'Nuevo campo %s agregado.'%(nombre_campo)
            else:
                current.session.flash = 'Campo %s ya existe.'%(nombre_campo)


        redirect(URL(c='transcriptions',f='edit',vars={'id' : id,'previous_page' : previous_page}), client_side = True)

    elif new_field_form.errors:
        response.flash = 'No se pudo agregar un nuevo campo.'


#-------------------------EDITAR CAMPO ADICIONAL-----------------------------
def form_editar_campo_adicional():
    """formulario para editar campos adicionales"""
    edit_field_form = SQLFORM.factory(
            Field('id_campo', type='string'),
            Field('contenido', type='text'),
            labels = {
                'contenido' : 'Contenido'},
            submit_button=T('Guardar')
            )
    return edit_field_form

def procesar_form_editar_campo_adicional(id,db,edit_field_form,previous_page):
    # procesamiento para editar campos adicionales
    if edit_field_form.process(formname = "edit_field_form").accepted:
        id_campo = edit_field_form.vars.id_campo
        db.CAMPOS_ADICIONALES_TRANSCRIPCION[id_campo] = dict (contenido = edit_field_form.vars.contenido)
        current.session.flash = 'Campo adicional actualizado.'
        redirect(URL(c='transcriptions',f='edit',vars={'id' : id, 'previous_page' : previous_page}), client_side = True)
    elif edit_field_form.errors:
        response.flash = 'No se pudo agregar un nuevo campo.'

#-------------------------EDITAR REFERENCIA VIDEO-----------------------------
def form_editar_ref_video():
    """formulario para editar videos"""
    reference_edit_field_form = SQLFORM.factory(
            Field('id_campo_video', type='string'),
            Field('autor', type='string'),
            Field('coautores', type='string'),
            Field('institucion', type='string'),
            Field('titulo', type='string'),
            Field('pubyear', type = 'integer',  length = 4,
                  requires = [IS_INT_IN_RANGE(0, 1e100,
                                  error_message='El año debe ser un numero positivo \
                                      de la forma YYYY a partir de 1967.'),
                              IS_LENGTH(4,
                                  error_message ='El año debe ser de la forma YYYY.')
                              ]),
            Field('url', type='string'),
            Field('fechaConsulta', type='date', requires = IS_NOT_EMPTY(error_message='Debe escoger una fecha de consulta')),
            Field('description', type='text'),
            Field('comentario_video', type='text'),
            labels = {
                'autor'         : 'Autor principal \n (Apellido, nombre)',
                'coautores'     : 'Coautor(es)',
                'institucion'   : 'Institucion',
                'titulo'       : 'Título',
                'pubyear'       : 'Año',
                'url'           : 'Enlace',
                'fechaConsulta' : 'Fecha de consulta',
                'description'   : 'Descripción',
                'comentario_video'    : 'Comentario del video'},
            submit_button=T('Guardar')
            )
    return reference_edit_field_form

def process_form_editar_ref_video(id,db,reference_edit_field_form,previous_page):
    # procesamiento para editar referencias (Videos)
    if reference_edit_field_form.process(formname = "reference_edit_field_form", onvalidation=autores_form_check).accepted:
        id_campo      = reference_edit_field_form.vars.id_campo_video
        autor         = reference_edit_field_form.vars.autor
        coautores     = reference_edit_field_form.vars.coautores
        institucion   = reference_edit_field_form.vars.institucion
        titulo        = reference_edit_field_form.vars.titulo
        anio          = reference_edit_field_form.vars.pubyear
        url           = reference_edit_field_form.vars.url
        fechaConsulta = reference_edit_field_form.vars.fechaConsulta
        descripcion   = reference_edit_field_form.vars.description
        comentario    = reference_edit_field_form.vars.comentario_video

        autorHarvard = formar_cita_autores(autor,coautores,institucion,'harvard')
        autorIEEE = formar_cita_autores(autor,coautores,institucion,'ieee')
        autorAPA = formar_cita_autores(autor,coautores,institucion,'apa')

        data1 = {'autor':autorHarvard,'fecha':anio,'titulo':titulo,
                'url': url, 'fechaConsulta': fechaConsulta}
        data2 = {'autor':autorIEEE,'fecha':anio,'titulo':titulo,
                'url': url, 'fechaConsulta': fechaConsulta}
        data3 = {'autor':autorAPA,'fecha':anio,'titulo':titulo,
                'url': url, 'fechaConsulta': fechaConsulta}

        # Se arma de nuevo la cita de la referencia.
        cita = citeVideo(data1,"harvard")
        citaIEEE = citeVideo(data2,"ieee")
        citaAPA  = citeVideo(data3,"apa")

        # Se modifica la referencia.
        db.VIDEOS_TRANSCRIPCIONES[id_campo] = dict(cite          = cita)
        db.VIDEOS_TRANSCRIPCIONES[id_campo] = dict(citeIEEE      = citaIEEE)
        db.VIDEOS_TRANSCRIPCIONES[id_campo] = dict(citeAPA       = citaAPA)
        db.VIDEOS_TRANSCRIPCIONES[id_campo] = dict(autor         = autor)
        db.VIDEOS_TRANSCRIPCIONES[id_campo] = dict(institucion   = institucion)
        db.VIDEOS_TRANSCRIPCIONES[id_campo] = dict(coautores     = coautores)
        db.VIDEOS_TRANSCRIPCIONES[id_campo] = dict(titulo        = titulo)
        db.VIDEOS_TRANSCRIPCIONES[id_campo] = dict(pubyear       = anio)
        db.VIDEOS_TRANSCRIPCIONES[id_campo] = dict(url           = url)
        db.VIDEOS_TRANSCRIPCIONES[id_campo] = dict(fecha_ingreso = fechaConsulta)
        db.VIDEOS_TRANSCRIPCIONES[id_campo] = dict(comentarios = comentario)
        db.VIDEOS_TRANSCRIPCIONES[id_campo] = dict(description = descripcion)

        current.session.flash = 'Referencia actualizada.'
        redirect(URL(c='transcriptions',f='edit',vars={'id' : id, 'previous_page' : previous_page}), client_side = True)
    elif reference_edit_field_form.errors:
        response.flash = 'No se pudo actualizar la referencia.'


#-------------------------EDITAR REFERENCIA LIBRO-----------------------------
def form_editar_ref_libros():
    """formulario para editar libros"""
    reference_edit_book_form = SQLFORM.factory(
            Field('id_libro',  type='string'),
            Field('autor',     type='string'),
            Field('titulo',    type='string'),
            Field('coautores', type="string"),
            Field('colectivo', type="string"),
            Field('isbn',      type='string'),
            Field('fecha',     type='integer'),
            Field('publisher', type='string'),
            Field('book_comment',type='text'),
            Field('edition_comment',type='string'),
            Field('open_access', type='boolean'),
            Field('book_url',type='string'),
            labels = {
                'autor'     : 'Autor principal (Apellidos, nombres)',
                'titulo'    : 'Título',
                'coautores' : 'Coautores',
                'fecha'     : 'Año',
                'isbn'      : 'ISBN',
                'publisher' : 'Editorial',
                'colectivo' : 'Autor institucional (Nombre o abreviación de la Institución)',
                'book_comment': 'Comentario del libro',
                'edition_comment': 'Comentario de la edición',
                'open_access': 'Acceso abierto',
                'book_url': 'Dirección web del libro'},
            submit_button=T('Guardar')
            )
    return reference_edit_book_form

def procesar_form_editar_ref_libros(id,db,reference_edit_book_form,previous_page):
    """procesamiento para editar referencias (Libros)"""
    if reference_edit_book_form.process(formname = "reference_edit_book_form").accepted:
        id_libro      = reference_edit_book_form.vars.id_libro
        autor         = reference_edit_book_form.vars.autor
        titulo        = reference_edit_book_form.vars.titulo
        anio          = reference_edit_book_form.vars.fecha
        isbn          = reference_edit_book_form.vars.isbn
        publisher     = reference_edit_book_form.vars.publisher
        book_comment  = reference_edit_book_form.vars.book_comment
        edition_comment = reference_edit_book_form.vars.edition_comment
        coautores = reference_edit_book_form.vars.coautores
        colectivo = reference_edit_book_form.vars.colectivo
        open_access = reference_edit_book_form.vars.open_access
        book_url = reference_edit_book_form.vars.book_url



        data = {'titulo':titulo,'publisher':publisher,
                'autor': autor ,'year':anio}

        # Se arma de nuevo la cita de la referencia.
        cita = cite(data,'book','book-minimal','harvard1')
        referenceCiteIEEE   = cite(data,'book','book-minimal','ieee')
        referenceCiteAPA    = cite(data,'book','book-minimal','apa')

        # Se modifica la cita.
        db.LIBROS_TRANSCRIPCIONES[id_libro] = {
            'citeIEEE': referenceCiteIEEE,
            'citeAPA': referenceCiteAPA,
            'cite': cita,
            'autor': autor,
            'titulo': titulo,
            'pubyear': anio,
            'isbn': isbn,
            'publisher': publisher,
            'comentarios': book_comment,
            'edition_comment': edition_comment,
            'colectivo': colectivo,
            'coautores': coautores,
            'open_access': open_access,
            'book_url': book_url
        }

        current.session.flash = 'Referencia actualizada.'
        redirect(URL(c='transcriptions',f='edit',vars={'id' : id, 'previous_page' : previous_page}), client_side = True)
    elif reference_edit_book_form.errors:
        response.flash = 'No se pudo actualizar la referencia.'


#-------------------------EDITAR REFERENCIA ARTICULO MINIMO-----------------------------
def form_editar_ref_articulo_minimo():
    """formulario para editar artículo (en forma minima)"""
    reference_edit_article_form = SQLFORM.factory(
            Field('id_campo_articulo_min',   type='string'),
            Field('autor',      type='string'),
            Field('titulo',     type='string'),
            Field('journal',    type='string'),
            Field('fecha',      type='string'),
            Field('comentario_articulo_minimo', type='text'),
            labels = {
                'autor'      : 'Autor principal \n (Apellido, nombre)',
                'titulo'     : 'Título',
                'journal'    : 'Publicación Periódica',
                'fecha'      : 'Año',
                'comentario_articulo_minimo' : 'Comentario del articulo'},
            submit_button=T('Guardar')
            )
    return reference_edit_article_form

def procesar_form_editar_ref_articulo_minimo(id,db,reference_edit_article_form,previous_page):
    # procesamiento para editar referencias (Articulos mínimos)
    if reference_edit_article_form.process(formname = "reference_edit_article_form").accepted:
        id_campo      = reference_edit_article_form.vars.id_campo_articulo_min
        autor         = reference_edit_article_form.vars.autor
        titulo        = reference_edit_article_form.vars.titulo
        journal       = reference_edit_article_form.vars.journal
        anio          = reference_edit_article_form.vars.fecha
        comentario    = reference_edit_article_form.vars.comentario_articulo_minimo


        data = {'title':titulo,'autor': autor ,'year':anio, 'journal': journal}
        # Se toman los datos de la referencia seleccionada y se
        # envian a la funcion encargada de escribir la cita de
        # dicha referencia.
        citeArticle = cite(data,"article","article-minimal",'harvard1')
        referenceCiteIEEE   = cite(data,'article','article-minimal','ieee')
        referenceCiteAPA    = cite(data,'article','article-minimal','apa')

        # Se modifica la cita.
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(cite      = citeArticle)
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(citeIEEE  = referenceCiteIEEE)
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(citeAPA   = referenceCiteAPA)
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(autor     = autor)
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(titulo    = titulo)
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(pubyear   = anio)
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(comentarios   = comentario)

        current.session.flash = 'Referencia actualizada.'
        redirect(URL(c='transcriptions',f='edit',vars={'id' : id,'previous_page' : previous_page}), client_side = True)

    elif reference_edit_article_form.errors:
        response.flash = 'No se pudo actualizar la referencia.'

#----------------------------EDITAR FUENTE WEB (CONTENEDOR)-----------------------------
def form_editar_fuente_web_1():
    """formulario para agregar fuentes web (MOOC, blogs, websites)"""
    reference_edit_fuente_web_1_form = SQLFORM.factory(
            Field('id_campo_fuente_uno',   type='string'),
            Field('titulo',      type='string', requires = IS_NOT_EMPTY(error_message='Debe introducir un título.')),
            Field('autor',       type='string'),
            Field('coautores',   type='string'),
            Field('institucion', type='string'),
            Field('plataforma',  type='string'),
            Field('enlace',      type='string', requires = IS_NOT_EMPTY(error_message='Debe introducir un enlace.')),
            Field('fecha_publicacion', type = 'integer',  length = 4,
                  requires = [IS_INT_IN_RANGE(0, 1e100,
                                  error_message='El año debe ser un numero positivo \
                                      de la forma YYYY.'),
                              IS_LENGTH(4,
                                  error_message ='El año debe ser de la forma YYYY.')
                              ]),
            Field('fecha_consulta',type='date', requires = IS_NOT_EMPTY(error_message='Debe escoger una fecha de consulta')),
            Field('comentario_fuentes_web_uno', type='text'),
            labels = {
                'titulo'           : 'Título',
                'autor'            : 'Autor principal \n (Apellido, nombre)',
                'coautores'        : 'Coautor(es)',
                'institucion'      : 'Institución',
                'plataforma'       : 'Plataforma',
                'enlace'           : 'Enlace',
                'fecha_publicacion' : 'Año',
                'fecha_consulta'   : 'Fecha de consulta',
                'comentario_fuentes_web_uno' : 'Comentario fuente web'},
            submit_button=T('Guardar')
    )
    return reference_edit_fuente_web_1_form

def procesar_form_editar_fuente_web_1(id,db,reference_edit_fuente_web_1_form,previous_page):
    """procesamiento para editar referencias web tipo contenedor"""
    if reference_edit_fuente_web_1_form.process(formname = "reference_edit_fuente_web_1_form", onvalidation=autores_form_check).accepted:
        id_campo      = reference_edit_fuente_web_1_form.vars.id_campo_fuente_uno
        autor         = reference_edit_fuente_web_1_form.vars.autor
        coautores     = reference_edit_fuente_web_1_form.vars.coautores
        titulo        = reference_edit_fuente_web_1_form.vars.titulo
        institucion   = reference_edit_fuente_web_1_form.vars.institucion
        plataforma    = reference_edit_fuente_web_1_form.vars.plataforma
        enlace        = reference_edit_fuente_web_1_form.vars.enlace
        fecha_publicacion = reference_edit_fuente_web_1_form.vars.fecha_publicacion
        fecha_consulta = reference_edit_fuente_web_1_form.vars.fecha_consulta
        comentarios    = reference_edit_fuente_web_1_form.vars.comentario_fuentes_web_uno


        data = {"autor":autor}

        autorHarvard = formar_cita_autores(autor,coautores,institucion,'harvard')
        autorIEEE = formar_cita_autores(autor,coautores,institucion,'ieee')
        autorAPA = formar_cita_autores(autor,coautores,institucion,'apa')

        dataHarvard = {"autor":autorHarvard, "titulo": titulo, "fecha": fecha_publicacion,
                "url": enlace, "fechaConsulta": fecha_consulta}

        dataAPA = {"autor":autorAPA, "titulo": titulo, "fecha": fecha_publicacion,
                "url": enlace, "fechaConsulta": fecha_consulta}

        dataIEEE = {"autor":autorIEEE, "titulo": titulo, "fecha": fecha_publicacion,
                "url": enlace, "fechaConsulta": fecha_consulta}

        citeHarvard = citeFuentesWeb(dataHarvard,"harvard")
        citeAPA     = citeFuentesWeb(dataAPA,"apa")
        citeIEEE    = citeFuentesWeb(dataIEEE,"ieee")

        print("id",id_campo)

        # Se modifica la cita.
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(cite      = citeHarvard)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(citeIEEE  = citeIEEE)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(citeAPA   = citeAPA)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(autor     = autor)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(coautores = coautores)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(titulo    = titulo)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(pubyear   = fecha_publicacion)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(url       = enlace)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(fecha_ingreso = fecha_consulta)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(institucion   = institucion)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(plataforma    = plataforma)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(comentarios    = comentarios)

        current.session.flash = 'Referencia actualizada.'
        redirect(URL(c='transcriptions',f='edit',vars={'id' : id, 'previous_page' : previous_page}), client_side = True)

    elif reference_edit_fuente_web_1_form.errors:
        current.response.flash = 'No se pudo actualizar la referencia.'

#-------------------------EDITAR FUENTE WEB (ITEM)-----------------------------
def form_editar_fuente_web_2():
    """formulario para agregar fuentes web, identico al formulario anterior pero con un campo adicional"""
    reference_edit_fuente_web_2_form = SQLFORM.factory(
            Field('id_campo_fuente_dos',   type='string'),
            Field('titulo', type='string', requires = IS_NOT_EMPTY(error_message='Debe introducir un título.')),
            Field('autor',  type='string'),
            Field('coautores',   type='string'),
            Field('institucion', type='string'),
            Field('titulo_contenedor', type='string'),
            Field('plataforma', type='string'),
            Field('enlace', type='string', requires = IS_NOT_EMPTY(error_message='Debe introducir un enlace.')),
            Field('fecha_publicacion', type = 'integer',  length = 4,
                  requires = [IS_INT_IN_RANGE(0, 1e100,
                                  error_message='El año debe ser un numero positivo \
                                      de la forma YYYY.'),
                              IS_LENGTH(4,
                                  error_message ='El año debe ser de la forma YYYY.')
                              ]),
            Field('fecha_consulta',type='date', requires = IS_NOT_EMPTY(error_message='Debe escoger una fecha de consulta')),
            Field('comentario_fuentes_web_dos', type='text'),
            labels = {
                'titulo'            : 'Título',
                'autor'             : 'Autor principal \n (Apellido, nombre)',
                'coautores'         : 'Coautor(es)',
                'institucion'       : 'Institución',
                'titulo_contenedor' : 'Título del contenedor',
                'plataforma'        : 'Plataforma',
                'enlace'            : 'Enlace',
                'fecha_publicacion' : 'Año',
                'fecha_consulta'    : 'Fecha de consulta',
                'comentario_fuentes_web_dos' : 'Comentario fuente web'},
            submit_button=T('Guardar')
     )
    return reference_edit_fuente_web_2_form

def procesar_form_editar_fuente_web_2(id,db,reference_edit_fuente_web_2_form, previous_page):
    # procesamiento para editar fuente web tipo item
    if reference_edit_fuente_web_2_form.process(formname = "reference_edit_fuente_web_2_form").accepted:
        id_campo      = reference_edit_fuente_web_2_form.vars.id_campo_fuente_dos
        autor         = reference_edit_fuente_web_2_form.vars.autor
        coautores     = reference_edit_fuente_web_2_form.vars.coautores
        titulo        = reference_edit_fuente_web_2_form.vars.titulo
        institucion   = reference_edit_fuente_web_2_form.vars.institucion
        plataforma    = reference_edit_fuente_web_2_form.vars.plataforma
        enlace        = reference_edit_fuente_web_2_form.vars.enlace
        fecha_publicacion = reference_edit_fuente_web_2_form.vars.fecha_publicacion
        fecha_consulta = reference_edit_fuente_web_2_form.vars.fecha_consulta
        titulo_contenedor = reference_edit_fuente_web_2_form.vars.titulo_contenedor
        comentarios    = reference_edit_fuente_web_2_form.vars.comentario_fuentes_web_dos

        data = {"autor":autor}

        autorHarvard = formar_cita_autores(autor,coautores,institucion,'harvard')
        autorIEEE = formar_cita_autores(autor,coautores,institucion,'ieee')
        autorAPA = formar_cita_autores(autor,coautores,institucion,'apa')

        dataHarvard = {"autor":autorHarvard, "titulo": titulo, "fecha": fecha_publicacion,
                "url": enlace, "fechaConsulta": fecha_consulta}

        dataAPA = {"autor":autorAPA, "titulo": titulo, "fecha": fecha_publicacion,
                "url": enlace, "fechaConsulta": fecha_consulta}

        dataIEEE = {"autor":autorIEEE, "titulo": titulo, "fecha": fecha_publicacion,
                "url": enlace, "fechaConsulta": fecha_consulta}

        citeHarvard = citeFuentesWeb(dataHarvard,"harvard")
        citeAPA     = citeFuentesWeb(dataAPA,"apa")
        citeIEEE    = citeFuentesWeb(dataIEEE,"ieee")

        print("id",id_campo)

        # Se modifica la cita.
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(cite      = citeHarvard)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(citeIEEE  = citeIEEE)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(citeAPA   = citeAPA)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(autor     = autor)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(titulo    = titulo)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(pubyear   = fecha_publicacion)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(url       = enlace)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(fecha_ingreso = fecha_consulta)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(institucion   = institucion)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(plataforma    = plataforma)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(titulo_contenedor = titulo_contenedor)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(comentarios = comentarios)

        current.session.flash = 'Referencia actualizada.'
        redirect(URL(c='transcriptions',f='edit',vars={'id' : id , 'previous_page' : previous_page}), client_side = True)

    elif reference_edit_fuente_web_2_form.errors:
        current.response.flash = 'No se pudo actualizar la referencia.'

#-------------------------EDITAR OTRO TIPO DE FUENTE WEB-----------------------------
def form_editar_fuente_web_otros():
    """formulario para agregar fuentes web (otros)"""
    reference_edit_fuente_web_otros_form = SQLFORM.factory(
            Field('id_campo_fuentes_otro',   type='string'),
            Field('titulo',      type='string', requires = IS_NOT_EMPTY(error_message='Debe introducir un título.')),
            Field('autor',       type='string'),
            Field('coautores', type="string"),
            Field('institucion', type="string"),
            Field('enlace',      type='string', requires = IS_NOT_EMPTY(error_message='Debe introducir un enlace.')),
            Field('fecha_publicacion', type = 'integer',  length = 4,
                  requires = [IS_INT_IN_RANGE(0, 1e100,
                                  error_message='El año debe ser un numero positivo \
                                                 de la forma YYYY.'),
                              IS_LENGTH(4,
                                  error_message ='El año debe ser de la forma YYYY.')
                             ]),
            Field('fecha_consulta',type='date', requires = IS_NOT_EMPTY(error_message='Debe escoger una fecha de consulta')),
            Field('comentarios', type='text'),
            labels = {
                      'titulo'               : 'Título',
                      'autor'                : 'Autor principal \n (Apellido, nombre)',
                      'coautores'            : 'Coautor(es)',
                      'institucion'            : 'Autor Institucional',
                      'enlace'               : 'Enlace',
                      'fecha_publicacion'     : 'Año',
                      'fecha_consulta'       : 'Fecha de consulta',
                      'comentarios'          : 'Comentarios'},
            submit_button=T('Guardar')
     )
    return reference_edit_fuente_web_otros_form

def procesar_form_editar_fuente_web_otros(id,db,reference_edit_fuente_web_otros_form, previous_page):
    # procesamiento para añadir fuentes web de tipo otros
    if reference_edit_fuente_web_otros_form.process(formname = "reference_edit_fuente_web_otros_form").accepted:
        id_campo              = reference_edit_fuente_web_otros_form.vars.id_campo_fuentes_otro
        autor                 = reference_edit_fuente_web_otros_form.vars.autor
        titulo                = reference_edit_fuente_web_otros_form.vars.titulo
        coautores             = reference_edit_fuente_web_otros_form.vars.coautores
        institucion             = reference_edit_fuente_web_otros_form.vars.institucion
        enlace                = reference_edit_fuente_web_otros_form.vars.enlace
        fecha_publicacion      = reference_edit_fuente_web_otros_form.vars.fecha_publicacion
        fecha_consulta        = reference_edit_fuente_web_otros_form.vars.fecha_consulta
        comentarios           = reference_edit_fuente_web_otros_form.vars.comentarios


        data = {"autor":autor}

        autorHarvard = formar_cita_autores(autor,coautores,institucion,'harvard')
        autorIEEE = formar_cita_autores(autor,coautores,institucion,'ieee')
        autorAPA = formar_cita_autores(autor,coautores,institucion,'apa')

        print("A.Hardvard", autorHarvard)
        print("A.IEEE",    autorIEEE)
        print("A.citeAPA", autorAPA)

        dataHarvard = {"autor":autorHarvard, "titulo": titulo, "fecha": fecha_publicacion,
                "url": enlace, "fechaConsulta": fecha_consulta}

        dataAPA = {"autor":autorAPA, "titulo": titulo, "fecha": fecha_publicacion,
                "url": enlace, "fechaConsulta": fecha_consulta}

        dataIEEE = {"autor":autorIEEE, "titulo": titulo, "fecha": fecha_publicacion,
                "url": enlace, "fechaConsulta": fecha_consulta}

        citeHarvard = citeFuentesWeb(dataHarvard,"harvard")
        citeAPA     = citeFuentesWeb(dataAPA,"apa")
        citeIEEE    = citeFuentesWeb(dataIEEE,"ieee")

        print("id",id_campo)

        # Se modifica la cita.
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(cite      = citeHarvard)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(citeIEEE  = citeIEEE)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(citeAPA   = citeAPA)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(autor     = autor)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(coautores = coautores)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(institucion = institucion)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(titulo    = titulo)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(url       = enlace)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(fecha_ingreso = fecha_consulta)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(pubyear       = fecha_publicacion)
        db.REFERENCIAS_WEB_TRANSCRIPCIONES[id_campo] = dict(comentarios   = comentarios)

        current.session.flash = 'Referencia actualizada.'
        redirect(URL(c='transcriptions',f='edit',vars={'id' : id, 'previous_page' : previous_page}), client_side = True)

    elif reference_edit_fuente_web_otros_form.errors:
        response.flash = 'No se pudo actualizar la referencia.'



#-------------------------EDITAR ARTICULOS COMPLETOS-----------------------------
def form_editar_articulo_completo():
    """formulario para modificar articulos (version completa)"""
    full_article_form = SQLFORM.factory(
            Field('id_campo_articulo_max',   type='string'),
            Field('autor',     type='string'),
            Field('titulo',    type='string'),
            Field('journal',    type='string'),
            Field('fecha', type = 'integer',  length = 4,
                  requires = [IS_INT_IN_RANGE(0, 1e100,
                                  error_message='El año debe ser un numero positivo \
                                      de la forma YYYY.'),
                              IS_LENGTH(4,
                                  error_message ='El año debe ser de la forma YYYY.')
                              ]),
            Field('volume', type='integer'),
            Field('pages' , type='string'),
            Field('number', type='integer'),
            Field('mes'   , type='string'),
            Field('comentario_articulo_completo', type='text'),
            labels = {
                'autor'     : 'Autor principal \n (Apellido, nombre)',
                'titulo'    : 'Título',
                'journal'   : 'Publicación Periódica',
                'fecha'     : 'Año',
                'volume'    : 'Volumen',
                'number'    : 'Número',
                'pages'     : 'Páginas',
                'mes'       : 'Mes de publicación',
                'comentario_articulo_completo' : 'Comentario del articulo' },
            submit_button=T('Guardar')
            )
    return full_article_form

def procesar_form_editar_articulo_completo(id,db,full_article_form,previous_page):
    # procesamiento para editar referencias (Articulos completos)
    if full_article_form.process(formname = "full_article_form").accepted:
        id_campo      = full_article_form.vars.id_campo_articulo_max

        autor = ""
        titulo = ""
        anio = ""
        journal = ""
        page = ""
        volume = ""
        number = ""
        month  = ""

        if (full_article_form.vars.autor):
          autor = full_article_form.vars.autor
        if (full_article_form.vars.titulo):
          titulo = full_article_form.vars.titulo
        if (full_article_form.vars.fecha):
          anio          = full_article_form.vars.fecha
        if (full_article_form.vars.journal):
          journal       = full_article_form.vars.journal
        if (full_article_form.vars.pages):
          page          = full_article_form.vars.pages
        if (full_article_form.vars.volume):
          volume        = full_article_form.vars.volume
        if (full_article_form.vars.number):
          number        = full_article_form.vars.number
        if (full_article_form.vars.month):
          month        = full_article_form.vars.mes

        comentario    = full_article_form.vars.comentario_articulo_completo

        print('Comentario: ',comentario)

        data = {'autor' : autor, 'title': titulo, 'year': anio, 'journal': journal,
                'volume': volume, 'pages': page, 'number': number, 'month': month}

        citeHarvard   = cite(data,"article","article-full",'harvard1')
        citeIEEE      = cite(data,'article',"article-full",'ieee')
        citeAPA       = cite(data,'article',"article-full",'apa')

        # Se modifica la cita.
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(cite      = citeHarvard)
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(citeIEEE  = citeIEEE)
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(citeAPA   = citeAPA)
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(autor     = autor)
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(titulo    = titulo)
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(pubyear   = anio)
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(journal   = journal)
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(paginas   = page)
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(mes       = month)
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(volume    = volume)
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(numero    = number)
        db.ARTICULOS_TRANSCRIPCIONES[id_campo] = dict(comentarios = comentario)

        current.session.flash = 'Referencia actualizada.'
        redirect(URL(c='transcriptions',f='edit',vars={'id' : id,'previous_page' : previous_page}), client_side = True)

    elif full_article_form.errors:
        response.flash = 'No se pudo actualizar la referencia.'



#-------------------------RECHAZAR TRANSCRIPCION-----------------------------
def form_rechazar_transcripcion():
    """Formulario para rechazar una transcripcion"""
    reject_transcription_form = SQLFORM.factory(
                                    Field('comentario', type='text', requires=IS_LENGTH(512, error_message='Comentario muy extenso, resuma a menos de 512 caracteres.')),
                                    labels = {
                                        'comentario' : 'Comentario'},
                                    submit_button=T('Rechazar'))
    return reject_transcription_form
