# -*- coding: utf-8 -*-
from gluon import current
import os


def chequear_vigencia_programas(programas, usuario_dpto_o_dace) :
    """Función que verifica si existen programas sin período o año de salida
    en vigencia.

    Args:
        * programas: Una lista de programas.
        * usuario_dpto_o_dace: booleano que dice si el usuario es dpto/dace o no.

    Returns:
        Devuelve un str que será insertado en un flash de la página para notifi
        carle al usuario la vigencia de los programas.

    """
    # Verificación de programas sin periodo_hasta o anio_hasta.
    programa_mas_reciente = None
    programas_sin_finalizacion = False
    
    for programa in programas:
        #Se registra el primer programa como el mas reciente para empezar el proceso
        if (programa_mas_reciente == None):
            programa_mas_reciente = programa
        else:
        #Se revisa si el programa que se esta insertando tiene periodo de finalización
            if ((programa.periodo_hasta is None) or (programa.anio_hasta is None)):
            #Si el programa no tiene periodo de finalización se revisa si es el programa mas reciente 
                if (programa_mas_reciente.periodo < programa.periodo) and (programa_mas_reciente.anio <= programa.anio):
                #Si el programa es el mas reciente, se revisa si el programa mas reciente previo tenia fecha de finalización
                    if ((programa_mas_reciente.periodo_hasta is None) or (programa_mas_reciente.anio_hasta is None)):
                    #Si el programa mas reciente previo no tenia fecha de finalización se notifica y se sugiere modificarlo                   

                        #<<<No Implementado>>>
                        programas_sin_finalizacion = True
                    
                    #Se actualizan las variables
                    programa_mas_reciente = programa
                    
                else:
                #Si el programa no es el mas reciente se reporta que el programa no tiene fecha de finalización 
                #y se sugiere modificarlo

                    #<<<No Implementado>>>
                    programas_sin_finalizacion = True



            #Si el programa tiene periodo de finalización, se continua la revisión de los otros programas

    if (programas_sin_finalizacion):
        if (usuario_dpto_o_dace):
            return "Estimado usuario, existen programas \n\
            sin fecha de salida de vigencia. \n\
            Consulte la tabla de programas disponibles."
        else:
            return ""
    return ""

def chequear_solapamiento_programas(programas, usuario_dpto_o_dace):
    """Función que verifica si en una lista de programas existen solapamientos.

    Para verificar si hay solapamientos entre una lista de programas se define
    un orden entre los períodos, luego se ordenan los programas de la lista
    primero en base al año y luego en base al período en orden decreciente.

    Seguidamente, se verifica que, el periodo y el año de fin de vigencia
    existan, luego, si el programa más reciente tiene fecha de entrada en \
    vigencia anterior a la fecha de salida de vigencia del programa más anterior.

    Args:
        programas: una lista de programas.
        usuario_dpto_o_dace: booleano que dice si el usuario es dpto/dace o no.

    Return:
        Retorna un mensaje para el flash y un diccionario donde las llaves
        son los ID de los programas y el valor es una lista de referencias
        a otros programas.

    """
    orden = {'SEP-DIC': 3,
             'ABR-JUL': 2,
             'ENE-MAR': 1,
             }

    # Ordenamiento por año y luego por período dependiendo del orden.
    key_func = (lambda prog: (prog.anio, orden[prog.periodo]))

    # ordenamiento decreciente de los programas.
    programas = sorted(programas, key=key_func, reverse=True)

    # Solapamiento existe para cuando dos programas en el mismo año
    # se tiene que la entrada en vigencia del más reciente sucede antes que
    # la salida en vigencia del programa anterior, en caso de que esta no
    # sea None.
    i = 0
    programas_solapando = []
    for i in range(len(programas)):
        for j in range(i+1,len(programas)):
            p_reciente, p_anterior = programas[i], programas[j]
            if (p_anterior.anio_hasta is not None and p_anterior.periodo_hasta is not None):
                if (p_reciente.anio < p_anterior.anio_hasta):
                    programas_solapando.append((p_reciente, p_anterior))

                elif (p_reciente.anio == p_anterior.anio_hasta):
                    if (orden[p_reciente.periodo] < orden[p_anterior.periodo_hasta]):
                        programas_solapando.append((p_reciente, p_anterior))

    # Finalmente se muestra una alerta a los usuarios.
    if (len(programas_solapando) > 0):
        if (usuario_dpto_o_dace):
            return "\n\n\nExisten programas cuyas vigencias solapan. \
            Por favor, verifique los periodos de vigencia y corríjalos."
        else:
            return ""
    else:
        return ""

    return ""
