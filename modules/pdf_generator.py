# -*- coding: utf-8 -*-
import cStringIO
import os
import re
from datetime import datetime
from reportlab.lib.enums import TA_JUSTIFY, TA_CENTER, TA_RIGHT
from reportlab.lib.pagesizes import letter
from reportlab.lib.units import cm
from reportlab.platypus import SimpleDocTemplate, Paragraph, Image, Table, TableStyle
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
import locale

try:
    locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')
except:
    print "es_ES locale setting not found"

def replaceBreak(text):
    return str(text).replace('\n','<br/>')

def setWaterMark(canvas, doc):
    canvas.saveState()
    canvas.setFont("Times-Roman", 32)
    canvas.setFillGray(0.85)
    canvas.drawCentredString(11*cm, 6*cm, "Documento sin validez")
    canvas.drawCentredString(11*cm, 13*cm, "Documento sin validez")
    canvas.drawCentredString(11*cm, 20*cm, "Documento sin validez")
    canvas.restoreState()

def generatePDF(buffer, request,
                cod,
                nombre,
                depto,
                division,
                anio_vig,
                periodo_vig,
                h_teoria,
                h_practica,
                h_laboratorio,
                creditos,
                sinopticos,
                fuentes,
                estrategias_met,
                estrategias_eval,
                obj_general,
                obj_especificos,
                observaciones,
                extras=None):

    # Medidas basadas en el documento de manual de estilo http://www.usb.ve/home/sites/default/files/identidad/manual%201_1.pdf
    
    title = "Programa analítico %s.pdf" % cod
    doc = SimpleDocTemplate(buffer, title=title, pagesize=letter,
                            rightMargin=3.8*cm,leftMargin=2.5*cm,
                            topMargin=1*cm, bottomMargin=1.4*cm)

    Story = []
    styles=getSampleStyleSheet()
    styles.add(ParagraphStyle(name='Top',
                              fontName="Times-Roman",
                              leading=16,
                              fontSize=10,
                              alignment=TA_CENTER))
    styles.add(ParagraphStyle(name='Right',
                              fontName="Times-Roman",
                              leading=16,
                              fontSize=10,
                              alignment=TA_RIGHT))
    styles.add(ParagraphStyle(name='Programa',
                              alignment=TA_JUSTIFY,
                              fontName="Times-Roman",
                              fontSize=10,
                              borderWidth=0.5,
                              borderColor='#000000',
                              borderPadding=(4, 6, 5, 6),
                              spaceAfter=18,
                              leading=14))

    # Header


    filepath = os.path.join(request.folder, 'static', 'images/home/usblogo.png')
    im = Image(filepath, width=5.6*cm, height=2.04*cm)
    text = "<b>Vicerrectorado Académico</b><br/>%s" % division
    text = Paragraph(text, styles["Top"])

    data = [
        [im ],  # Logo
        [text], # Subemisor principal
    ]
    t = Table(data, colWidths=[8*cm], hAlign='LEFT')
    t.setStyle(TableStyle([
        ('ALIGN',(0, 0),(-1,-1),'CENTER'),
        ('BOTTOMPADDING',(0, 1),(-1,-1), 0.47*cm + 0.2*cm + 0.9*cm),
        ('LEFTPADDING',(0, 0),(-1,-1), -0.2*cm),
        ('TOPPADDING',(0, 0),(-1,-1), 0),
    ]))
    Story.append(t)
    # TODO: Tabla para fecha
    text = "Sartenejas, %s" % datetime.now().strftime("%d de %B de %Y")
    text = Paragraph(text, styles["Right"])
    data = [
        [text],  # Fecha
        [''] # Margen Fecha
    ]
    t = Table(data, rowHeights=[0.3*cm, 1.15*cm], colWidths=[7.29*cm], hAlign='RIGHT')
    t.setStyle(TableStyle([
        ('ALIGN',(0, 0),(-1,-1),'CENTER'),
        ('RIGHTPADDING',(0, 0),(-1,-1), -0.2*cm),
    ]))
    Story.append(t)

    #Departamento
    text = "<b>Departamento: </b>" + depto
    Story.append(Paragraph(text, styles["Programa"]))

    #Nombre Asignatura
    text = "<b>Asignatura: </b>%s - %s" % (cod, nombre)
    Story.append(Paragraph(text, styles["Programa"]))

    # Unidades credito
    text = "<b>N° unidades de crédito:</b> %s<br/>" % creditos
    text +="<b>N° de horas semanales:</b>  Teoría %s, Práctica %s, Laboratorio %s" % (h_teoria,h_practica,h_laboratorio)
    Story.append(Paragraph(text, styles["Programa"]))

    #Entrada Vigencia
    if(periodo_vig and anio_vig):
        text = "<b>Fecha de entrada en vigencia: </b> %s %s" % (periodo_vig,anio_vig)
        Story.append(Paragraph(text, styles["Programa"]))

    #Objetivos
    if(obj_general):
        text = "<b>Objetivo General:</b><br/>" + replaceBreak(obj_general)
        Story.append(Paragraph(text, styles["Programa"]))

    if(obj_especificos):
        text = "<b>Objetivos Específicos:</b><br/>" + replaceBreak(obj_especificos)
        Story.append(Paragraph(text, styles["Programa"]))

    #Justificacion
    # Removed here as it is not mandatory anymore.
    # if(justificacion):
    #     text = "<b>Justificacion:</b><br/>" + replaceBreak(justificacion)
    #     Story.append(Paragraph(text, styles["Programa"]))

    #Contenidos sinopticos
    if(sinopticos):
        text = "<b>Contenidos sinópticos:</b><br/>" + replaceBreak(sinopticos)
        Story.append(Paragraph(text, styles["Programa"]))

    #Estrategias
    if(estrategias_met):
        text = "<b>Estrategias metodológicas:</b><br/>" + replaceBreak(estrategias_met)
        Story.append(Paragraph(text, styles["Programa"]))

    if(estrategias_eval):
        text = "<b>Estrategias de evaluación:</b><br/>" + replaceBreak(estrategias_eval)
        Story.append(Paragraph(text, styles["Programa"]))

    #Fuentes
    if(fuentes):
        text = "<b>Fuentes recomendadas de información:</b><br/>" + replaceBreak(fuentes)
        Story.append(Paragraph(text, styles["Programa"]))

    #Extras
    if extras:
        for key in extras:
            text = "<b>%s:</b><br/>%s" % (key,replaceBreak(extras[key]))
            Story.append(Paragraph(text, styles["Programa"]))

    #Observaciones
    if(observaciones):
        text = "<b>Observaciones:</b><br/>" + replaceBreak(observaciones)
        Story.append(Paragraph(text, styles["Programa"]))

    doc.build(Story, onFirstPage=setWaterMark, onLaterPages=setWaterMark)
