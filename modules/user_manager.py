# -*- coding: utf-8 -*-
from usbutils import *

def get_user_data():

    usbid = "teruel"
    info = get_ldap_data(usbid)
    print(info)

def _test_login():
    ''' Controller method for testing purposes only.

    It logs in the user with usbid '99-99999' PREVIOUSLY created within a fixture.

    '''
    usbid = "99-99999"
    tabla_usuario = db.auth_user
    datos_usuario = db(tabla_usuario.username == usbid).select()[0]
    clave = datos_usuario.access_key

    auth.login_bare(usbid, clave)

    redirect(URL(c='default',f='index'))


def registrar(usuario, auth, db):
    """
        Una vez el usuario inicia sesión, extrae la información asociada a su id,
        provista por el CAS, para llenar los campos de perfil del usuario.
    """

    nombre = usuario['first_name']
    apellido = usuario['last_name']
    tipo  = usuario['tipo']
    email = usuario['email']
    cedula  = usuario['cedula']
    phone = usuario['phone']
    usbid = usuario['email'].split('@')[0]
    clave = random_key()

    try:
    	auth_user_id = db.auth_user.insert(
                                 first_name = nombre,
                                 last_name  = apellido,
                                 username = usbid,
                                 email = email,
                                 access_key = clave,
                                 ci = cedula,
                                 phone = phone,
                                 password = db.auth_user.password.validate(clave)[0])

        # Agregando roles por defecto.
        if tipo == 'Pregrado':
            auth.add_membership(auth.id_group(role="ESTUDIANTE"), auth_user_id)
        if tipo == 'Docente':
            auth.add_membership(auth.id_group(role="PROFESOR"), auth_user_id)
        if tipo == 'Egresado':
            auth.add_membership(auth.id_group(role="EGRESADO"), auth_user_id)
        if tipo == 'Administrativo':
            auth.add_membership(auth.id_group(role="ADMINISTRATIVO"), auth_user_id)
        if tipo == 'Organización':
            auth.add_membership(auth.id_group(role="ORGANIZACION"), auth_user_id)
        if tipo == 'Empleado':
            auth.add_membership(auth.id_group(role="EMPLEADO"), auth_user_id)

    	return auth_user_id

    except Exception as e:
        pass

    return dict( message = usuario)
