# -*- coding: utf-8 -*-
from gluon import current
from gluon.dal import DAL, Field
from gluon.validators import *
from gluon.http import *
from gluon.html import *
import os
import sys
import io
import re
import pyocr
import pyocr.builders
import StringIO
from gluon.sqlhtml import SQLFORM
from gluon.languages import *
# Librerias para realizar las citas
from citeproc.py2compat import *
from citeproc import CitationStylesStyle, CitationStylesBibliography
from citeproc import formatter
from citeproc import Citation, CitationItem
from citeproc.source.bibtex import BibTeX
from pybtex.database import BibliographyData, Entry

#-------------------------------CREACION DE CITAS------------------------#

def cite(data,typeOfDocument,documentId,citeStyle):

  """
    Función encargada de transformar el libro seleccionado por el usuario a una
    cita de formato que indique la variable citeStyle.
  """

  def fix_year(yearVar):
    yearVar = yearVar.replace(".","").replace("c","").replace("[","").replace("]","")

    return yearVar

  def construir_bibliographyData(datos,typeOfDocument,documentId):

    listaDeParametros = []

    if datos['autor']:
      listaDeParametros.append(('author', datos['autor'].decode('utf-8')))
    else:
      listaDeParametros.append(('author', "Anonimo"))


    if (typeOfDocument == "book"):

      listaDeParametros.append(('title',datos['titulo'].decode('utf-8') ) )
      listaDeParametros.append(('publisher', datos['publisher'].decode('utf-8')))
      listaDeParametros.append(('year', fix_year(datos['year'].decode('utf-8'))))

    elif (typeOfDocument == "article"):
      listaDeParametros.append(('title', datos['title'].decode('utf-8')))
      listaDeParametros.append(('year', fix_year(datos['year'].decode('utf-8'))))

      if (datos['journal']):
        listaDeParametros.append(('journal', datos['journal'].decode('utf-8')))

    if (documentId == "article-full"):
      listaDeParametros.append(('volume', datos['volume'].decode('utf-8')))
      if not(datos['pages'] == ''):
        listaDeParametros.append(('pages', datos['pages'].decode('utf-8')))
      if (datos['number']):
        listaDeParametros.append(('number', datos['number'].decode('utf-8')))

      if (datos['month']):
        listaDeParametros.append(('month', datos['month'].decode('utf-8')))

    return BibliographyData({documentId:Entry(typeOfDocument,listaDeParametros)})

  bib_data = construir_bibliographyData(data,typeOfDocument,documentId)

  # Se busca la ruta del archivo test.bib
  current_path = os.getcwd()
  path = current_path + "/applications/SIGPAE/static/test.bib"

  with open(path,"w",encoding='utf-8') as text_file:
      text_file.write(bib_data.to_string('bibtex'))

  bib_source = BibTeX(path, encoding="utf-8")

  bib_style = CitationStylesStyle(os.path.join(current.request.folder, 'static', citeStyle), validate=False)

  bibliography = CitationStylesBibliography(bib_style, bib_source,
                                            formatter.plain)
  citation1 = Citation([CitationItem(documentId)])

  bibliography.register(citation1)

  cita = ''
  for item in bibliography.bibliography():
      cita+=str(item)

  return cita

def formar_cita_autores(autor,coautor,institucion,tipo):
    """funcion que se encarga de colocar en el formato correcto los autores de una referencia
       dependiendo de si son autores principales, coautores, o instituciones, de forma que se puedan
       pasar de una vez a las funciones que arman la cita completa como un solo texto"""
    if (autor is "") and (coautor is ""):
        #print ("PRIMER CASO: NI AUTOR NI COAUTOR")
        cita = ""
    elif (autor is "") and (not(coautor is "")):
        #print ("SEGUNDO CASO: SOLO COAUTOR")
        data = {"autor":coautor}
        if tipo == 'harvard':
            cita   = cite(data,"misc","online",'harvard1')
        elif tipo == 'ieee':
            cita = cite(data,'misc',"online",'ieee')
        elif tipo == 'apa':
            cita = cite(data,'misc',"online",'apa')
    elif (not(autor is "")) and (coautor is ""):
        #print ("TERCER CASO: SOLO AUTOR")
        data = {"autor":autor}
        if tipo == 'harvard':
            cita   = cite(data,"misc","online",'harvard1')
        elif tipo == 'ieee':
            cita = cite(data,'misc',"online",'ieee')
        elif tipo == 'apa':
            cita = cite(data,'misc',"online",'apa')
    elif (not(autor is "")) and (not(coautor is "")):
        #print ("CUARTO CASO: AMBOS")
        data1 = {"autor":autor}
        data2 = {"autor":coautor}
        if tipo == 'harvard':
            cita_autor  = cite(data1,"misc","online",'harvard1')
            cita_coautor = cite(data2,"misc","online",'harvard1')
        elif tipo == 'ieee':
            cita_autor = cite(data1,'misc',"online",'ieee')
            cita_coautor = cite(data2,'misc',"online",'ieee')
        elif tipo == 'apa':
            cita_autor = cite(data1,'misc',"online",'apa')
            cita_coautor = cite(data2,'misc',"online",'apa')
        cita = cita_autor+", "+cita_coautor
    if institucion is "":
        #print cita
        return cita
    else:
        if cita == "":
            #print institucion
            return institucion
        else:
            #print cita+", "+institucion
            return cita+", "+institucion

def citeVideo(data, citeStyle):

  """
    Función realizar citas en formato APA, IEEE y Harvard de vídeos.
  """

  if (citeStyle == "harvard"):
    # Tipo de cita para videos en el estilo Hardvard.
    cita = "{autor}. {anio}. {titulo}.[Online] [Accedido {fechaConsulta}]. Disponible en: {url}".format(autor=data['autor'],
            anio = data['fecha'].split('-')[0],
            titulo = data['titulo'],
            fechaConsulta = data['fechaConsulta'],
            url = data['url'])

  elif (citeStyle == "apa"):

    # Tipo de cita para videos en el estilo APA.
    cita = "{autor}.({anio}). {titulo}. Disponible en: {url}".format(autor=data['autor'],
            anio = data['fecha'].split('-')[0],
            titulo = data['titulo'],
            url = data['url'])

  elif (citeStyle == "ieee"):

    # Tipo de cita para videos en el estilo IEEE.
    cita = "{autor}. ({anio}, {fechaConsulta}). {titulo}.[Vídeo Online]. Disponible en: {url}".format(autor=data['autor'],
            anio = data['fecha'].split('-')[0],
            fechaConsulta = data['fechaConsulta'],
            titulo = data['titulo'],
            url = data['url'])
  else:

     cita = ""

  return cita


def citeFuentesWeb(data, citeStyle):

    """
      Función realizar citas en formato APA, IEEE y Harvard de fuentes web.
    """

    if (citeStyle == "harvard" and data["fecha"]):
      # Tipo de cita para videos en el estilo Hardvard.
      cita = "{autor} ({anio}). {titulo}. [Online]. Disponible en: {url}. [Accedido {fechaConsulta}]. ".format(autor=data['autor'],
              anio = data['fecha'].split('-')[0],
              titulo = data['titulo'],
              fechaConsulta = data['fechaConsulta'],
              url = data['url'])

    elif (citeStyle == "harvard"):
      # Tipo de cita para videos en el estilo Hardvard.
      cita = "{autor}. {titulo}. [Online]. Disponible en: {url}. [Accedido {fechaConsulta}]. ".format(autor=data['autor'],
              titulo = data['titulo'],
              fechaConsulta = data['fechaConsulta'],
              url = data['url'])

    elif (citeStyle == "apa" and data["fecha"]):

      # Tipo de cita para videos en el estilo APA.
      cita = "{autor} ({anio}). {titulo}. Disponible en: {url}".format(autor=data['autor'],
              anio = data['fecha'].split('-')[0],
              titulo = data['titulo'],
              url = data['url'])

    elif (citeStyle == "apa"):

      # Tipo de cita para videos en el estilo APA.
      cita = "{autor}. {titulo}. Disponible en: {url}".format(autor=data['autor'],
              titulo = data['titulo'],
              url = data['url'])

    elif (citeStyle == "ieee" and data["fecha"]):

      # Tipo de cita para videos en el estilo IEEE.
      cita = "{autor} {titulo}. {anio}. [Online]. Disponible en: {url}. [Accedido {fechaConsulta}] ".format(autor=data['autor'],
              anio = data['fecha'].split('-')[0],
              fechaConsulta = data['fechaConsulta'],
              titulo = data['titulo'],
              url = data['url'])

    elif (citeStyle == "ieee"):

      # Tipo de cita para videos en el estilo IEEE.
      cita = "{autor} {titulo}. [Online]. Disponible en: {url}. [Accedido {fechaConsulta}] ".format(autor=data['autor'],
              fechaConsulta = data['fechaConsulta'],
              titulo = data['titulo'],
              url = data['url'])

    else:

       cita = ""

    return cita

def citeFuenteOtro(data, citeStyle):

    """
      Función realizar citas en formato APA, IEEE y Harvard de otro tipo de fuente.
    """

    if (citeStyle == "harvard" and data["fecha"]):
      # Tipo de cita para otros en el estilo Hardvard.
      cita = "{autor} ({anio}). {titulo}. {tipo_material}.".format(autor=data['autor'],
              anio = data['fecha'].split('-')[0],
              titulo = data['titulo'],
              tipo_material = data['tipo_material'])

    elif (citeStyle == "harvard"):
      # Tipo de cita para otros en el estilo Hardvard.
      cita = "{autor}. {titulo}. {tipo_material}. ".format(autor=data['autor'],
              titulo = data['titulo'],
              tipo_material = data['tipo_material'])

    elif (citeStyle == "apa" and data["fecha"]):
      # Tipo de cita para otros en el estilo APA.
      cita = "{autor} ({anio}). {titulo}. {tipo_material}.".format(autor=data['autor'],
              anio = data['fecha'].split('-')[0],
              titulo = data['titulo'],
              tipo_material = data['tipo_material'])

    elif (citeStyle == "apa"):

      # Tipo de cita para otros en el estilo APA.
      cita = "{autor}. {titulo}. {tipo_material}.".format(autor=data['autor'],
              titulo = data['titulo'],
              tipo_material = data['tipo_material'])

    elif (citeStyle == "ieee" and data["fecha"]):

      # Tipo de cita para otros en el estilo IEEE.
      cita = "{autor} {titulo}. {anio}. {tipo_material}.".format(autor=data['autor'],
              anio = data['fecha'].split('-')[0],
              titulo = data['titulo'],
              tipo_material = data['tipo_material'])

    elif (citeStyle == "ieee"):

      # Tipo de cita para videos en el estilo IEEE.
      cita = "{autor} {titulo}. {tipo_material}.".format(autor=data['autor'],
              titulo = data['titulo'],
              tipo_material = data['tipo_material'])

    else:

       cita = ""

    return cita
