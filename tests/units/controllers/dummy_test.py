#!/usr/bin/env python

'''py.test test cases to test people application.
These tests run simulating web2py shell environment and don't use webclient
module.
So, they run faster and don't need web2py server running.
If you want to see webclient approach, see test_people_controller_webclient.py
in this same directory.
'''


def test_index_exists(client):
    '''Page index exists?
    '''

    # run the controller, without view
    #result = web2py.run('people', 'index', web2py)

    # now, render the view with context received from controller
    #html = web2py.response.render('people/index.html', result)
    #assert "Hi. I'm the people index" in html
    assert 1 == 1
