# -*- coding: utf-8 -*-

import urllib2
import json
import pytest
import collections
# Al migrar a Python 3, la libreria mock viene incluida en unittest.mock
from mock import Mock, patch

def test_departments(web2py, login_user, mocker):
    """Prueba unitaria para probar el controller department_queries_list utilizando mocks.

        Específicamente, prueba que la lista de departamentos que se devuelve
        es la misma que se obtiene de SIGPAE_WS.

        Fixtures:
            * web2py -> Conexión con el servidor de web2py.
            * mocker -> Fixture de la libreria mock (pytest-mock)
    """
    # No importa realmente la respuesta a efectos de la prueba
    mock_response = []

    # Mock list_departments para simular request al webservice
    mock_list_departments = mocker.patch('webservice_queries.list_departments')
    mock_list_departments.return_value = mock_response

    # Ejecutar controlador
    result = web2py.run('queries', 'departments', web2py)

    # Comprobar que el controlador debe llamar al metodo list_departments
    assert(mock_list_departments.called)
    # Verificar que los departamentos que devuelve el controlador coinciden con
    # los devueltos por list_departments
    assert(result['departments'] == mock_response)

    # Finish the test
    web2py.db.commit()

def test_subjects(web2py, login_user, add_update_profile_auth_user_permission, mocker):
    # No importa realmente la respuesta a efectos de la prueba
    mock_response = []

    # Mock list_subjects para simular request al webservice
    mock_list_subjects = mocker.patch('webservice_queries.list_subjects')
    mock_list_subjects.return_value = mock_response

    # Ejecutar controlador
    result = web2py.run('queries', 'subjects', web2py)

    # Comprobar que el controlador debe llamar al metodo list_subjects
    assert(mock_list_subjects.called)
    # Verificar que las asignaturas que devuelve el controlador coinciden con
    # los devueltos por list_subjects
    assert(result['subjects'] == mock_response)

    # Finish the test
    web2py.db.commit()

def test_careers(web2py, login_user, add_update_profile_auth_user_permission, mocker):
    # No importa realmente la respuesta a efectos de la prueba
    mock_response = []

    # Mock list_careers para simular request al webservice
    mock_list_careers = mocker.patch('webservice_queries.list_careers')
    mock_list_careers.return_value = mock_response

    # Ejecutar controlador
    result = web2py.run('queries', 'careers', web2py)

    # Comprobar que el controlador debe llamar al metodo list_careers
    assert(mock_list_careers.called)
    # Verificar que las carreras que devuelve el controlador coinciden con
    # los devueltos por list_careers
    assert(result['careers'] == mock_response)

    # Finish the test
    web2py.db.commit()
