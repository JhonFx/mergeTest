# -*- coding: utf-8 -*-

'''
web2py_default_test.py

py.test test cases to test the default.py controller.

These tests run simulating web2py shell environment and don't use webclient
module.
So, they run faster and don't need web2py server running.
If you want to see webclient approach, see client_*.py
in this same directory.

Al incluir web2py como argumento, se le dice a py.test que debe incluir la función
Web2Py que está anotada como @pytest.fixture() en el archivo tests/conftest.py

Ref: https://docs.pytest.org/en/latest/fixture.html
'''
import pytest

def test_search_results_unregistered_user_program_code(web2py, load_test_programs):
    '''Tests if the search_results controller works fine if the user is not
       registered in SIGPAE and only with the program code.

        First we load some programs into the Test to be read with the fixture.
    '''

    # Setting the parameters.
    # Run the test only with the program code, e.g. CI2511.
    load_test_programs['departamento'] = None
    load_test_programs['denominacion'] = ""

    # Set the GET parameters
    web2py.set_get_parameters(env=web2py, data=load_test_programs)

    # La firma de la función web2py es:
    # * web2py.run(controller, function, env):
    result = web2py.run('default', 'search_results', web2py)

    # Verificar que efectivamente se retorna una lista de programas con el código
    # del programa en BD.
    subjects = result['subjects']
    assert(len(subjects) >= 0)
    for sub in subjects:
        assert(sub['codigo'] == load_test_programs['codigo'])

    # Finish the test
    web2py.db.commit()

def test_search_results_unregistered_user_department(web2py, load_test_programs):
    '''Tests if the search_results controller works fine if the user is not
       registered in SIGPAE and only with the department

        First we load some programs into the Test to be read with the fixture.
    '''

    # Setting the parameters.
    # Run the test only with the department name.
    load_test_programs['codigo'] = ""
    load_test_programs['denominacion'] = ""

    # Set the GET parameters
    web2py.set_get_parameters(env=web2py, data=load_test_programs)

    # La firma de la función web2py es:
    # * web2py.run(controller, function, env):
    result = web2py.run('default', 'search_results', web2py)

    # Verificar que efectivamente se retorna una lista de programas con el código
    # del programa en BD.
    subjects = result['subjects']
    assert(len(subjects) >= 0)
    for sub in subjects:
        # Check if the received course is from the test department.
        # We only obtain the letters, e.g. if the department is (FS), then we
        # want FS.
        assert(sub['codigo'].startswith(load_test_programs['departamento'][-3:-1]))

    # Finish the test
    web2py.db.commit()

def test_search_results_unregistered_user_program_name(web2py, load_test_programs):
    '''Tests if the search_results controller works fine if the user is not
       registered in SIGPAE and only with the program name (denomination).

        First we load some programs into the Test to be read with the fixture.
    '''

    # Setting the parameters.
    # Run the test only with the department name.
    load_test_programs['codigo'] = ""
    load_test_programs['departamento'] = None

    # Set the GET parameters
    web2py.set_get_parameters(env=web2py, data=load_test_programs)

    # La firma de la función web2py es:
    # * web2py.run(controller, function, env):
    result = web2py.run('default', 'search_results', web2py)

    # Verificar que efectivamente se retorna una lista de programas con el código
    # del programa en BD.
    subjects = result['subjects']
    assert(len(subjects) >= 0)
    for sub in subjects:
        assert(sub['denominacion'] == load_test_programs["denominacion"])

    # Finish the test
    web2py.db.commit()

def test_search_results_registered_user_program_code(web2py, login_user, add_update_profile_auth_user_permission, load_test_programs):
    '''Tests if the search_results controller works fine if the user is
       registered, logged in in SIGPAE, with a system role, with basic permission
        and only with the program code.
    '''
    assert(login_user['username'] == "99-99999")
    assert(add_update_profile_auth_user_permission == "OK")
    # Setting the parameters.
    # Run the test only with the program code, e.g. CI2511.
    load_test_programs['departamento'] = None
    load_test_programs['denominacion'] = ""

    # Set the GET parameters
    web2py.set_get_parameters(env=web2py, data=load_test_programs)

    # La firma de la función web2py es:
    # * web2py.run(controller, function, env):
    result = web2py.run('default', 'search_results', web2py)

    # Verificar que efectivamente se retorna una lista de programas con el código
    # del programa en BD.
    subjects = result['subjects']
    assert(len(subjects) >= 0)
    for sub in subjects:
        assert(sub['codigo'] == load_test_programs['codigo'])

    # Finish the test
    web2py.db.commit()

def test_search_results_registered_user_department(web2py, login_user, add_update_profile_auth_user_permission, load_test_programs):
    '''Tests if the search_results controller works fine if the user is
       registered, logged in in SIGPAE, with a system role, with basic permission
        and only with the department.
    '''
    assert(login_user['username'] == "99-99999")
    assert(add_update_profile_auth_user_permission == "OK")
    # Setting the parameters.
    # Run the test only with the department name.
    load_test_programs['codigo'] = ""
    load_test_programs['denominacion'] = ""

    # Set the GET parameters
    web2py.set_get_parameters(env=web2py, data=load_test_programs)

    # La firma de la función web2py es:
    # * web2py.run(controller, function, env):
    result = web2py.run('default', 'search_results', web2py)

    # Verificar que efectivamente se retorna una lista de programas con el código
    # del programa en BD.
    subjects = result['subjects']
    assert(len(subjects) >= 0)
    for sub in subjects:
        # Check if the received course is from the test department.
        # We only obtain the letters, e.g. if the department is (FS), then we
        # want FS.
        assert(sub['codigo'].startswith(load_test_programs['departamento'][-3:-1]))

    # Finish the test
    web2py.db.commit()

def test_search_results_registered_user_program_name(web2py, login_user, add_update_profile_auth_user_permission, load_test_programs):
    '''Tests if the search_results controller works fine if the user is
       registered, logged in in SIGPAE, with a system role, with basic permission
        and only with the program name (denomination).
    '''
    assert(login_user['username'] == "99-99999")
    assert(add_update_profile_auth_user_permission == "OK")

    # Setting the parameters.
    # Run the test only with the department name.
    load_test_programs['codigo'] = ""
    load_test_programs['departamento'] = None

    # Set the GET parameters
    web2py.set_get_parameters(env=web2py, data=load_test_programs)

    # La firma de la función web2py es:
    # * web2py.run(controller, function, env):
    result = web2py.run('default', 'search_results', web2py)

    # Verificar que efectivamente se retorna una lista de programas con el código
    # del programa en BD.
    subjects = result['subjects']
    assert(len(subjects) >= 0)
    for sub in subjects:
        assert(sub['denominacion'] == load_test_programs["denominacion"])

    # Finish the test
    web2py.db.commit()
