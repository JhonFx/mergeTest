# -*- coding: utf-8 -*-

'''
web2py_users_test.py

py.test test cases to test the users.py controller.

These tests run simulating web2py shell environment and don't use webclient
module.
So, they run faster and don't need web2py server running.
If you want to see webclient approach, see client_*.py
in this same directory.

Al incluir web2py como argumento, se le dice a py.test que debe incluir la función
Web2Py que está anotada como @pytest.fixture() en el archivo tests/conftest.py

Ref: https://docs.pytest.org/en/latest/fixture.html
'''

import pytest


def test_get_roles_empty(web2py):
    '''Tests if the get_roles controller works correctly when there are no roles.

        We should remember that the DB is empty if there is no setup at the
        beginning.
    '''
    # run the controller, without view\
    # Este controlador no popula ninguna vista.
    # La firma de la función web2py es:
    # * web2py.run(controller, function, env):
    result = web2py.run('users', 'get_roles', web2py)

    # Verificar que el endpoint retornó una lista vacía y que efectivamente en
    # la BD no hay nada.
    assert(len(result) == 0)
    assert(web2py.db(web2py.db.auth_group).count() == 0)

    # Finish the test
    web2py.db.commit()


def test_profile(web2py, login_user, add_update_profile_auth_user_permission):
    """ Prueba el controlador user ejecutando la función profile.

        Esta prueba necesita:
            * Un usuario que se haya registrado (fixture create_user)
            * Un usuario que haya iniciado sesión (fixture login_user, depende de create_user)
            * Que tenga permisos: 'update_profile' y 'auth_user'
            * No esté incluido en el grupo de inactivos (tenga cualquier rol menos "INACTIVO")
    """
    # Verificar que sea el mismo usuario creado para la prueba y que tenga
    # los permisos suficientes.
    assert(login_user['username'] == "99-99999")
    assert(add_update_profile_auth_user_permission=="OK")
    # Precondiciones
    # @auth.requires(
    #   auth.is_logged_in() and
    #   auth.has_permission('manage_users', 'auth_user')
    #   and not(auth.has_membership(auth.id_group(role="INACTIVO")))
    # )
    assert(web2py.auth.is_logged_in())
    assert(web2py.auth.has_permission('update_profile', 'auth_user'))
    assert(not web2py.auth.has_membership(web2py.auth.id_group(role="INACTIVO")))

    # Obtiene el diccionario que devuelve el controlador.
    result = web2py.run('users', 'profile', web2py)

    # Renderiza el HTML utilizando lo que se obtuvo en la corrida.
    html = web2py.response.render('users/profile.html', result)

    # TODO: finish the test.

    # Cosas que deben probarse cuando se renderiza la página de perfil de
    # un usuario.
    # * El campo de Cédula de identidad debe estar lleno y no vacío.
    # * El campo de nombre de usuario debe estar lleno y no vacío
    # * El campo de correo electrónico de usuario debe estar lleno y no vacío.

    # Finish the test
    web2py.db.commit()
