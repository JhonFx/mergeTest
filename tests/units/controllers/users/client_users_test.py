# -*- coding: utf-8 -*-

'''py.test test cases to test the users.py controller.

These tests run based on webclient and need web2py server running.
If you want to see a faster approach, see test_people_controller_web2pyenv.py
in this same directory.

Toda la información sobre el objeto WebClient que se crea está en:
>> web2py/gluon/contrib/webclient.py
'''

def test_get_roles_empty(client):
    '''Tests if the get_roles controller works correctly when there are no roles.

        We should remember that the DB is empty if there is no setup at the
        beginning.
    '''
    # run the controller, without view\
    # Este controlador no popula ninguna vista.
    client.get('/edit/get_roles') # get a page

    assert (client.status == 200)

