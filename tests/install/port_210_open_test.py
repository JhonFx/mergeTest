import urllib2

def test_port_210_open():
    "Tests if the server can connect to the library of congress API using remote port 210"
    test_url = "http://lx2.loc.gov:210/LCDB?version=1.2&operation=searchRetrieve&query=dc.title=Hola&maximumRecords=1"
    request = urllib2.urlopen(test_url)
    assert(request.getcode() == 200)
