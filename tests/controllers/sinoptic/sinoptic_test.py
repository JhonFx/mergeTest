# -*- coding: utf-8 -*-

#######################################################################################################################
# DESCRIPTION
#######################################################################################################################

"""
Tests for synoptic controller
"""

#######################################################################################################################
# DEPENDENCIES
#######################################################################################################################

import pytest

#######################################################################################################################
# FUNCTIONS
#######################################################################################################################

def displayed_menu(web2py, valid_login, menu_name):
    """
    Procedimiento para probar que solo el rol DECANATO puede observar el menu específicado
    """

    # Verificar login valido
    assert(valid_login["status"])

    # Renderizar vista
    result = web2py.run('default', 'index', web2py)
    html = web2py.response.render('default/index.html', result)

    # Verificar que solo miembros de DECANATO pueden ver el menu
    visible = menu_name in html
    isDecanato = valid_login['user']['role'] == 'DECANATO'
    assert(visible if isDecanato else not visible)

    # Finalizar prueba
    web2py.db.commit()

#######################################################################################################################
# VISUALIZATION TESTS OF THE MENU
#######################################################################################################################

def test_displayed_synoptic_programs(web2py, valid_login):
    """
    Prueba que solo usuarios con el rol DECANATO pueden ver el menu sinoptico
    """

    displayed_menu(web2py, valid_login, 'Sinoptico')

def test_displayed_specific_programs(web2py, valid_login):
    """
    Prueba que solo usuarios con el rol DECANATO pueden ver el menu sinoptico
    """

    displayed_menu(web2py, valid_login, 'name="specific-programs"')

def test_displayed_current_programs(web2py, valid_login):
    """
    Prueba que solo usuarios con el rol DECANATO pueden ver el menu sinoptico
    """

    displayed_menu(web2py, valid_login, 'name="current-programs"')

def test_displayed_all_programs(web2py, valid_login):
    """
    Prueba que solo usuarios con el rol DECANATO pueden ver el menu sinoptico
    """

    displayed_menu(web2py, valid_login, 'name="all-programs"')

#######################################################################################################################
# TEST FOR SPECIFIC MENU
#######################################################################################################################

def test_search(web2py, valid_login, load_test_programs):
    # Verificar login valido
    assert(valid_login["status"])

    # Hacer la búsqueda si DECANATO esta entre los roles del usuario
    if valid_login['user']['role'] == 'DECANATO':
        # Establecer los parametros de la solicitud
        web2py.set_get_parameters(web2py, load_test_programs)

        # Obtener resultados de buscar la asignatura
        result = web2py.run('sinoptic', 'search_results', web2py)

        # La asignatura debe estar en genesis
        assert(result['registrado_genesis'])

    # Finalizar prueba
    web2py.db.commit()
