# -*- coding: utf-8 -*-

import urllib2
import json
import pytest
from gluon.html import SPAN

# Probar que sale la pestania de transcriptor para transcriptores y transcriptores para dace blah blah blah http://localhost:8000/SIGPAE/
# Probar que http://localhost:8000/SIGPAE/transcriptions/add solo esta autorizada para transcriptores
# Probar el listar las transcripciones d eun usuario http://localhost:8000/SIGPAE/transcriptions/list
# Probar transcripciones en revision http://localhost:8000/SIGPAE/transcriptions/list_sent
# Probar agregar transcripciones http://localhost:8000/SIGPAE/transcriptions/add
# Probar que el codigo de la transcripcion corresponda al de un programa en bd
# Probar que peuden verse los transcriptores creados http://localhost:8000/SIGPAE/transcriptions/transcriptors

# NOTA IMPORTANTE!!!
# PRUEBAS COMENTADAS DEBIDO A QUE NO FUNCIONAN
# FALTA LA DECLARACION DEL FIXTURE: add_create_transcription_permission

'''
def test_transcriptor_puede_ver_enlace_nueva_transcripcion(
        web2py,login_user,
        add_update_profile_auth_user_permission,
        add_create_transcription_permission):
    """
    Prueba que un usuario con el ROL de TRANSCRIPTOR pueda ver en la barra de navegación "nueva transcripción"
    :param web2py:
    :param login_user:
    :param add_create_transcription_permission:
    :param add_update_profile_auth_user_permission:
    :return:
    """
    # Verificar que sea el mismo usuario creado para la prueba y que tenga
    # los permisos suficientes.
    #assert (add_create_transcription_permission == "OK")
    #assert (add_update_profile_auth_user_permission == "OK")
    print(1)
    assert (web2py.auth.is_logged_in())
    assert (not web2py.auth.has_membership(web2py.auth.id_group(role="INACTIVO")))
    assert (web2py.auth.has_permission('create_transcription'))
    #web2py.response.auth = web2py.auth
    print(2)
    result = web2py.run('transcriptions', 'add', web2py)
    #assert (result['message'] == 'Sistema de Gestión de Programas Analíticos de Estudio')
    print(3)
    html = web2py.response.render('transcriptions/add.html', result)
    print(4)
    print(html)
    #assert ('<a href="/SIGPAE/transcriptions/add">' in html) # TODO NOT f working
    web2py.db.commit()

def test_puede_ver_formulario_nueva_transcripcion(web2py, login_user, add_create_transcription_permission,
                                       add_update_profile_auth_user_permission):
    """
    Prueba que un usuario con el ROL de TRANSCRIPTOR pueda crear una transcripción
    :param web2py:
    :param login_user:
    :param add_create_transcription_permission:
    :param add_update_profile_auth_user_permission:
    :return:
    """
    # Verificar que sea el mismo usuario creado para la prueba y que tenga
    # los permisos suficientes.
    assert (add_create_transcription_permission == "OK")
    assert (add_update_profile_auth_user_permission == "OK")

    assert (web2py.auth.is_logged_in())
    assert (not web2py.auth.has_membership(web2py.auth.id_group(role="INACTIVO")))
    assert (web2py.auth.has_permission('create_transcription'))
    result = web2py.run('transcriptions', 'add', web2py)
    assert (result['message'] == 'Nueva Transcripción')
    assert str(result['form'].__class__) == "<class 'gluon.sqlhtml.SQLFORM'>"

    html = web2py.response.render('transcriptions/add.html', result)

    assert ('<h2> Nueva Transcripción</h2>' in html)
    assert '<form action="#" enctype="multipart/form-data" method="post">' in html
    assert 'Archivo PDF</label>' in html
    assert 'Tipo de Archivo</label>' in html
    assert 'Extraer</label>' in html
    web2py.db.commit()

def test_puede_enviar_transcripcion(web2py, login_user, add_create_transcription_permission,
                                       add_update_profile_auth_user_permission):
    """
    Prueba que un usuario con el ROL de TRANSCRIPTOR pueda crear una transcripción
    :param web2py:
    :param login_user:
    :param add_create_transcription_permission:
    :param add_update_profile_auth_user_permission:
    :return:
    """
    # Verificar que sea el mismo usuario creado para la prueba y que tenga
    # los permisos suficientes.
    assert (add_create_transcription_permission == "OK")
    assert (add_update_profile_auth_user_permission == "OK")

    assert (web2py.auth.is_logged_in())
    assert (not web2py.auth.has_membership(web2py.auth.id_group(role="INACTIVO")))
    assert (web2py.auth.has_permission('create_transcription'))
    result = web2py.run('transcriptions', 'add', web2py)
    # TODO : No f funciona
    result = web2py.submit('transcriptions', 'add', web2py,{'file':'','file_type':''},'no_table/create')
    print(result)
    web2py.db.commit()

def test_puede_enviar_transcripcion(web2py, login_user, add_create_transcription_permission,
                                       add_update_profile_auth_user_permission):
    """
    Prueba que un usuario con el ROL de TRANSCRIPTOR pueda crear una transcripción
    :param web2py:
    :param login_user:
    :param add_create_transcription_permission:
    :param add_update_profile_auth_user_permission:
    :return:
    """
    # Verificar que sea el mismo usuario creado para la prueba y que tenga
    # los permisos suficientes.
    assert (add_create_transcription_permission == "OK")
    assert (add_update_profile_auth_user_permission == "OK")

    assert (web2py.auth.is_logged_in())
    assert (not web2py.auth.has_membership(web2py.auth.id_group(role="INACTIVO")))
    assert (web2py.auth.has_permission('create_transcription'))
    result = web2py.run('transcriptions', 'add', web2py)
    # TODO : No f funciona
    result = web2py.submit('transcriptions', 'add', web2py,{'file':'','file_type':''},'no_table/create')
    print(result)
    web2py.db.commit()
#
#
# def test_new_transcription_code_exists(web2py, login_user, load_test_programs):
#     web2py.db.commit()

'''
