# -*- coding: utf-8 -*-

#######################################################################################################################
# DESCRIPTION
#######################################################################################################################

"""
Tests for books controller
"""

#######################################################################################################################
# DEPENDENCIES
#######################################################################################################################

import pytest

#######################################################################################################################
# FUNCTIONS
#######################################################################################################################

def test_displayed_books_programs(web2py, valid_login):
    """
    Prueba que solo usuarios con el rol BIBLIOTECA pueden ver el menu libros
    """

    displayed_menu(web2py, valid_login, 'Books')

def test_displayed_specific_programs(web2py, valid_login):
    """
    Prueba que solo usuarios con el rol BIBLIOTECA pueden ver el menu libros especificos
    """

    displayed_menu(web2py, valid_login, 'name="specific-books"')

def test_search(web2py, valid_login, load_test_code):
    # Verificar login valido
    assert(valid_login["status"])

    # Hacer la búsqueda si BIBLIOTECA esta entre los roles del usuario
    if valid_login['user']['role'] == 'BIBLIOTECA':
        # Establecer los parametros de la solicitud
        web2py.set_get_parameters(web2py, load_test_code)

        # Obtener resultados de buscar la asignatura
        result = web2py.run('books', 'search', web2py)

        # La asignatura debe estar en genesis
        assert(result['registrado_genesis'])

    # Finalizar prueba
    web2py.db.commit()


def test_search_list(web2py,valid_login,load_test_code):
    
    '''
        Prueba que dado un codigo registrado en genesis,encuentre un programa asociado a esa asignatura
    '''

     # Verificar login valido
    assert(valid_login["status"])

    # Hacer la búsqueda si BIBLIOTECA esta entre los roles del usuario
    if valid_login['user']['role'] == 'BIBLIOTECA':
        # Establecer los parametros de la solicitud
        web2py.set_get_parameters(web2py, load_test_code)

        # Obtener resultados de buscar la asignatura
        result = web2py.run('books', 'search_list', web2py)

        # La asignatura debe estar en genesis
        assert(result['registrado_genesis'])

    # Finalizar prueba
    web2py.db.commit()

def test_details(web2py,valid_login,load_test_code):

    '''
        Prueba que dado un id de un programa, busque sus detalles.
    '''

     # Verificar login valido
    assert(valid_login["status"])

    # Hacer la búsqueda si BIBLIOTECA esta entre los roles del usuario
    if valid_login['user']['role'] == 'BIBLIOTECA':
        # Establecer los parametros de la solicitud
        web2py.set_get_parameters(web2py, load_test_code)

        # Obtener resultados de buscar la asignatura
        result = web2py.run('books', 'details', web2py)

        # La asignatura debe estar en genesis
        assert len(result['books']) > 0
        assert len(result['subject']) > 0
        assert len(result['program']) > 0

    # Finalizar prueba
    web2py.db.commit()

def test_book_details(web2py,valid_login,load_test_code):

    '''
        Prueba que dado un id de un libro, muestre los detalles del mismo.
    '''

     # Verificar login valido
    assert(valid_login["status"])

    # Hacer la búsqueda si BIBLIOTECA esta entre los roles del usuario
    if valid_login['user']['role'] == 'BIBLIOTECA':
        # Establecer los parametros de la solicitud
        web2py.set_get_parameters(web2py, load_test_code)

        # Obtener resultados de buscar la asignatura
        result = web2py.run('books', 'book_details', web2py)

        # La asignatura debe estar en genesis
        assert len(result['books']) > 0

    # Finalizar prueba
    web2py.db.commit()




