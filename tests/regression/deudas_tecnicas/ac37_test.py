# -*- coding: utf-8 -*-

"""
ac37_test.py

Esta es un conjunto de pruebas de regresión que verifican que la deuda técnica
AC.37 del manual de usuario ha sido efectivamente solucionada y que no ha
vuelto a ocurrir.

Deuda técnica AC37: Como Transcriptor, puedo ver y editar las referencias a libros
agregadas en la vista de edición de transcripciones.

Criterios de aceptación:
1. El sistema me mostrará autor(es), título, isbn, edición(es), año(s), editorial.

2. Si el libro tiene más de un autor, el sistema me mostrará todos los autores.

3. Si el libro tiene un autor colectivo o institucional, el sistema me lo
mostrará advirtiéndome que se trata de un autor de este tipo (lo recomendable es
que haya un campo opcional denominado autor colectivo que se muestra sólo si el
sistema recupera un autor colectivo o si, como Transcriptor, yo lo solicito.

4. Si el programa analítico puede apoyarse en más de una edición del libro,
puedo indicar o registrar la primera edición autorizada como fuente y la más
reciente. El sistema también me ofrece, un campo de comentarios que me permite
registrar cualquier observación que requiera transcribir sobre las ediciones
tales como pueden ser: “La primera edición tiene errores serios en el capítulo
5. No se recomienda el uso de la segunda edición por problemas en su traducción.
Para el curso los capítulos a consultar de la tercera edición son 2,3,5,8, y 9.
 La cuarta edición sólo incluye cambios menores y un mayor uso de colores en los
 diagramas que no aportan gran cosa a su comprensión.”

5. El sistema me permitirá registrar un comentario sobre el libro. Por ejemplo
“Texto obligatorio”, “Recomendado como libro de consulta”, “Lectura adicional”,
“Los ejercicios de final de capítulo son particularmente recomendados”.

Autor: Fernando Pérez, 12-11152.

Última modificación: 04/03/2018.
"""

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0

def test_regression_criterios1y2(selenium, settings, web2py, login_user, appname, load_test_transcription, add_book_reference):
    '''Dada una transcripción con una referencia, verificar que se cumple la
    prueba de aceptación 1.

    '''
    # Fetch url.
    selenium.get(settings['host']['hostname_url'] + appname)

    # Iniciar sesión.
    # Esto no redigirá al CAS porque se está en ambiente de pruebas.
    selenium.find_element_by_link_text("Iniciar Sesión").click()

    # Dadas las siguientes condiciones
    trans_id = add_book_reference['transcription_id']
    ref_id = add_book_reference['reference_id']
    ref_autor = add_book_reference['autor']
    ref_coautores = add_book_reference['coautores']
    ref_titulo = add_book_reference['titulo']
    ref_isbn = add_book_reference['isbn']
    ref_anio = add_book_reference['pubyear']
    ref_editorial = add_book_reference['publisher']

    # Abrir la transcripción de prueba.
    selenium.get(
        settings['host']['hostname_url'] + \
        appname + \
        "/transcriptions/edit?id={transcription_id}".format(
            transcription_id=trans_id)
        )

    # We get the <a> element that contains the modal data, i.e., the icon that opens
    # the edit book modal.
    ref = selenium.find_element_by_css_selector(
        '[data-id="{ref_id}"][data-target="{target}"]'.format(
            ref_id=ref_id,
            target="#modalEditarLibro"
            )
        )

    # get the reference autor.
    autor = ref.get_attribute('data-autor')
    coautores = ref.get_attribute('data-coautores')
    titulo = ref.get_attribute('data-titulo')
    isbn = ref.get_attribute('data-isbn')
    anio = int(ref.get_attribute('data-anio'))
    editorial = ref.get_attribute('data-publisher')

    # Entonces
    assert(autor == ref_autor)
    assert(coautores == ('None' if ref_coautores is None else ref_coautores))
    assert(titulo == ref_titulo)
    assert(isbn == ref_isbn)
    assert(anio == ref_anio)
    assert(editorial == ref_editorial)

    # Finalizar prueba
    web2py.db.commit()
    selenium.quit()

def test_regression_view_book(client, settings, login_user, load_test_transcription, add_book_reference):
    # import urllib2, ssl
    # ssl._create_default_https_context = ssl._create_unverified_context
    # print(add_book_reference)
    # import pdb
    # pdb.set_trace()
    #
    # client.get(
    #     '/transcriptions/edit?id={transcription_id}'.format(
    #         transcription_id=load_test_transcription
    #         )
    #     )
    #
    assert(1==1)


#
# def test_regression_autor2(client, settings):
#     client.get('/users/get_roles') # get a page
#
#     assert client.status == 200
