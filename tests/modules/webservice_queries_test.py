# -*- coding: utf-8 -*-

import urllib2
import json
import pytest
# Al migrar a Python 3, la libreria mock viene incluida en unittest.mock
from mock import Mock, patch

ws_baseurl = "baseurl"

@patch('webservice_queries.get_json')
def mock_json_call(function, args, url, data, mock_get_json):
    # Los import del modulo deben hacerse dentro de la funcion para que pueda
    # resolverse la dependencia
    from webservice_queries import get_json

    data = None or data

    # Doesn't really matter which value we set
    mock_get_json.return_value = []

    # Verify that get_json is called with the appropriate endpoint
    result = function(*args)

    if data is not None:
        mock_get_json.assert_called_with(url, data)
    else:
        mock_get_json.assert_called_with(url)

    # Verify that it returns the result of the get_json call
    assert(result == get_json(url))

def test_get_json(web2py, mocker):
    from webservice_queries import get_json

    url = "url"
    data = None
    mock_response = "[]"

    mock_urlopen = mocker.patch('webservice_queries.urllib2.urlopen')
    mock_urlopen.return_value.read.return_value = mock_response

    # Verify that urlopen is called with the url provided, and get_json returns
    # the result of the call parsed as json
    assert(get_json(url) == json.loads(mock_response))
    mock_urlopen.assert_called_with(url, data=data)


def test_get_json_case_urlerror(web2py, mocker):
    from webservice_queries import get_json

    url = "invalid url"
    data = None

    mock_urlopen = mocker.patch('webservice_queries.urllib2.urlopen')
    mock_urlopen.side_effect = urllib2.URLError(reason='URL Error')

    # Verify that if URLError is raised, get_json returns None
    assert(get_json(url) is None)
    mock_urlopen.assert_called_with(url, data=data)

def test_list_departments():
    from webservice_queries import get_json, list_departments

    endpoint = "departamentos"
    url = "{}/{}".format(ws_baseurl, endpoint)
    mock_json_call(list_departments, [ws_baseurl], url, None)

def test_list_subjects():
    from webservice_queries import get_json, list_subjects

    endpoint = "asignaturas"
    url = "{}/{}".format(ws_baseurl, endpoint)
    mock_json_call(list_subjects, [ws_baseurl], url, None)

def test_list_careers():
    from webservice_queries import get_json, list_careers

    endpoint = "carreras"
    url = "{}/{}".format(ws_baseurl, endpoint)
    mock_json_call(list_careers, [ws_baseurl], url, None)

def test_list_department_subjects():
    from webservice_queries import get_json, list_department_subjects

    endpoint = "asignaturas"
    department_code = "codigo_dpto"
    url = "{}/{}?siglas_depto={}".format(ws_baseurl, endpoint, department_code)
    mock_json_call(list_department_subjects, [ws_baseurl, department_code], url, None)

def test_list_career_subjects():
    from webservice_queries import get_json, list_career_subjects

    endpoint = "asignaturas"
    career_code = "codigo_carrera"
    url = "{}/{}?cod_carrera={}".format(ws_baseurl, endpoint, career_code)
    mock_json_call(list_career_subjects, [ws_baseurl, career_code], url, None)
