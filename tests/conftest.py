#!/usr/bin/env python
# -*- coding: utf-8 -*-

#######################################################################################################################
# DESCRIPTION
#######################################################################################################################

"""
PyTest configuration and fixtures file.

* Tells application she's running in a test environment.
* Creates a complete web2py environment, similar to web2py shell.
* Creates a WebClient instance to browse your application, similar to a real web browser.
* Propagates some application data to test cases via fixtures, like baseurl and automatic appname discovering.
"""

#######################################################################################################################
# DEPENDENCIES
#######################################################################################################################

from json import load
from datetime import date
from pytest import fixture
from selenium import webdriver
from os import getcwd, pardir
from os.path import join, split, sep
from gluon.compileapp import run_controller_in
from gluon.contrib.webclient import WebClient
from gluon.shell import env
from gluon.storage import Storage
from ..modules.usbutils import random_key
from ..modules.web2pytest import web2pytest

#######################################################################################################################
# FIXTURES
#######################################################################################################################

@fixture(scope='session')
def settings():
    curr_dir = split(__file__)[0]
    config_dir = join(curr_dir, pardir)
    config_file = join(config_dir, 'private/appconfig.json')
    with open(config_file, 'r') as config:
        settings = load(config, encoding="utf-8")
    return settings

@fixture(scope='session')
def baseurl(appname, settings):
    """
    The base url to call your application.

    The settings variable is a dict that holds the data of the config file.
    Its purpose is to have the "same" interface as the settings variable defined
    in our web2py project.
    """

    return settings['host']['hostname_url'] + appname

@fixture(scope='session')
def appname():
    """
    Discover application name.

    Your test scripts must be on applications/<your_app>/tests
    """

    dirs = split(__file__)[0]
    appname = dirs.split(sep)[-2]
    return appname

@fixture(scope='session', autouse=True)
def create_testfile_to_application(request, appname):
    """
    Creates a temp file to tell application she's running under a
    test environment.

    Usually you will want to create your database in memory to speed up
    your tests and not change your development database.

    This fixture is automatically run by py.test at session level. So, there's
    no overhad to test performance.
    """

    web2pytest.create_testfile(appname)
    request.addfinalizer(web2pytest.delete_testfile)

@fixture(autouse=True)
def cleanup_db(web2py):
    """
    Truncate all database tables before every single test case.

    This can really slow down your tests. So, keep your test data small and try
    to allocate your database in memory.

    Automatically called by test.py due to decorator.
    """

    web2py.db.rollback()
    for tab in web2py.db.tables:
        web2py.db[tab].truncate('CASCADE')
    web2py.db.commit()

@fixture(scope='session')
def client(baseurl):
    """
    Create a new WebClient instance once per session.
    """

    webclient = WebClient(baseurl)
    return webclient

@fixture()
def create_user(request, web2py):
    """
    Fixture that creates a new user to test.
    """
    username = "99-99999"
    clave = random_key()
    auth_user_id = web2py.db.auth_user.insert(
        first_name="Pedro",
        last_name="Perez",
        username=username,
        email="test@mail.com",
        access_key=clave,
        ci="1234568",
        phone="04145555555",
        password=web2py.db.auth_user.password.validate(clave)[0]
    )

    # Commit these changes into the DB to avoid deadlocks and idle jobs
    web2py.db.commit()

    return {'id': auth_user_id, 'username': username, 'password': clave}

@fixture(params=["DECANATO", "ESTUDIANTE", "PROFESOR", "EGRESADO", "ADMINISTRATIVO", "ORGANIZACION", "EMPLEADO"])
def create_role_user(web2py, request, create_user):
    web2py.db.auth_group.insert(role=request.param, description="")
    web2py.auth.add_membership(web2py.auth.id_group(role=request.param), create_user['id'])

    # Commit these changes into the DB to avoid deadlocks and idle jobs
    web2py.db.commit()

    # Add role tu user dict
    create_user['role'] = request.param

    return create_user

@fixture()
def login_user(web2py, create_role_user):
    """
    Fixture that, given a created user, logs it in.

    Hace login al usuario utilizando la misma funcion que /default/login_cas
    """
    web2py.auth.login_bare(create_role_user['username'], create_role_user['password'])

    # Commit these changes into the DB to avoid deadlocks and idle jobs
    web2py.db.commit()
    return create_role_user

@fixture()
def valid_login(web2py, login_user):
    """
    Filtrar login de usuarios inactivos
    """

    # Inicio de sesión
    logged = web2py.auth.is_logged_in()

    # Usuario activo
    active = not web2py.auth.has_membership(web2py.auth.id_group(role="INACTIVO"))

    result = {
        "status": logged and active,
        "user": login_user
    }

    return result

@fixture(params=["ESTUDIANTE", "PROFESOR", "EGRESADO", "ADMINISTRATIVO", "ORGANIZACION", "EMPLEADO"])
def add_update_profile_auth_user_permission(request, web2py, create_user):
    web2py.db.auth_group.insert(role=request.param, description="")
    web2py.auth.add_permission(web2py.auth.id_group(role=request.param), 'update_profile', 'auth_user')
    web2py.auth.add_permission(web2py.auth.id_group(role=request.param), 'manage_users', 'auth_user')
    web2py.auth.add_membership(web2py.auth.id_group(role=request.param), create_user['id'])

    # Commit these changes into the DB to avoid deadlocks and idle jobs
    web2py.db.commit()
    return "OK"

@fixture(params=["TRANSCRIPTOR"])
def add_transcriptor_role(request, web2py, create_user):
    web2py.db.auth_group.insert(role=request.param,
                                description="Transcriptor en SIGPAE")
    web2py.auth.add_permission(web2py.auth.id_group(role=request.param),
                               'create_transcription')
    web2py.auth.add_membership(web2py.auth.id_group(role=request.param),
                               create_user['id'])

    # Commit these changes into the DB to avoid deadlocks and idle jobs
    web2py.db.commit()
    return "OK"

# Usando las BD de prueba de JSWeCan
@fixture(
    params=[
        {
            "original_pdf": "",
            "codigo": "FS1111",
            "denominacion": "Fisica I",
            "fecha_elaboracion": date.today(),
            "periodo": "ABR-JUL",
            "anio": 2017,
            "horas_teoria": 3,
            "horas_practica": 2,
            "horas_laboratorio": 1,
            "creditos": 3,
            "departamento": "(FS)",
        },
        {
            "original_pdf": "",
            "codigo": "FS5321",
            "denominacion": "Introduccion a la Mecanica Cuantica I",
            "fecha_elaboracion": date.today(),
            "periodo": "SEP-DIC",
            "anio": 1990,
            "horas_teoria": 3,
            "horas_practica": 2,
            "horas_laboratorio": 0,
            "creditos": 4,
            "departamento": "(FS)",
        }
    ]
)
def load_test_programs(request, web2py):
    web2py.db.PROGRAMA.insert(
        original_pdf=request.param['original_pdf'],
        codigo=request.param['codigo'],
        denominacion=request.param['denominacion'],
        fecha_elaboracion=request.param['fecha_elaboracion'],
        periodo=request.param['periodo'],
        anio=request.param['anio'],
        horas_teoria=request.param['horas_teoria'],
        horas_practica=request.param['horas_practica'],
        horas_laboratorio=request.param['horas_laboratorio'],
        creditos=request.param['creditos'],
        estado='aprobado'
    )
    # Commit these changes into the DB to avoid deadlocks and idle jobs
    web2py.db.commit()

    program = {
        "departamento": request.param['departamento'],
        "codigo": request.param['codigo'],
        "denominacion": request.param["denominacion"]
    }

    return program

# Usando las BD de prueba de JSWeCan
@fixture(
    params=[
        {
            "original_pdf": "programas_pdf/Fisica1.pdf",
            "codigo": "FS1111",
            "denominacion": "Física I",
            "fecha_elaboracion": date.today(),
            "periodo": "ABR-JUL",
            "anio": 2017,
            "horas_teoria": 3,
            "horas_practica": 2,
            "horas_laboratorio": 1,
            "creditos": 3,
            "departamento": "(FS)",
        },
    ]
)
def load_test_transcription(request, web2py, login_user, add_transcriptor_role):
    """
    This fixture requires a logged in user and a transcriptor.
    """
    transcription_id = web2py.db.TRANSCRIPCION.insert(
        original_pdf=request.param['original_pdf'],
        transcriptor=login_user['id'],
        codigo=request.param['codigo'],
        periodo=request.param['periodo'],
        anio=request.param['anio'])

    # Commit these changes into the DB to avoid deadlocks and idle jobs
    web2py.db.commit()

    # Creamos el registro transcriptor <-> supervisor
    web2py.db.REGISTRO_TRANSCRIPTORES.insert(
        transcriptor=transcriptor_data['username'],
        supervisor=create_user['username'],
    )
    web2py.db.commit()

    return transcriptor_data

@fixture(params=["TRANSCRIPTOR"])
def add_create_transcription_permission(request, web2py, create_user):
    web2py.db.auth_group.insert(role=request.param, description="")
    web2py.auth.add_permission(web2py.auth.id_group(role=request.param), 'create_transcription')
    # web2py.auth.add_permission(web2py.auth.id_group(role=request.param), 'create_transcription','auth_user')
    web2py.auth.add_membership(web2py.auth.id_group(role=request.param), create_user['id'])
    web2py.db.commit()
    return "OK"

@fixture(params=["DECANATO", "COORDINACION", "DEPARTAMENTO"])
def add_manage_transcriptors_auth_user_permission(request, web2py, create_user):
    web2py.db.auth_group.insert(role=request.param, description="")
    web2py.auth.add_permission(web2py.auth.id_group(role=request.param), 'manage_transcriptors', 'auth_user')
    web2py.auth.add_permission(web2py.auth.id_group(role=request.param), 'manage_transcriptors')
    web2py.auth.add_membership(web2py.auth.id_group(role=request.param), create_user['id'])
    web2py.db.commit()

    return transcription_id

@fixture(
    params=[
        {
            "transcripcion": "",
            "tipo": "book",
            "titulo": "Harry Potter y el prisionero de Azkaban",
            "autor": "J. K. Rowlings",
            "isbn": "9789994030149",
            "coautores": None,
            "colectivo": None,
            "pubyear": 1999,
            "publisher": "Bloomsbury Publishing",
        },
    ]
)
def add_book_reference(request, web2py, load_test_transcription):
    """
    This fixture loads a book as a reference.
    """

    # TODO: Separate the cite functions as a system-wide module.
    # # Create the reference.
    # data = {'titulo':request.param['titulo'],
    #         'publisher':request.param['publisher'],
    #         'autor': request.param['autor'],
    #         'year':request.param['pubyear']
    #         }
    #
    # # Se arma de nuevo la cita de la referencia.
    # citeHarvard = cite(data,'book','book-minimal','harvard1')
    # citeIEEE = cite(data,'book','book-minimal','ieee')
    # citeAPA = cite(data,'book','book-minimal','apa')
    citeHarvard = None
    citeIEEE = None
    citeAPA = None

    reference_id = web2py.db.LIBROS_TRANSCRIPCIONES.insert(
        transcripcion=load_test_transcription,
        tipo=request.param['tipo'],
        titulo=request.param['titulo'],
        autor=request.param['autor'],
        isbn=request.param['isbn'],
        coautores=request.param['coautores'],
        colectivo=request.param['colectivo'],
        pubyear=request.param['pubyear'],
        publisher=request.param['publisher'],
        cite=citeHarvard,
        citeIEEE=citeIEEE,
        citeAPA=citeAPA
    )

    # Commit these changes into the DB to avoid deadlocks and idle jobs
    web2py.db.commit()

    request.param['transcription_id'] = load_test_transcription
    request.param['reference_id'] = reference_id
    return request.param

@fixture()
def web2py(appname):
    """
    Create a Web2py environment similar to that achieved by
    Web2py shell.

    It allows you to use global Web2py objects like db, request, response,
    session, etc.

    Concerning tests, it is usually used to check if your database is an
    expected state, avoiding creating controllers and functions to help
    tests.
    """

    def run(controller, function, env):
        """
        Injects request.controller and request.function into
        web2py environment.
        """

        env.request.controller = controller
        env.request.function = function
        r = None
        try:
            r = run_controller_in(controller, function, env)
        except HTTP as e:
            if str(e.status).startswith("2") or str(e.status).startswith("3"):
                env.db.commit()
            raise
        else:
            env.db.commit()
        finally:
            env.db.rollback()
        return r

    def set_get_parameters(env, data=None):
        if data:
            env.request.vars.update(data)

    def submit(controller, action, env, data=None, formname=None):
        """
        Submits a form, setting _formkey and _formname accordingly.

        env must be the web2py environment fixture.
        """

        formname = formname or "default"

        hidden = dict(
            _formkey=action,
            _formname=formname
        )

        if data:
            env.request.post_vars.update(data)
        env.request.post_vars.update(hidden)
        env.session["_formkey[%s]" % formname] = [action]

        return env.run(controller, action, env)

    web2py_env = env(appname, import_models=True,
                     extra_request=dict(is_local=True,
                                        _running_under_test=True))

    del web2py_env['__file__']  # avoid py.test import error
    web2py_env['run'] = run
    web2py_env['submit'] = submit
    web2py_env['set_get_parameters'] = set_get_parameters
    globals().update(web2py_env)

    return Storage(web2py_env)

@fixture()
def selenium():
    """
    Loads a selenium firefox webdriver for acceptance tests.
    """
    driverPath = join(getcwd(), "applications/SIGPAE/tests/webdrivers/chromedriver/2.38-linux64")
    return webdriver.Chrome(driverPath)
